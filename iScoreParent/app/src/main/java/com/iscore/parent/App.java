package com.iscore.parent;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.iscore.parent.activity.library.PersonalFiles;
import com.iscore.parent.activity.library.SystemFiles;
import com.iscore.parent.activity.SplashActivity;
import com.iscore.parent.model.CctReport;
import com.iscore.parent.model.Comparision;
import com.iscore.parent.model.MainUser;
import com.iscore.parent.model.Overall;
import com.iscore.parent.model.ReportMCQ;
import com.iscore.parent.model.ReportTest;
import com.iscore.parent.utils.MySharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by admin on 10/12/2016.
 */
public class App extends MultiDexApplication {

    public static App app;
    public static MainUser mainUser;
    public static String app_Key = "", Image = "";
    public static String myPref = "iscore_pref";
    public static MySharedPref mySharedPref;
    public static String Target = "";
    public static String Otp = "";
    public static HashMap<Integer, Boolean> flags = new HashMap<>();
    public static ArrayList<CctReport> cctArray = new ArrayList<>();
    public static int cctTotal = 0, cctTotalAccuracy = 0, mcqTotalAccuracy = 0, McqTotal = 0, PractisePaperTotal = 0;
    public int notiCount = 0, NotificationID;

    Context mContext;
    public static boolean isNoti = false;
    public static String isType = "";
    public static ArrayList<SystemFiles> video_list = new ArrayList<>();
    public static ArrayList<PersonalFiles> perrsonal_video_list = new ArrayList<>();
    public String bigPicture;
    public String SubjectName = "", type = "";
    public Overall overall;
    public ReportMCQ mcq;
    public CctReport cctReport;

    public ReportTest test;
    public Comparision compar;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            MultiDex.install(this);
        } catch (RuntimeException multiDexException) {
            multiDexException.printStackTrace();
        }
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        mContext = this;



        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        try {
                            isNoti = true;
                            JSONObject jsonObject = new JSONObject();
                            jsonObject = result.toJSONObject();
                            //  Log.i("One Signal", "==> " + result.stringify());
                            JSONObject jNoti = jsonObject.getJSONObject("notification");
                            JSONObject jpayload = jNoti.getJSONObject("payload");
                            NotificationID = jNoti.getInt("androidNotificationId");
                            JSONObject additionalData = jpayload.getJSONObject("additionalData");
                            if (additionalData.getString("type").equalsIgnoreCase("cct")) {
                                isType = "cct";
                            } else if (additionalData.getString("type").equalsIgnoreCase("notice_board")) {
                                isType = "notice_board";
                            } else if (additionalData.getString("type").equalsIgnoreCase("test_paper")) {
                                isType = "test_paper";
                            } else if (additionalData.getString("type").equalsIgnoreCase("library")) {
                                isType = "library";
                            } else if (additionalData.getString("type").equalsIgnoreCase("zooki")) {
                                isType = "zooki";
                            } else {
                                isType = "1";
                            }
                            try {
                                bigPicture = jpayload.getString("bigPicture");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(mContext, SplashActivity.class);
                            intent.putExtra("Notification_type_id", additionalData.getString("notification_type_id"));
                            intent.putExtra("paper_type", additionalData.getString("paper_type"));
                            intent.putExtra("NotificationID", NotificationID);
                            intent.putExtra("title", jpayload.getString("title"));

                            intent.putExtra("bigPicture", bigPicture);
                            intent.putExtra("created_on", additionalData.getString("created_on"));
                            intent.putExtra("body", jpayload.getString("body"));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                    }
                })

                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();

        try {
            mySharedPref = new MySharedPref(this);
            app = this;
            Class.forName("android.os.AsyncTask");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }
    //------------------- Noti Bubble ------------------- //


    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public App() {
        setInstance(this);
    }

    public static void setInstance(App instance) {
        App.app = instance;
    }

    public static App getInstance() {
        return app;
    }
}