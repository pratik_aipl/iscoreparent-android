package com.iscore.parent.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.IConnect.activity.CctActivity;
import com.iscore.parent.activity.IConnect.activity.NoticeBoardActivity;
import com.iscore.parent.activity.IConnect.activity.TestPaperActivity;
import com.iscore.parent.activity.library.Activity.LibrarySubjectActivity;
import com.iscore.parent.activity.profile.MyProfileActivity;
import com.iscore.parent.activity.reports.ReportsActivity;
import com.iscore.parent.activity.weeklydise.WeeklyDiseActivity;
import com.iscore.parent.adapter.ZookiAdapter;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.interfaceparent.OnTaskCompletedDashboard;
import com.iscore.parent.model.CctReport;
import com.iscore.parent.model.Comparision;
import com.iscore.parent.model.MainUser;
import com.iscore.parent.model.NotificationOneSignal;
import com.iscore.parent.model.NotificationType;
import com.iscore.parent.model.Overall;
import com.iscore.parent.model.ReportMCQ;
import com.iscore.parent.model.ReportTest;
import com.iscore.parent.model.Subject;
import com.iscore.parent.model.Zooki;
import com.iscore.parent.observscroll.BaseActivity;
import com.iscore.parent.observscroll.ObservableScrollView;
import com.iscore.parent.observscroll.ObservableScrollViewCallbacks;
import com.iscore.parent.observscroll.ScrollState;
import com.iscore.parent.observscroll.ScrollUtils;
import com.iscore.parent.observscroll.ViewHelper;
import com.iscore.parent.R;
import com.iscore.parent.db.DBNewQuery;
import com.iscore.parent.db.MyDBManager;
import com.iscore.parent.db.MyDBManagerOneSignal;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.CursorParserUniversal;
import com.iscore.parent.utils.FragmentDrawer;
import com.iscore.parent.utils.ItemOffsetDecoration;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.NetworkUtil;
import com.iscore.parent.utils.Utils;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

public class DashBoardActivity extends BaseActivity implements ObservableScrollViewCallbacks, FragmentDrawer.FragmentDrawerListener, AsynchTaskListner, OnTaskCompletedDashboard {

    public DashBoardActivity instance;
    public App app;
    public AQuery aQuery;
    public JsonParserUniversal jParser;
    public String PleyaerID = "";
    private View mFlexibleSpaceView, mToolbarView, mFab;
    private int mFlexibleSpaceHeight, flexibleSpaceAndToolbarHeight,
            mFabMargin, mActionBarSize, mFlexibleSpaceImageHeight;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    public boolean zookiIsFirstTie = false;
    public ImageView drawerButton;
    private TextView mTitleView, btn_subscrib, tv_userName, tv_visit_site, tv_studID,
            tv_medium, tv_board, tv_standard, tv_licence_status;
    public RelativeLayout lin_iSearch, lin_cct, lin_practies_paper, lin_noticeboard, lin_library, lin_child, lin_weekly;
    public RecyclerView rvZooki;
    public ObservableScrollView scrollView;
    private FragmentDrawer drawerFragment;
    public RecyclerView.LayoutManager mLayoutManager1;
    public CircleImageView fab_profile;
    public RelativeLayout  mainLay;
    IntentFilter intentFilter;
    public Zooki zooki;
    public ArrayList<Zooki> zookiList = new ArrayList<>();
    public static LinkedHashMap<String, String> mapImages = new LinkedHashMap<>();

    public ZookiAdapter zookiAdapter;
    public MyDownloadDownloadStatusListenerV1 myDownloadStatusListener = new MyDownloadDownloadStatusListenerV1();
    public RetryPolicy retryPolicy;
    public static ThinDownloadManager downloadManager;
    public MyDBManagerOneSignal mDbOneSignal;

    public ProgressDialog pDialog;
    public static ArrayList<NotificationOneSignal> OneSignalArray = new ArrayList<>();
    public int cct_count = 0, practies_count = 0, noticeboard_count = 0, lib_count = 0;
    public TextView tv_class, tv_class_name, tv_practies_count, tv_cct_count, tv_noticboard_count, tv_lib_count, tv_search_count;
    public static MyDBManager mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_dash_board);
        Utils.logUser();
        instance = this;
        retryPolicy = new DefaultRetryPolicy();
        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        intentFilter = new IntentFilter();
        intentFilter.addAction(Constant.CONNECTIVITY_ACTION);
        PleyaerID = Utils.OneSignalPlearID();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        mDbOneSignal = MyDBManagerOneSignal.getInstance(this);
        mDbOneSignal.open(this);
        DataSherdToAppClass();
        FindElements();
        ScrollXYPotion();

    }

    //-------------------  Data Sherd To App Class  ------------------- //

    public void DataSherdToAppClass() {
        app.mainUser = new MainUser();
        app.mainUser.setFirstName(app.mySharedPref.getFirstName());
        app.mainUser.setLastName(app.mySharedPref.getLastName());
        app.mainUser.setRegKey(app.mySharedPref.getReg_key());
        app.mainUser.setGuid(app.mySharedPref.getGuuid());
        app.mainUser.setClassID(app.mySharedPref.getClassID());
        app.mainUser.setBoardID(app.mySharedPref.getBoardID());
        app.mainUser.setMediumID(app.mySharedPref.getMediumID());
        app.mainUser.setStandardID(app.mySharedPref.getStandardID());
        app.mainUser.setEmailID(app.mySharedPref.getEmailID());
        app.mainUser.setRegMobNo(app.mySharedPref.getRegMobNo());
        app.mainUser.setSubjects(app.mySharedPref.getSubjects());
        app.mainUser.setStudent_id(app.mySharedPref.getStudentID());
        app.mainUser.setStanderdName(app.mySharedPref.getStandardName());
        app.mainUser.setClassName(app.mySharedPref.getClassName());
        app.mainUser.setKeyStatus(app.mySharedPref.getKeyStatus());
        app.mainUser.setBranchID(app.mySharedPref.getBranchID());
        app.mainUser.setBatchID(app.mySharedPref.getBatchID());
        app.mainUser.setSchoolName(app.mySharedPref.getSchoolName());
        app.mainUser.setVersion(app.mySharedPref.getVersion());
        app.mainUser.setBoardName(app.mySharedPref.getBoardName());
        app.mainUser.setMediumName(app.mySharedPref.getMediumName());
        app.mainUser.setStudentCode(app.mySharedPref.getStudentCode());
    }

    //-------------------  Cast Veriabels  ------------------- //

    public void FindElements() {
        aQuery = new AQuery(DashBoardActivity.this);

        mTitleView = (TextView) findViewById(R.id.title);
        tv_userName = (TextView) findViewById(R.id.tv_userName);
        fab_profile = (CircleImageView) findViewById(R.id.img_profile);
        tv_studID = (TextView) findViewById(R.id.tv_studID);
        tv_medium = (TextView) findViewById(R.id.tv_medium);
        tv_board = (TextView) findViewById(R.id.tv_board);
        tv_standard = (TextView) findViewById(R.id.tv_standard);
        tv_visit_site = (TextView) findViewById(R.id.tv_visit_site);
        tv_licence_status = (TextView) findViewById(R.id.tv_licence_status);
        rvZooki = (RecyclerView) findViewById(R.id.rvRelated);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        drawerButton = (ImageView) findViewById(R.id.drawerButton);
        tv_cct_count = findViewById(R.id.tv_cct_count);
        tv_practies_count = findViewById(R.id.tv_practies_count);
        tv_noticboard_count = findViewById(R.id.tv_noticboard_count);
        tv_lib_count = findViewById(R.id.tv_lib_count);

        mToolbarView = findViewById(R.id.toolbar);
        mainLay = (RelativeLayout) findViewById(R.id.mainLay);
        drawerFragment = (FragmentDrawer) getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        scrollView = (ObservableScrollView) findViewById(R.id.scroll);
        findViewById(R.id.body).setPadding(0, getResources().getDimensionPixelSize(R.dimen._20sdp), 0, 0);
        mFab = (RelativeLayout) findViewById(R.id.fab);
        // lin_practies_paper = (LinearLayout) findViewById(R.id.lin_practies_paper);
        //  lin_search_paper = (LinearLayout) findViewById(R.id.lin_search_paper);
        lin_cct = (RelativeLayout) findViewById(R.id.lin_cct);
        lin_practies_paper = (RelativeLayout) findViewById(R.id.lin_practies_paper);
        lin_noticeboard = (RelativeLayout) findViewById(R.id.lin_noticeboard);
        lin_library = (RelativeLayout) findViewById(R.id.lin_library);
        lin_child = (RelativeLayout) findViewById(R.id.lin_child);
        lin_weekly = (RelativeLayout) findViewById(R.id.lin_weekly);
        btn_subscrib = (TextView) findViewById(R.id.btn_subscrib);

        if (App.mySharedPref.getVersion().equals("0")) {
            tv_licence_status.setText("Licence : Purchased");
//            btn_subscrib.setVisibility(View.GONE);
        } else {
            tv_licence_status.setText("Licence : Demo");

        }

        lin_library.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, LibrarySubjectActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        lin_weekly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, WeeklyDiseActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        lin_noticeboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, NoticeBoardActivity.class));
            }
        });
        lin_cct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, CctActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        lin_practies_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, TestPaperActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        lin_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(instance, ReportsActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });
        tv_userName.setText(app.mainUser.getFirstName() + " " + app.mainUser.getLastName());
        tv_board.setText(app.mainUser.getBoardName());
        tv_medium.setText(app.mainUser.getMediumName() + " Medium");
        tv_studID.setText(app.mainUser.getStudentCode());
        tv_standard.setText("CLASS " + app.mainUser.getStanderdName());
        new CallRequest(DashBoardActivity.this).getStudentParentData();

        if (!app.mySharedPref.getClassLogo().isEmpty()) {
            File imageFile = new File(app.mySharedPref.getClassLogo());
            if (imageFile.isFile()) {

                fab_profile.setImageBitmap(BitmapFactory.decodeFile(app.mySharedPref.getClassRoundLogo()));

            } else {
                imageFile = new File(app.mySharedPref.getProf_image());
                if (imageFile.isFile()) {
                    fab_profile.setImageBitmap(BitmapFactory.decodeFile(app.mySharedPref.getProf_image()));

                } else {
                    fab_profile.setImageResource(R.drawable.profile);
                }
            }
        } else {
            File imageFile = new File(app.mySharedPref.getProf_image());
            if (imageFile.isFile()) {
                fab_profile.setImageBitmap(BitmapFactory.decodeFile(app.mySharedPref.getProf_image()));

            } else {
                fab_profile.setImageResource(R.drawable.profile);
            }

        }


    }


    //-------------------  Noti Bubble  ------------------- //


    //-------------------  Sync Data  ------------------- //

    public void SyncData() {
        if (NetworkUtil.getConnectivityStatus(instance) > 0) {
            System.out.println("Connect");
            new CallRequest(this).get_zooki();
        } else {
            System.out.println("No connection");
        }
    }

    //-------------------  Set Target  ------------------- //

    @Override
    public void onBackPressed() {
        if (drawerFragment.mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerFragment.mDrawerLayout.closeDrawer(Gravity.LEFT);
        }

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setMessage("Are you sure you want to exit the iScocre APP")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finishAffinity();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", null)
                .show();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public static void getConnectivityStatusString(Context context) {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //-------------------  Side Panel  Start -------------------//

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {

        switch (position) {
            case 0:

                break;
            case 1:
                startActivity(new Intent(instance, MyProfileActivity.class));

                break;

            case 2:
                startActivity(new Intent(instance, QueryCallUsActivity.class));

                break;
            case 3:
                ReferFriend();
                //startActivity(new Intent(instance, IscoreWebActivity.class));
                break;

            case 4:
                launchMarket();
                break;

            case 5:
                startActivity(new Intent(instance, LegalActivity.class));
                break;

            case 6:
                startActivity(new Intent(instance, NotificationActivity.class));
                break;


            default:
                break;
        }

    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Log.i("TAG url ::--> ", String.valueOf(uri));
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    public void ReferFriend() {
        //"market://details?id=" + getPackageName()
        Uri uri = Uri.parse("http://bit.ly/iScore_App");

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "iSocre");
        i.putExtra(Intent.EXTRA_TEXT, "Terrified of Tests? Be confident with iScore! Get unlimited set of tests with model answers for Maharashtra Board Std 8-10. Install NOW " + String.valueOf(uri));
        startActivity(Intent.createChooser(i, "Share via"));
    }


    //-------------------  Side Panel  END -------------------//


    //-------------------  Scrooling  Start -------------------//

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        updateFlexibleSpaceText(scrollY);

    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    public void ScrollXYPotion() {

        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._130sdp);
        mFabMargin = getResources().getDimensionPixelSize(R.dimen._17sdp);
        mActionBarSize = getActionBarSize();
        flexibleSpaceAndToolbarHeight = mFlexibleSpaceHeight + getActionBarSize();
        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen._70sdp);


        mTitleView.setText("Dashboard");
        ViewHelper.setScaleX(mFab, 0);
        ViewHelper.setScaleY(mFab, 0);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), drawerButton, mainLay);
        drawerFragment.setDrawerListener(this);
        scrollView.setScrollViewCallbacks(this);
        mFlexibleSpaceView.getLayoutParams().height = flexibleSpaceAndToolbarHeight;

        ScrollUtils.addOnGlobalLayoutListener(mFab, new Runnable() {
            @Override
            public void run() {
                updateFlexibleSpaceText(scrollView.getCurrentScrollY());
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void updateFlexibleSpaceText(final int scrollY) {

        ViewHelper.setTranslationY(mFlexibleSpaceView, -scrollY);
        int adjustedScrollY = (int) ScrollUtils.getFloat(scrollY, 0, mFlexibleSpaceHeight);
        float maxScale = (float) (mFlexibleSpaceHeight - mToolbarView.getHeight()) / mToolbarView.getHeight();
        float scale = maxScale * ((float) mFlexibleSpaceHeight - adjustedScrollY) / mFlexibleSpaceHeight;

        ViewHelper.setPivotX(mFab, 0);
        ViewHelper.setPivotY(mFab, 0);
        ViewHelper.setScaleX(mFab, 1 + scale);
        ViewHelper.setScaleY(mFab, 1 + scale);

        int maxTitleTranslationX = mToolbarView.getWidth() - mFab.getWidth() - 10;
        int baseColor = getResources().getColor(R.color.colorPrimary);
        float alpha = Math.min(1, (float) scrollY / 180);
        int maxFabTranslationY = mFlexibleSpaceImageHeight - mFab.getHeight() / 2;
        float fabTranslationY = ScrollUtils.getFloat(-scrollY + mFlexibleSpaceImageHeight - mFab.getHeight() / 2, mActionBarSize - mFab.getHeight() / 2, maxFabTranslationY);
        float fabTranslationX = ScrollUtils.getFloat((int) ((scrollY - scale) * ((scale + 1.5))), mFabMargin, maxTitleTranslationX);

        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mFab.getLayoutParams();
            lp.leftMargin = mFlexibleSpaceView.getWidth() - mFabMargin;
            lp.topMargin = (int) fabTranslationY;
            mFab.requestLayout();
        } else {
            ViewHelper.setTranslationX(mFab, fabTranslationX);
            ViewHelper.setTranslationY(mFab, fabTranslationY);
        }

    }

    //-------------------  Scrooling  END-------------------//

    //-------------------  Zooki   Start-------------------//

    public void RecyclerViewZooki() {

        mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvZooki.setLayoutManager(mLayoutManager1);
        rvZooki.setHasFixedSize(true);
        if (!zookiIsFirstTie) {
            zookiIsFirstTie = true;
            final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
            rvZooki.addItemDecoration(new ItemOffsetDecoration(spacing));
        }
        RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                rvZooki.removeOnScrollListener(this);

                rvZooki.addOnScrollListener(this);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        rvZooki.addOnScrollListener(scrollListener);
        zookiAdapter = new ZookiAdapter(instance, zookiList);
        rvZooki.setAdapter(zookiAdapter);
        tv_visit_site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, ZookiWebActivity.class));

            }
        });
    }

    @Override
    public void onGetRecorComplite() {

    }

    @Override
    public void onInsertRecorComplite() {

    }

    class MyDownloadDownloadStatusListenerV1 implements DownloadStatusListenerV1 {


        int tempCompleteDownload = 0;

        @Override
        public void onDownloadComplete(DownloadRequest request) {

            Log.i("TAG", "Download Complete  ZOOKI:: ");
        }

        @Override
        public void onDownloadFailed(DownloadRequest request, int errorCode, String errorMessage) {

            Log.i("TAG", "Download Failed  ZOOKI:: ");
        }

        @Override
        public void onProgress(DownloadRequest request, long totalBytes, long downloadedBytes, int progress) {
            int id = request.getDownloadId();


        }
    }

    public void manageImagesDownload(LinkedHashMap<String, String> map) {

        Set<String> keys = map.keySet();
        int i = 0;
        for (String url : keys) {
            final DownloadRequest downloadRequest1 = new DownloadRequest(Uri.parse(url.replace(" ", "%20")))
                    .setDestinationURI(Uri.parse(map.get(url))).setPriority(DownloadRequest.Priority.HIGH)
                    .setRetryPolicy(retryPolicy)
                    .setDownloadContext(url)
                    .setDeleteDestinationFileOnFailure(false)
                    .setStatusListener(myDownloadStatusListener);
            downloadManager.add(downloadRequest1);
        }
        Log.i("Zooki list size", "==>" + zookiList.size());
        RecyclerViewZooki();

    }

    //-------------------  Zooki  END-------------------//

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case upadateProfile:
                        Utils.hideProgressDialog();
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                        }
                        break;
                    case getStudentParentData:
                        Utils.hideProgressDialog();
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {

                            JSONObject jData = jObj.getJSONObject("data");
                            app.mySharedPref.setLastSyncTime(jData.getString("last_synctime"));
                            JSONObject jOverall = jData.getJSONObject("overall");
                            app.overall = new Overall();
                            app.overall.setMcq_attemps(jOverall.getString("mcq_attemps"));
                            app.overall.setReady_papaer_download(jOverall.getString("ready_paper_download"));
                            app.overall.setSet_papaer_download(jOverall.getString("set_paper_download"));
                            app.overall.setPrelim_papaer_download(jOverall.getString("prelim_paper_download"));
                            app.overall.setBoard_papaer_download(jOverall.getString("board_paper_download"));
                            app.overall.setPractice_paper_download(jOverall.getInt("practice_paper_download"));
                            app.overall.setCct_test_total(jOverall.getString("cct_test_total"));
                            app.overall.setCct_overall_accuracy(String.valueOf(jOverall.getInt("cct_overall_accuracy")));


                            //------------ MCQ ---------------

                            JSONObject jMcq = jData.getJSONObject("mcq");
                            app.mcq = new ReportMCQ();
                            app.mcq.setMcq_attemps(jMcq.getString("mcq_attemps"));
                            app.mcq.setLevel_1(jMcq.getString("level_1"));
                            app.mcq.setLevel_2(jMcq.getString("level_2"));
                            app.mcq.setLevel_3(jMcq.getString("level_3"));
                            app.mcq.setAccuracy(String.valueOf(jMcq.getInt("accuracy_level")));


                            JSONArray jSubject_accuracy = jMcq.getJSONArray("subject_accuracy");
                            if (jSubject_accuracy != null && jSubject_accuracy.length() > 0) {

                                for (int i = 0; i < jSubject_accuracy.length(); i++) {

                                    JSONObject jSubjectObj = jSubject_accuracy.getJSONObject(i);
                                    ReportMCQ mcq = new ReportMCQ();
                                    mcq.setSubject(jSubjectObj.getString("subject"));
                                    mcq.setSubject_total_test(jSubjectObj.getString("subject_total_test"));
                                    mcq.setTotallevel1(jSubjectObj.getString("Totallevel1"));
                                    mcq.setTotallevel2(jSubjectObj.getString("Totallevel2"));
                                    mcq.setTotallevel3(jSubjectObj.getString("Totallevel3"));
                                    mcq.setAccurecy_level(jSubjectObj.getInt("accurecy_level1"));
                                    mcq.setAccurecy_level2(jSubjectObj.getInt("accurecy_level2"));
                                    mcq.setAccurecy_level3(jSubjectObj.getInt("accurecy_level3"));
                                    mcq.setAccurecy_level(jSubjectObj.getInt("accurecy_level"));

                                    app.mcq.mcqArray.add(mcq);
                                    Log.i("TAG", "MCQ ARRAY:->" + app.mcq.mcqArray.get(i).getSubject());
                                }

                            }
                            //------------ CCT ---------------

                            JSONObject jCct = jData.getJSONObject("cct");
                            app.cctReport = new CctReport();
                            app.cctReport.setCct_attemps(jCct.getString("cct_attemps"));
                            app.cctReport.setCct_overall_accuracy(String.valueOf(jCct.getInt("cct_overall_accuracy")));

                            JSONArray jCct_accuracy = jCct.getJSONArray("subject_accuracy");
                            if (jCct_accuracy != null && jCct_accuracy.length() > 0) {
                                for (int i = 0; i < jCct_accuracy.length(); i++) {
                                    JSONObject obj = jCct_accuracy.getJSONObject(i);
                                    CctReport cctReportObj = new CctReport();
                                    cctReportObj.setSubjectName(obj.getString("SubjectName"));
                                    cctReportObj.setTotal(obj.getString("Total"));
                                    cctReportObj.setAcuuracy(obj.getString("Acuuracy"));
                                    app.cctReport.cctReportsArray.add(cctReportObj);
                                }

                            }
                            //SubjectAccuracyLevelAdapter subAccu = new SubjectAccuracyLevelAdapter(this,mcqArray);
                            //-------------- TEST -----------------

                            JSONObject jTest = jData.getJSONObject("test");
                            app.test = new ReportTest();
                            app.test.setNo_of_paper_generate(jTest.getString("no_of_paper_generate"));
                            app.test.setWith_self_assesment(jTest.getString("with_self_assesment"));
                            app.test.setWithout_self_assesment(jTest.getString("without_self_assesment"));
                            app.test.setPrelim_Paper_test(jTest.getString("prelim_test"));
                            app.test.setSet_paper_test(jTest.getString("set_paper_test"));
                            app.test.setReady_paper_test(jTest.getString("ready_paper_test"));

                            JSONArray jSubject_Test = jTest.getJSONArray("subject_array");
                            if (jSubject_Test != null && jSubject_Test.length() > 0) {

                                for (int i = 0; i < jSubject_Test.length(); i++) {

                                    JSONObject jSubjectObj = jSubject_Test.getJSONObject(i);
                                    ReportTest test = new ReportTest();
                                    test.setSubject_name(jSubjectObj.getString("subject_name"));
                                    test.setPrelim_test(jSubjectObj.getString("prelim_test"));
                                    test.setReady_test(jSubjectObj.getString("ready_test"));
                                    test.setSet_test(jSubjectObj.getString("set_test"));
                                    test.setTotal_test(jSubjectObj.getString("total_test"));

                                    app.test.testArray.add(test);
                                }
                            }

                            //--------------- Comparition -------------------


                            JSONObject jComparision = jData.getJSONObject("comparision");
                            app.compar = new Comparision();
                            JSONArray jSubject_comparision = jComparision.getJSONArray("subject_comparision");
                            if (jSubject_comparision != null && jSubject_comparision.length() > 0) {
                                for (int i = 0; i < jSubject_comparision.length(); i++) {

                                    JSONObject jSubjectOb = jSubject_comparision.getJSONObject(i);
                                    Comparision cmp = new Comparision();
                                    cmp.setSubject(jSubjectOb.getString("subject"));
                                    cmp.setMcq_total_test(jSubjectOb.getString("mcq_total_test"));
                                    cmp.setPaper_total_test(jSubjectOb.getString("paper_total_test"));
                                    cmp.setScalar(jSubjectOb.getString("scalar"));
                                    cmp.setMcq_accuracy(jSubjectOb.getInt("mcq_accuracy"));
                                    cmp.setCct_total_test(jSubjectOb.getInt("cct_total_test"));
                                    cmp.setCct_accuracy(jSubjectOb.getInt("cct_accuracy"));
                                    app.compar.compArray.add(cmp);
                                }
                            }
                        } else {
                            Utils.showToast("Please try again later", DashBoardActivity.this);

                        }
                        break;

                    case get_zooki:
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            zookiList.clear();
                            String folderPath = jObj.getString("folder_path");
                            JSONArray jData = jObj.getJSONArray("data");
                            if (jData.length() > 0 || jData != null) {
                                String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/" + "Zooki";
                                File f = new File(SDCardPath);
                                if (!f.exists()) {
                                    if (f.mkdirs()) {
                                    }
                                }
                                for (int i = 0; i < jData.length(); i++) {
                                    zooki = (Zooki) jParser.parseJson(jData.getJSONObject(i), new Zooki());
                                    mDb.insertRow(zooki, "zooki");
                                    zookiList.add(zooki);
                                    String url = folderPath + zooki.getImage();
                                    String localUrl = SDCardPath + "/" + zooki.getImage();
                                    Log.i("TAG", " IMAGE PATH ::::>" + localUrl);
                                    File lf = new File(localUrl);
                                    if (!lf.exists()) {
                                        mapImages.put(url, localUrl);
                                    }
                                }
                                Zooki obj = new Zooki();
                                obj.setTitle("View More");
                                zookiList.add(obj);
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                manageImagesDownload(mapImages);
                            }
                        }
                        break;
                    case check_login_token:

                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                        } else {
                            App.deleteCache(DashBoardActivity.this);
                            app.mySharedPref.clearApp();
                            Intent intent = new Intent(DashBoardActivity.this, MobiLoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            App.deleteCache(this);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //------------------- Data  GET and  Parse  Start-----------------------//


    //----------MCQ Start-------------//
    public class getCountRecorrd extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public String SubjectName = "", paper_type = "", created_on = "", notification_type_id = "", type = "";


        private getCountRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);

        }

        @Override
        protected void onPreExecute() {
            OneSignalArray.clear();
            noticeboard_count = 0;
            lib_count = 0;
            practies_count = 0;
            cct_count = 0;
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {

            int N_ID = mDb.getLastRecordIDForRequest("notification", "OneSignalID");

            Cursor noti = mDbOneSignal.getAllRows(DBNewQuery.getNotificatonDash(N_ID));
            Log.d("TAG", "Cursor :->" + DatabaseUtils.dumpCursorToString(noti));

            if (noti != null && noti.moveToFirst()) {
                do {
                    NotificationOneSignal notiObj = (NotificationOneSignal) cParse.parseCursor(noti, new NotificationOneSignal());
                    OneSignalArray.add(notiObj);
                } while (noti.moveToNext());
                noti.close();
            }

            if (OneSignalArray != null && OneSignalArray.size() > 0) {
                for (int i = 0; i < OneSignalArray.size(); i++) {
                    try {
                        NotificationType typeObj = new NotificationType();

                        JSONObject jObj = new JSONObject(OneSignalArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        notification_type_id = jtype.getString("notification_type_id");
                        created_on = jtype.getString("created_on");
                        SubjectName = jtype.getString("subject_name");
                        paper_type = jtype.getString("paper_type");
                        typeObj.setOneSignalID(Integer.parseInt(OneSignalArray.get(i).get_id()));
                        typeObj.setCreatedOn(created_on);
                        typeObj.setIsOpen(OneSignalArray.get(i).getOpened());
                        typeObj.setIsTypeOpen("0");
                        typeObj.setSubjectName(SubjectName);
                        typeObj.setNotification_type_id(notification_type_id);
                        typeObj.setModuleType(type);
                        typeObj.setPaperType(paper_type);
                        typeObj.setMessage(OneSignalArray.get(i).getMessage());
                        typeObj.setTitle(OneSignalArray.get(i).getTitle());
                        mDb.insertWebRow(typeObj, "notification");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            Cursor c = mDb.getAllRows("Select * from notification where ModuleType = 'cct' AND isTypeOpen = 0");
            // Log.i("TAG", "Query :-> Select * from notification where opened=0");
            if (c != null && c.moveToFirst()) {
                cct_count = c.getCount();
                c.close();
            } else {
                cct_count = 0;
            }
            c = mDb.getAllRows("Select * from notification where ModuleType = 'notice_board' AND isTypeOpen = 0");
            // Log.i("TAG", "Query :-> Select * from notification where opened=0");
            if (c != null && c.moveToFirst()) {
                noticeboard_count = c.getCount();
                c.close();
            } else {
                noticeboard_count = 0;
            }
            c = mDb.getAllRows("Select * from notification where ModuleType = 'library' AND isTypeOpen = 0");
            // Log.i("TAG", "Query :-> Select * from notification where opened=0");
            if (c != null && c.moveToFirst()) {
                Log.i("TAG", "In IF library:-> ");
                lib_count = c.getCount();
                c.close();
            } else {
                lib_count = 0;
            }
            c = mDb.getAllRows("Select * from notification where ModuleType = 'test_paper' AND isTypeOpen = 0");
            // Log.i("TAG", "Query :-> Select * from notification where opened=0");
            if (c != null && c.moveToFirst()) {
                Log.i("TAG", "In IF test_paper:-> ");
                practies_count = c.getCount();
                c.close();
            } else {
                practies_count = 0;
            }


            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (cct_count != 0) {
                tv_cct_count.setVisibility(View.VISIBLE);
                tv_cct_count.setText(cct_count + "");
            } else {
                tv_cct_count.setVisibility(View.GONE);
            }
            if (practies_count != 0) {
                tv_practies_count.setVisibility(View.VISIBLE);
                tv_practies_count.setText(practies_count + "");
            } else {
                tv_practies_count.setVisibility(View.GONE);
            }
            if (lib_count != 0) {
                tv_lib_count.setVisibility(View.VISIBLE);
                tv_lib_count.setText(lib_count + "");
            } else {
                tv_lib_count.setVisibility(View.GONE);
            }
            if (noticeboard_count != 0) {
                tv_noticboard_count.setVisibility(View.VISIBLE);
                tv_noticboard_count.setText(noticeboard_count + "");
            } else {
                tv_noticboard_count.setVisibility(View.GONE);
            }

        }
    }


    //------------------- Data  GET and  Parse  End-----------------------//


    @Override
    protected void onResume() {
        SyncData();
        new getCountRecorrd(DashBoardActivity.this).execute();
        super.onResume();
    }

}
