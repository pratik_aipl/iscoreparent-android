package com.iscore.parent.activity.IConnect;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 1/26/2018.
 */

public class EMcqPaper implements Serializable {

    public String PaperID = "";
    public String SubjectName = "";
    public String PaperFile = "";

    public String getTakenTest() {
        return TakenTest;
    }

    public void setTakenTest(String takenTest) {
        TakenTest = takenTest;
    }

    public String TakenTest="";

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String ModifiedOn="";

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String ExpiryDate="";



    public String getPaperID() {
        return PaperID;
    }

    public void setPaperID(String paperID) {
        PaperID = paperID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getPaperFile() {
        return PaperFile;
    }

    public void setPaperFile(String paperFile) {
        PaperFile = paperFile;
    }
}
