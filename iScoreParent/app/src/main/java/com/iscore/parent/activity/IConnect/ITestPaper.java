package com.iscore.parent.activity.IConnect;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 1/31/2018.
 */

public class ITestPaper implements Serializable {
    public String PaperID = "";
    public String SubjectName = "";
    public String PaperDate = "";

    public String getPaperID() {
        return PaperID;
    }

    public void setPaperID(String paperID) {
        PaperID = paperID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getPaperDate() {
        return PaperDate;
    }

    public void setPaperDate(String paperDate) {
        PaperDate = paperDate;
    }

    public String getPaperTypeName() {
        return PaperTypeName;
    }

    public void setPaperTypeName(String paperTypeName) {
        PaperTypeName = paperTypeName;
    }

    public String getTotalMarks() {
        return TotalMarks;
    }

    public void setTotalMarks(String totalMarks) {
        TotalMarks = totalMarks;
    }

    public String PaperTypeName = "";
    public String TotalMarks = "";

    public String IsStudentMarkUploaded = "";
    public String StudentMark = "";

    public String getStudentMark() {
        return StudentMark;
    }

    public void setStudentMark(String studentMark) {
        StudentMark = studentMark;
    }

    public String getIsStudentMarkUploaded() {
        return IsStudentMarkUploaded;
    }

    public void setIsStudentMarkUploaded(String isStudentMarkUploaded) {
        IsStudentMarkUploaded = isStudentMarkUploaded;
    }
}
