package com.iscore.parent.activity.IConnect;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 2/28/2018.
 */

public class NoticeBoard implements Serializable {
    public String Title = "", Description = "", CreatedOn = "", Image = "";

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
}
