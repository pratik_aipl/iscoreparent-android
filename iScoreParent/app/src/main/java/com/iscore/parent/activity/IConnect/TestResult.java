package com.iscore.parent.activity.IConnect;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 2/1/2018.
 */

public class TestResult implements Serializable {


    public String TotalRight = "";
    public String TotalQquestion = "";
    public String TotalNotAppeared = "";
    public String accuracy = "";
    public String TestTime = "";
    public String AvaragePerQuestionTime = "";
    public String TotalIncorrectQue = "";
    public String total_level_1 = "";
    public String total_level_2 = "";
    public String total_level_3 = "";
    public String total_right_level_1 = "";
    public String total_right_level_2 = "";

    public String getTotal_right_level_1() {
        return total_right_level_1;
    }

    public void setTotal_right_level_1(String total_right_level_1) {
        this.total_right_level_1 = total_right_level_1;
    }

    public String getTotal_right_level_2() {
        return total_right_level_2;
    }

    public void setTotal_right_level_2(String total_right_level_2) {
        this.total_right_level_2 = total_right_level_2;
    }

    public String getTotal_right_level_3() {
        return total_right_level_3;
    }

    public void setTotal_right_level_3(String total_right_level_3) {
        this.total_right_level_3 = total_right_level_3;
    }

    public String total_right_level_3 = "";
    public String subject_accuracy = "";

    public String over_all_accuracy = "";

    public String getSubject_accuracy() {
        return subject_accuracy;
    }

    public void setSubject_accuracy(String subject_accuracy) {
        this.subject_accuracy = subject_accuracy;
    }

    public String getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(String totalRight) {
        TotalRight = totalRight;
    }

    public String getTotalQquestion() {
        return TotalQquestion;
    }

    public void setTotalQquestion(String totalQquestion) {
        TotalQquestion = totalQquestion;
    }

    public String getTotalNotAppeared() {
        return TotalNotAppeared;
    }

    public void setTotalNotAppeared(String totalNotAppeared) {
        TotalNotAppeared = totalNotAppeared;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public String getTestTime() {
        return TestTime;
    }

    public void setTestTime(String testTime) {
        TestTime = testTime;
    }

    public String getAvaragePerQuestionTime() {
        return AvaragePerQuestionTime;
    }

    public void setAvaragePerQuestionTime(String avaragePerQuestionTime) {
        AvaragePerQuestionTime = avaragePerQuestionTime;
    }

    public String getTotalIncorrectQue() {
        return TotalIncorrectQue;
    }

    public void setTotalIncorrectQue(String totalIncorrectQue) {
        TotalIncorrectQue = totalIncorrectQue;
    }

    public String getTotal_level_1() {
        return total_level_1;
    }

    public void setTotal_level_1(String total_level_1) {
        this.total_level_1 = total_level_1;
    }

    public String getTotal_level_2() {
        return total_level_2;
    }

    public void setTotal_level_2(String total_level_2) {
        this.total_level_2 = total_level_2;
    }

    public String getTotal_level_3() {
        return total_level_3;
    }

    public void setTotal_level_3(String total_level_3) {
        this.total_level_3 = total_level_3;
    }

    public String getOver_all_accuracy() {
        return over_all_accuracy;
    }

    public void setOver_all_accuracy(String over_all_accuracy) {
        this.over_all_accuracy = over_all_accuracy;
    }


}
