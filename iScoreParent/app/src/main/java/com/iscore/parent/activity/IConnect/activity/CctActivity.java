package com.iscore.parent.activity.IConnect.activity;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.IConnect.adapter.CctTestAdapter;
import com.iscore.parent.activity.IConnect.EMcqPaper;
import com.iscore.parent.adapter.CustomSubjectSpinnerAdapter;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.db.MyDBManager;
import com.iscore.parent.model.Subject;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.ItemOffsetDecoration;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class CctActivity extends AppCompatActivity implements AsynchTaskListner {
    private static final String TAG = "CctActivity";
    public RecyclerView recycler_mcq_sub;
    public JsonParserUniversal jParser;
    public EMcqPaper emp;
    public ArrayList<EMcqPaper> paperArray = new ArrayList<>();
    public ImageView img_back;
    public TextView tv_title, tv_empty;
    public ArrayList<String> NOTIFICATION_ID = new ArrayList<>();
    private CctTestAdapter adapter;
    private LinearLayout emptyView;
    public MyDBManager mDb;
    Spinner mSubjectSpinner;
    List<Subject> subjectList = new ArrayList<>();
    CustomSubjectSpinnerAdapter subjectSpinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_cct);
        Utils.logUser();
        jParser = new JsonParserUniversal();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        mDb.dbQuery("UPDATE notification SET isTypeOpen = 1,isOpen=1 WHERE ModuleType = 'cct'");
        Cursor c = mDb.getAllRows("select android_notification_id from notification where ModuleType = 'cct'");
        if (c != null && c.moveToFirst()) {
            do {
                NOTIFICATION_ID.add(c.getString(c.getColumnIndex("android_notification_id")));
            } while (c.moveToNext());
            c.close();
        }
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        for (int i = 0; i < NOTIFICATION_ID.size(); i++) {
            try {
                int ID = Integer.parseInt(NOTIFICATION_ID.get(i));
                Log.i("TAG", "NOTIFICATION_ID :-> " + ID);
                notificationManager.cancel(ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        img_back = (ImageView) findViewById(R.id.img_back);
        mSubjectSpinner = (Spinner) findViewById(R.id.mSubjectSpinner);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("CCT");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        emptyView = (LinearLayout) findViewById(R.id.empty_view);
        tv_empty = (TextView) findViewById(R.id.tv_empty);
        emptyView.setVisibility(View.GONE);
        recycler_mcq_sub = (RecyclerView) findViewById(R.id.recycler_mcq_sub);
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recycler_mcq_sub.addItemDecoration(new ItemOffsetDecoration(spacing));

        subjectSpinnerAdapter = new CustomSubjectSpinnerAdapter(this, R.layout.raw_spinner_layout, R.id.title, subjectList);
        subjectSpinnerAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mSubjectSpinner.setAdapter(subjectSpinnerAdapter);

        new CallRequest(this).get_subject();
        mSubjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemSelected: " + i);
                if (adapter != null) {
                    if (!subjectList.get(i).getSubjectName().equalsIgnoreCase("all")) {
                        adapter.getFilter().filter(subjectList.get(i).getSubjectName());
                    } else {
                        adapter.getFilter().filter("");
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupRecyclerView(ArrayList<EMcqPaper> paperArray) {
        Context context = recycler_mcq_sub.getContext();
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_mcq_sub.setLayoutAnimation(controller);
        recycler_mcq_sub.scheduleLayoutAnimation();
        recycler_mcq_sub.setLayoutManager(new LinearLayoutManager(context));
        adapter = new CctTestAdapter(paperArray, context);
        recycler_mcq_sub.setAdapter(adapter);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            switch (request) {
                case get_subject:
                    try {
                        JSONArray jData = new JSONArray(result);
                        if (jData.length() > 0) {
                            subjectList.clear();
                            Subject subject = new Subject();
                            subject.setSubjectID("-1");
                            subject.setSubjectName("All");
                            subjectList.add(subject);
                            for (int i = 0; i < jData.length(); i++) {
                                Subject subject1 = (Subject) jParser.parseJson(jData.getJSONObject(i), new Subject());
                                subjectList.add(subject1);
                            }
                            subjectSpinnerAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case get_recent_mcq_paper:
                    paperArray.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {
                                for (int i = 0; i < jDataArray.length(); i++) {
                                    JSONObject jpaper = jDataArray.getJSONObject(i);
                                    emp = (EMcqPaper) jParser.parseJson(jpaper, new EMcqPaper());
                                    paperArray.add(emp);
                                }
                                setupRecyclerView(paperArray);
                                Utils.hideProgressDialog();
                            }
                        } else {
                            Utils.hideProgressDialog();
                            tv_empty.setText(jObj.getString("message"));
                            recycler_mcq_sub.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        tv_empty.setText("message");
                        recycler_mcq_sub.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new CallRequest(CctActivity.this).get_recent_mcq_paper();
    }
}
