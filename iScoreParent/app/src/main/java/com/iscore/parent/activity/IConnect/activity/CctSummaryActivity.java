package com.iscore.parent.activity.IConnect.activity;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.iscore.parent.activity.IConnect.fragment.ICorrectSummaryFragment;
import com.iscore.parent.activity.IConnect.fragment.IIncorrectFragment;
import com.iscore.parent.activity.IConnect.fragment.INotAppearedFragment;
import com.iscore.parent.model.EvaluterMcqTestMain;
import com.iscore.parent.model.EvaluterQuestionOption;
import com.iscore.parent.observscroll.BaseActivity;
import com.iscore.parent.observscroll.CacheFragmentStatePagerAdapter;
import com.iscore.parent.observscroll.FlexibleSpaceWithImageBaseFragment;
import com.iscore.parent.observscroll.ScrollUtils;
import com.iscore.parent.observscroll.Scrollable;
import com.iscore.parent.R;
import com.iscore.parent.utils.RobotoTextView;
import com.iscore.parent.utils.Utils;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class CctSummaryActivity extends BaseActivity {
    protected static final float MAX_TEXT_SCALE_DELTA = 0.3f;
    public CctSummaryActivity instance;
    private ViewPager mPager;
    public TextView tv_non_attempted, tv_incorrect, tv_correct;
    private NavigationAdapter mPagerAdapter;
    private TabLayout mSlidingTabLayout;
    private int mFlexibleSpaceHeight;
    private int mTabHeight;
    public static ArrayList<EvaluterMcqTestMain> rightAnsArray = new ArrayList<>();
    public static ArrayList<EvaluterMcqTestMain> nonAttemptAnsArray = new ArrayList<>();
    public static ArrayList<EvaluterMcqTestMain> incorectAnsArray = new ArrayList<>();
    ImageView img_back;

    public EvaluterMcqTestMain evaMcqTestMain = new EvaluterMcqTestMain();
    public static ArrayList<EvaluterMcqTestMain> EvalMainArray = new ArrayList<>();
    public EvaluterQuestionOption options = new EvaluterQuestionOption();
    public String start = "", id = "", subject_name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_cct_summary);
        Utils.logUser();
        instance = this;
        Utils.showProgressDialog(instance);
        mPager = (ViewPager) findViewById(R.id.pager);
        mSlidingTabLayout = (TabLayout) findViewById(R.id.sliding_tabs);

        id = getIntent().getExtras().getString("temp");
        rightAnsArray = (ArrayList<EvaluterMcqTestMain>) getIntent().getExtras().getSerializable("rightAnsArray");
        incorectAnsArray = (ArrayList<EvaluterMcqTestMain>) getIntent().getExtras().getSerializable("incorectAnsArray");
        nonAttemptAnsArray = (ArrayList<EvaluterMcqTestMain>) getIntent().getExtras().getSerializable("nonAttemptAnsArray");

        img_back = (ImageView) findViewById(R.id.img_back);
        tv_non_attempted = findViewById(R.id.tv_non_attempted);
        tv_incorrect = findViewById(R.id.tv_incorrect);
        tv_correct = findViewById(R.id.tv_correct);

        tv_correct.setText("   "+(rightAnsArray.size()-1)+" Correct");
        tv_incorrect.setText("   "+(incorectAnsArray.size()-1)+" Incorrect");
        tv_non_attempted.setText("   "+(nonAttemptAnsArray.size()-1)+" Not answerred");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mFlexibleSpaceHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mTabHeight = getResources().getDimensionPixelSize(R.dimen._42sdp);
        mPagerAdapter = new NavigationAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mSlidingTabLayout.setupWithViewPager(mPager);
        setupTabFont();
        // Initialize the first Fragment's state when layout is completed.

        ScrollUtils.addOnGlobalLayoutListener(mSlidingTabLayout, new Runnable() {
            @Override
            public void run() {
                translateTab(0, false);
            }
        });
        Utils.hideProgressDialog();

    }


    private void setupTabFont() {

        RobotoTextView OverAll = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_reports, null);
        OverAll.setText("Correct");
        mSlidingTabLayout.getTabAt(0).setCustomView(OverAll);

        RobotoTextView MCQ = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_reports, null);
        MCQ.setText("Incorrect");
        mSlidingTabLayout.getTabAt(1).setCustomView(MCQ);

        RobotoTextView CCT = (RobotoTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_reports, null);
        CCT.setText("Not Appeared");
        mSlidingTabLayout.getTabAt(2).setCustomView(CCT);
    }

    public void onScrollChanged(int scrollY, Scrollable s) {
        FlexibleSpaceWithImageBaseFragment fragment =
                (FlexibleSpaceWithImageBaseFragment) mPagerAdapter.getItemAt(mPager.getCurrentItem());
        if (fragment == null) {
            return;
        }
        View view = fragment.getView();
        if (view == null) {
            return;
        }
        Scrollable scrollable = (Scrollable) view.findViewById(R.id.recycler_view);
        if (scrollable == null) {
            return;
        }
        if (scrollable == s) {
            // This method is called by not only the current fragment but also other fragments
            // when their scrollY is changed.
            // So we need to check the caller(S) is the current fragment.
            int adjustedScrollY = Math.min(scrollY, mFlexibleSpaceHeight - mTabHeight);
            translateTab(adjustedScrollY, false);
            propagateScroll(adjustedScrollY);
        }
    }

    private void translateTab(int scrollY, boolean animated) {
        int flexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        int tabHeight = getResources().getDimensionPixelSize(R.dimen._42sdp);
        View imageView = findViewById(R.id.image);
        View overlayView = findViewById(R.id.overlay);
        TextView titleView = (TextView) findViewById(R.id.title);

        LinearLayout linear = (LinearLayout) findViewById(R.id.linear);
        // Translate overlay and image
        float flexibleRange = flexibleSpaceImageHeight - getActionBarSize() - tabHeight;
        int minOverlayTransitionY = tabHeight - overlayView.getHeight();
        ViewHelper.setTranslationY(overlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(imageView, ScrollUtils.getFloat(-scrollY / 2, minOverlayTransitionY, 0));

        // Change alpha of overlay
        ViewHelper.setAlpha(overlayView, ScrollUtils.getFloat((float) scrollY / flexibleRange, 0, 1));

        // Scale title text
        float scale = 1 + ScrollUtils.getFloat((flexibleRange - scrollY - tabHeight) / flexibleRange, 0, MAX_TEXT_SCALE_DELTA);
        setPivotXToTitle(titleView);
        setPivotXToImage(img_back);
        setPivotXToLayout(linear);

        // Translate title text
        int maxTitleTranslationY = tabHeight - getActionBarSize();
        int titleTranslationY = maxTitleTranslationY - scrollY;
        ViewHelper.setTranslationY(titleView, titleTranslationY);
        ViewHelper.setTranslationY(img_back, titleTranslationY);
        ViewHelper.setTranslationY(linear, titleTranslationY);

        // If tabs are moving, cancel it to start a new animation.
        ViewPropertyAnimator.animate(mSlidingTabLayout).cancel();
        // Tabs will move between the top of the screen to the bottom of the image.
        float translationY = ScrollUtils.getFloat(-scrollY + mFlexibleSpaceHeight - mTabHeight, 0, mFlexibleSpaceHeight - mTabHeight);
        if (animated) {
            // Animation will be invoked only when the current tab is changed.
            ViewPropertyAnimator.animate(mSlidingTabLayout)
                    .translationY(translationY)
                    .setDuration(200)
                    .start();
        } else {
            // When Fragments' scroll, translate tabs immediately (without animation).
            ViewHelper.setTranslationY(mSlidingTabLayout, translationY);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setPivotXToTitle(View view) {
        final TextView mTitleView = (TextView) view.findViewById(R.id.title);
        Configuration config = getResources().getConfiguration();
        if (Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT
                && config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            ViewHelper.setPivotX(mTitleView, view.findViewById(android.R.id.content).getWidth());
        } else {
            ViewHelper.setPivotX(mTitleView, 0);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setPivotXToImage(View view) {

        Configuration config = getResources().getConfiguration();
        if (Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT
                && config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            ViewHelper.setPivotX(img_back, view.findViewById(android.R.id.icon).getWidth());
        } else {
            ViewHelper.setPivotX(img_back, 0);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setPivotXToLayout(View view) {
        LinearLayout linear = (LinearLayout) findViewById(R.id.linear);
        Configuration config = getResources().getConfiguration();
        if (Build.VERSION_CODES.JELLY_BEAN_MR1 <= Build.VERSION.SDK_INT
                && config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL) {
            ViewHelper.setPivotX(linear, view.findViewById(android.R.id.content).getWidth());
        } else {
            ViewHelper.setPivotX(linear, 0);
        }
    }

    private void propagateScroll(int scrollY) {
        // Set scrollY for the fragments that are not created yet
        mPagerAdapter.setScrollY(scrollY);

        // Set scrollY for the active fragments
        for (int i = 0; i < mPagerAdapter.getCount(); i++) {
            // Skip current item
            if (i == mPager.getCurrentItem()) {
                continue;
            }

            // Skip destroyed or not created item
            FlexibleSpaceWithImageBaseFragment f =
                    (FlexibleSpaceWithImageBaseFragment) mPagerAdapter.getItemAt(i);
            if (f == null) {
                continue;
            }

            View view = f.getView();
            if (view == null) {
                continue;
            }
            f.setScrollY(scrollY, mFlexibleSpaceHeight);
            f.updateFlexibleSpace(scrollY);
        }
    }

    private static class NavigationAdapter extends CacheFragmentStatePagerAdapter {

        private static final String[] TITLES = new String[]{"Correct", "Incorrect", "Not Appeared"};

        private int mScrollY;

        public NavigationAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setScrollY(int scrollY) {
            mScrollY = scrollY;
        }

        @Override
        protected Fragment createItem(int position) {
            FlexibleSpaceWithImageBaseFragment f;
            final int pattern = position % 4;
            switch (pattern) {
                case 0: {
                    f = new ICorrectSummaryFragment();
                    break;
                }
                case 1: {
                    f = new IIncorrectFragment();
                    break;
                }
                case 2: {
                    f = new INotAppearedFragment();
                    break;
                }
                default: {
                    f = new ICorrectSummaryFragment();
                    break;
                }
            }
            f.setArguments(mScrollY);
            return f;
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }
    }

}
