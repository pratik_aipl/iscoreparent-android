package com.iscore.parent.activity.IConnect.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.DashBoardActivity;
import com.iscore.parent.activity.IConnect.TestResult;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.model.EvaluterMcqTestMain;
import com.iscore.parent.model.EvaluterQuestionOption;
import com.iscore.parent.model.LevelTestReportMCQ;
import com.iscore.parent.observscroll.BaseActivity;
import com.iscore.parent.observscroll.ObservableScrollView;
import com.iscore.parent.observscroll.ObservableScrollViewCallbacks;
import com.iscore.parent.observscroll.ScrollState;
import com.iscore.parent.observscroll.ScrollUtils;
import com.iscore.parent.R;
import com.iscore.parent.db.MyDBManager;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.Utils;
import com.nineoldandroids.view.ViewHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class CctTestResultActivity extends BaseActivity implements ObservableScrollViewCallbacks, AsynchTaskListner {
    public CctTestResultActivity instance;
    public Toolbar mToolbar;

    private View mFlexibleSpaceView, mOverlayView, mImageView;
    private int mActionBarSize, mFlexibleSpaceImageHeight, HeaderId;
    public ObservableScrollView mScrollView;
    public WebView GoalStaticsProgress, SubjectStaticsProgress;
    public ImageView img_back;

    public ArrayList<LevelTestReportMCQ> levelArrayList = new ArrayList<>();
    public Button btn_start_new;
    public RelativeLayout rel_summary;
    public TextView tv_title, tv_all_sub_accuracy, tv_accu_subject, tv_subName, tv_previous_goal, tv_total_three, tv_total_two, tv_total_one, tv_right_three, tv_right_two, tv_right_one, tv_right, tv_total_que, tv_non_attempted, tv_incorrect, tv_correct;
    public int TotalQue = 0, TotalDuration = 0, TotalRight = 0, GoalStatics = 0, SubjectStatics = 0, AvgTimePerQuestion = 0, AvgTimePerQuestionSubjectWise = 0, AvgTimePerQuestionAllSubject = 0, AccuracyAllSubject = 0;

    public TextView tv_totla_time, tv_time_subName, tv_avg_per_que, tv_sub_vise_per_que, tv_all_sub_wise_per_que;
    public ArrayList<EvaluterMcqTestMain> rightAnsArray = new ArrayList<>();
    public ArrayList<EvaluterMcqTestMain> nonAttemptAnsArray = new ArrayList<>();
    public ArrayList<EvaluterMcqTestMain> incorectAnsArray = new ArrayList<>();
    public String total_que, attempted_que, right_ans, taken_id, paper_id, wrong_ans, accuracy_level,
            subject_name, paperid = "", TestTime, AvaragePerQuestionTime, notAnswerd;
    public JsonParserUniversal jParser;
    public TestResult testResult;
    public EvaluterMcqTestMain evaMcqTestMain = new EvaluterMcqTestMain();
    public static ArrayList<EvaluterMcqTestMain> EvalMainArray = new ArrayList<>();
    public EvaluterQuestionOption options = new EvaluterQuestionOption();
    public MyDBManager mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_cct_test_result);
        Utils.logUser();
        instance = this;
        jParser = new JsonParserUniversal();

        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen._170sdp);
        mActionBarSize = getActionBarSize();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mFlexibleSpaceView = findViewById(R.id.flexible_space);
        mOverlayView = findViewById(R.id.overlay);
        GoalStaticsProgress = findViewById(R.id.progress);
        SubjectStaticsProgress = findViewById(R.id.progress_two);

        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);

        tv_title = findViewById(R.id.tv_title);
        img_back = findViewById(R.id.img_back);
        tv_subName = findViewById(R.id.tv_subName);
        tv_previous_goal = findViewById(R.id.tv_previous_goal);
        tv_total_three = findViewById(R.id.tv_total_three);
        tv_accu_subject = findViewById(R.id.tv_accu_subject);
        tv_total_two = findViewById(R.id.tv_total_two);
        tv_total_one = findViewById(R.id.tv_total_one);
        tv_right_three = findViewById(R.id.tv_right_three);
        tv_right_two = findViewById(R.id.tv_right_two);
        tv_right_one = findViewById(R.id.tv_right_one);
        tv_total_que = findViewById(R.id.tv_total_que);
        tv_right = findViewById(R.id.tv_right);
        tv_all_sub_accuracy = findViewById(R.id.tv_all_sub_accuracy);

        tv_totla_time = findViewById(R.id.tv_totla_time);
        tv_avg_per_que = findViewById(R.id.tv_avg_per_que);
        tv_sub_vise_per_que = findViewById(R.id.tv_sub_vise_per_que);
        tv_all_sub_wise_per_que = findViewById(R.id.tv_all_sub_wise_per_que);

        tv_non_attempted = findViewById(R.id.tv_non_attempted);
        tv_incorrect = findViewById(R.id.tv_incorrect);
        tv_correct = findViewById(R.id.tv_correct);
        rel_summary = findViewById(R.id.rel_summary);
        btn_start_new = findViewById(R.id.btn_start_new);
        tv_time_subName = findViewById(R.id.tv_time_subName);
        if (getIntent().getExtras().getString("type").equals("1")) {
            taken_id = getIntent().getExtras().getString("taken_id");
            subject_name = getIntent().getExtras().getString("subject_name");
            tv_subName.setText(subject_name);
            tv_accu_subject.setText("Accuracy comparision with " + subject_name + " Subject");
            new CallRequest(CctTestResultActivity.this).get_mcq_test_result_activity(taken_id);
        } else {
            paper_id = getIntent().getExtras().getString("id");

            //    taken_id = getIntent().getExtras().getString("taken_id");
            //    subject_name = getIntent().getExtras().getString("subject_name");
            //    tv_subName.setText(subject_name);
            //    tv_accu_subject.setText("Accuracy comparision with " + subject_name + " Subject");
            new CallRequest(CctTestResultActivity.this).get_mcq_test_result_activity(paper_id);
            mDb.dbQuery("UPDATE notification SET CctSubmit = 1,isTypeOpen = 1 WHERE ModuleType = 'cct' AND notification_type_id = " + paper_id);
            rel_summary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new CallRequest(instance).mcq_view_summary(paper_id);

                }
            });
        }
        HeaderId = getIntent().getExtras().getInt("temp");

        tv_title.setText(" CCT Test Report");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mImageView = findViewById(R.id.image);
        mScrollView = (ObservableScrollView) findViewById(R.id.scroll);
        mScrollView.setScrollViewCallbacks(this);
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) 0 / mFlexibleSpaceImageHeight, 0, 1));


        rel_summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CallRequest(instance).mcq_view_summary(taken_id);

            }
        });
        btn_start_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, DashBoardActivity.class)
                        //  .putExtra("suObj", mcqdata)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void WebViewSetting(WebView webview, String Process, String process2) {
        Utils.webSettings(webview);
        webview.setWebViewClient(new myWebClient());
        //webview.loadUrl("file:///android_asset/progressbar/progress.html");
        webview.loadDataWithBaseURL("", ProgressBarHtmL(Process, process2), "text/html", "UTF-8", "");

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Utils.hideProgressDialog();

            try {

                switch (request) {
                    case get_mcq_test_result:
                        JSONObject jObj = new JSONObject(result);
                        Utils.hideProgressDialog();
                        if (jObj.getBoolean("status") == true) {
                            JSONObject jData = jObj.getJSONObject("data");

                            testResult = (TestResult) jParser.parseJson(jData, new TestResult());
                            setData(testResult);
                        } else {
                            Utils.showToast("try again", this);
                            //switchFragment(new EvalTestResultFragment());
                        }
                        break;
                    case mcq_view_summary:
                        jObj = new JSONObject(result);
                        Utils.hideProgressDialog();
                        if (jObj.getBoolean("status") == true) {
                            if (jObj.has("data")) {
                                JSONObject jData = jObj.getJSONObject("data");
                                EvalMainArray.clear();
                                if (jData.has("header_data")) {
                                    JSONObject jheaderData = jData.getJSONObject("header_data");
                                    subject_name = jheaderData.getString("SubjectName");
                                    JSONArray question_list = jData.getJSONArray("question_list");
                                    if (question_list != null && question_list.length() > 0) {
                                        try {
                                            for (int i = 0; i < question_list.length(); i++) {
                                                JSONObject jpaper = question_list.getJSONObject(i);

                                                evaMcqTestMain = new EvaluterMcqTestMain();
                                                evaMcqTestMain.setClassMCQTestHDRID(jheaderData.getString("ClassMCQTestHDRID"));
                                                evaMcqTestMain.setQuestion(jpaper.getString("Question"));
                                                evaMcqTestMain.setLevel(jpaper.getString("Level"));
                                                evaMcqTestMain.setQuestionID(jpaper.getString("QuestionID"));
                                                evaMcqTestMain.setMasterorCustomized(jpaper.getString("MasterorCustomized"));
                                                evaMcqTestMain.setIsAttempt(jpaper.getString("IsAttempt"));
                                                evaMcqTestMain.setIsRight(jpaper.getString("IsRight"));
                                                evaMcqTestMain.setClassMCQTestDTLID(jpaper.getString("ClassMCQTestDTLID"));
                                                evaMcqTestMain.setAnswerID(jpaper.getString("AnswerID"));

                                                JSONArray jQuestion_optionArray = jpaper.getJSONArray("Question_option");

                                                if (jQuestion_optionArray != null && jQuestion_optionArray.length() > 0) {

                                                    for (int j = 0; j < jQuestion_optionArray.length(); j++) {
                                                        JSONObject jQuestion_option = jQuestion_optionArray.getJSONObject(j);
                                                        options = new EvaluterQuestionOption();
                                                        options.setIsCorrect(jQuestion_option.getString("isCorrect"));
                                                        options.setMCQOPtionID(jQuestion_option.getString("MCQOPtionID"));
                                                        options.setOptions(jQuestion_option.getString("Options"));
                                                        if (evaMcqTestMain.getAnswerID().equals(options.getMCQOPtionID())) {
                                                            evaMcqTestMain.selectedAns = Integer.parseInt(options.getMCQOPtionID());
                                                        }
                                                        if (evaMcqTestMain.getAnswerID().equals(options.getMCQOPtionID()) && options.getIsCorrect().equals("1")) {
                                                            options.isRightAns = 1;
                                                        }
                                                        evaMcqTestMain.optionArray.add(options);
                                                    }
                                                }
                                                EvalMainArray.add(evaMcqTestMain);
                                            }

                                            setDataArray();

                                            Utils.hideProgressDialog();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                }


                            }
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setDataArray() {
        int i = 0;


        nonAttemptAnsArray.clear();
        incorectAnsArray.clear();
        rightAnsArray.clear();

        EvaluterMcqTestMain question = new EvaluterMcqTestMain();
        question.isHeader = true;
        nonAttemptAnsArray.add(question);
        incorectAnsArray.add(question);
        rightAnsArray.add(question);

        for (EvaluterMcqTestMain que : EvalMainArray) {
            if (que.getIsRight().equalsIgnoreCase("1")) {
                rightAnsArray.add(que);
            }
            if (que.getIsAttempt().equalsIgnoreCase("1") && que.getIsRight().equalsIgnoreCase("0")) {
                incorectAnsArray.add(que);

            }
            if (que.getIsAttempt().equalsIgnoreCase("0") && que.getIsRight().equalsIgnoreCase("0")) {
                nonAttemptAnsArray.add(que);

            }


        }
        startActivity(new Intent(instance, CctSummaryActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra("temp", taken_id)
                .putExtra("rightAnsArray", rightAnsArray)
                .putExtra("incorectAnsArray", incorectAnsArray)
                .putExtra("nonAttemptAnsArray", nonAttemptAnsArray));

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void setData(TestResult testResult) {
        tv_right.setText(testResult.getTotalRight());
        tv_total_que.setText(testResult.getTotalQquestion());
        tv_right_one.setText(testResult.getTotal_right_level_1());
        tv_total_one.setText(testResult.getTotal_level_1());
        tv_right_two.setText(testResult.getTotal_right_level_2());
        tv_total_two.setText(testResult.getTotal_level_2());
        tv_right_three.setText(testResult.getTotal_right_level_3());
        tv_total_three.setText(testResult.getTotal_level_3());
        WebViewSetting(GoalStaticsProgress, testResult.getAccuracy(), App.mySharedPref.getlblTarget());
        WebViewSetting(SubjectStaticsProgress, testResult.getAccuracy(), testResult.getSubject_accuracy());
        tv_correct.setText(testResult.getTotalRight() + " Correct");
        tv_all_sub_accuracy.setText("Your Accuracy for all subject is: " + testResult.getOver_all_accuracy() + "%");
        tv_incorrect.setText(testResult.getTotalIncorrectQue() + " Incorrect");
        tv_non_attempted.setText(testResult.getTotalNotAppeared() + " Not answerred");
        // tv_previous_goal.setText("Your Previous Year School Result was: " + 76 + "%");
        tv_previous_goal.setText(Html.fromHtml("Your Previous Year School Result : <b>" + App.mySharedPref.getlblPrviousTarget() + "% </b>"));

        //  tv_sub_vise_per_que.setText(testResult.getTestTime());
        // tv_all_sub_wise_per_que.setText(testResult.getAvaragePerQuestionTime());
        try {
            tv_sub_vise_per_que.setText("" + MinuteTotal(Integer.parseInt(testResult.getTestTime())) + ":" + SecondTotal(Integer.parseInt(testResult.getTestTime())) + "");
            tv_all_sub_wise_per_que.setText(testResult.getAvaragePerQuestionTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String mins = String.valueOf(TotalDuration / 60);
        String sec = String.valueOf(TotalDuration % 60);
        try {
            if (mins.length() <= 1) {
                mins = "0" + mins;
            }
            if (sec.length() <= 1) {
                sec = "0" + sec;
            }
            tv_totla_time.setText("Total Taken Time " + mins + ":" + sec + " Min:Sec");


        } catch (Exception e) {
            tv_totla_time.setText("Total Taken Time --:-- Min:Sec");
            e.printStackTrace();

        }

    }

    public String SecondTotal(int second) {
        String sec = String.valueOf(second % 60);
        if (sec.length() <= 1) {
            sec = "0" + sec;
        }
        return sec;
    }

    public String MinuteTotal(int sec) {

        String mins = String.valueOf(sec / 60);
        if (mins.length() <= 1) {
            mins = "0" + mins;
        }
        return mins;
    }

    private class myWebClient extends WebViewClient {
        ProgressDialog progressDialog;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                // Utils.hideProgressDialog();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        float flexibleRange = mFlexibleSpaceImageHeight - mActionBarSize;
        int minOverlayTransitionY = mActionBarSize - mOverlayView.getHeight();
        ViewHelper.setTranslationY(mOverlayView, ScrollUtils.getFloat(-scrollY, minOverlayTransitionY, 0));
        ViewHelper.setTranslationY(mImageView, ScrollUtils.getFloat(-scrollY / 2, minOverlayTransitionY, 0));
        // Change alpha of overlay
        ViewHelper.setAlpha(mOverlayView, ScrollUtils.getFloat((float) scrollY / flexibleRange, 0, 1));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    public String ProgressBarHtmL(String Progress, String SecondProgress) {
        return "<!DOCTYPE html>" +
                "<head>  " +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" +
                "<meta charset=\"utf-8\" />" +
                "  <link rel='stylesheet' type='text/css' href='file:///android_asset/bootstrap.min.css'/>" +
/*
                "  <link rel=\"stylesheet\" type=\"text/css\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\" />" +
               */ "  <script type=\"text/javascript\" src=\"file:///android_asset/jquery.min.js\"></script>" +
                "  <script type=\"text/javascript\" src=\"file:///android_asset/bootstrap.min.js\"></script>" +

                "<Script>" +
                "  $(document).ready(function() {" +
                "    $(function () {" +
                "       $('[data-toggle=\"tooltip\"]').tooltip({trigger: 'manual'}).tooltip('show');\n" +
                "  });" +
                "  $(\".progress-bar\").each(function(){" +
                "    each_bar_width = $(this).attr('aria-valuenow');" +
                "    $(this).width(each_bar_width + '%');" +
                "  });" +
                "   });" +
                "  </Script>" +
                "<style>" +
                ".barWrapper{height: 60px;margin-top: 30px;width:100%}" +
                "" +
                ".tooltip{ " +
                "  position:relative;" +
                "  float:right;" +
                "} " +
                ".tooltip > .tooltip-inner {" +
                "  padding: 3px 6px;" +
                "  color: #fff;" +
                "  font-weight: bold;" +
                "  font-size: 11px;" +
                "  width: auto;" +
                "  height: auto;" +
                "  border-radius: 4px;" +
                "  line-height: 14px;" +
                "  display: block;" +
                "  vertical-align: middle;" +
                "}" +
                ".tooltip.top{top:-34px !important; opacity: 1; margin-top: 0; width:auto;}" +
                "" +
                ".tooltip.top > .tooltip-inner {  background-color: #f49738; }" +
                "" +
                ".popOver + .tooltip.top > .tooltip-arrow {  border-left: 6px solid transparent; border-right: 6px solid transparent; border-top: 6px solid #f49738; left:50% !important}" +
                "" +
                "" +
                "" +
                ".tooltip.bottom > .tooltip-inner {  background-color: #206e9e; }" +
                ".tooltip.bottom{top:12px !important; opacity: 1; margin-top: 0;}" +
                ".popOver + .tooltip.bottom > .tooltip-arrow {  border-left: 6px solid transparent; border-right: 6px solid transparent; border-bottom: 6px solid #206e9e;  left:50% !important}" +
                "" +
                "section{" +
                "  margin:100px auto; " +
                "  height:1000px;" +
                "}" +
                ".progress{" +
                "  border-radius:0;" +
                "  overflow:visible;" +
                "  height: 10px;" +
                "  margin: 0 auto;" +
                " width:82%;" +
                "}" +
                ".progress-bar{ " +
                "  -webkit-transition: width 1.5s ease-in-out;" +
                "  transition: width 1.5s ease-in-out;" +
                "}" +
                "" +
                ".me-at.progress{ position: relative; z-index: 11; background: rgba(0,0,0,0);}" +
                ".me-at.progress .progress-bar{   background:#f49738; opacity: 1; z-index: 11}" +
                "" +
                ".mygoal.progress{position: relative;top: -10px; z-index: 1}" +
                "" +
                ".mygoal.progress .progress-bar{   background:#206e9e; opacity: .9; z-index: 11}" +
                "" +
                "</style>" +
                "" +
                "</head>" +
                "<body>" +
                "     " +
                "  <!--<h2 class=\"text-center\">Scroll down the page a bit</h2><br><br> -->" +
                "<div class=\"container\">" +
                "  " +
                "    " +
                "    " +
                "       " +
                "      <div class=\"barWrapper\"> " +
                "        <div class=\"progress me-at\">" +
                "          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=" + Progress + " aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"ME&nbsp;(" + Progress + "%)\"> </span>     " +
                "          </div> " +
                "        </div>" +
                "        <div class=\"progress mygoal\">" +
                "         <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=" + SecondProgress + " aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\" MY&nbsp;GOAL&nbsp;(" + SecondProgress + "%)\"> </span>     " +
                "          </div>" +
                "        </div>" +
                "      </div>" +
                " " +
                "</div>   " +


                "</body>" +

                "</html>";
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }
}

