package com.iscore.parent.activity.IConnect.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.DashBoardActivity;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.Utils;

import io.fabric.sdk.android.Fabric;

public class IPreviewPaperActivity extends AppCompatActivity implements AsynchTaskListner {
    public String HTML = "";
    public WebView webpdf;
    public String paper_id = "";
    public ImageView img_back, img_home,img_rotation;
    public TextView tv_title;
    public FloatingActionButton imgprint;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_preview_pdf);
        Utils.logUser();

        imgprint = (FloatingActionButton) findViewById(R.id.imgprint);
        img_rotation = (ImageView) findViewById(R.id.img_rotation);
        img_home = (ImageView) findViewById(R.id.img_home);
        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });

        webpdf = (WebView) findViewById(R.id.pdfView);
        webpdf.setWebViewClient(new myWebClient());
        webpdf.setBackgroundColor(Color.TRANSPARENT);
        Utils.webSettings(webpdf);
        tv_title = (TextView) findViewById(R.id.tv_title);
        img_back = (ImageView) findViewById(R.id.img_back);

        tv_title.setText("Preview");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (getIntent().hasExtra("privew")) {
            paper_id = getIntent().getExtras().getString("paper_id");
         //   new CallRequest(IPreviewPaperActivity.this).view_paper_activity(paper_id);
            webpdf.loadUrl("https://test.pemevaluater.parshvaa.com/class-admin/web-services-paper/view-paper?id="+paper_id);
        } else {
            paper_id = getIntent().getExtras().getString("paper_id");
            new CallRequest(IPreviewPaperActivity.this).view_model_answer(paper_id);
        }
        img_rotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

                }
            }
        });
        imgprint.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                printPDF();
            }
        });

    }

    public void gotoDashboard() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("Are you sure you want to visit Dashboard?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Utils.showProgressDialog(IPreviewPaperActivity.this);

                        startActivity(new Intent(IPreviewPaperActivity.this, DashBoardActivity.class));
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                Utils.hideProgressDialog();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case view_paper:
                    //Utils.hideProgressDialog();
                    try {

                        if (result.contains("true")) {
                            HTML = result.substring(result.indexOf(",\"data\":\"") + 9, result.length() - 2);
                            HTML = HTML.replace("\\t", "");
                            HTML = HTML.replace("\\r", "");
                            HTML = HTML.replace("\\n", "");
                            HTML = HTML.replace("\\\"", "\"");
                            /*HTML = HTML.replace("\\t", "");
                            HTML = HTML.replace("\\r", "");
                            HTML = HTML.replace("\\n", "");
                            HTML = HTML.replace("\\/", "/");
                            HTML = HTML.replace("\\\"","&quot;");*/


                            //settings.setDefaultTextEncodingName("UTF-8");
                            longInfo(HTML,"Html print");
                            webpdf.setWebViewClient(new myWebClient());
                            webpdf.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
                        } else {
                            //result.substring(result.indexOf("\"message\":\""), result.indexOf("\""));
                            Utils.showToast(result.substring(result.indexOf("\"message\":\"") + 12, result.indexOf("\",\"data\"")), this);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;

                case view_model_answer:
                    Utils.hideProgressDialog();
                    try {
                        if (result.contains("true")) {
                            HTML = result.substring(result.indexOf(",\"data\":\"") + 9, result.length() - 2);
                            HTML = HTML.replace("\\t", "");
                            HTML = HTML.replace("\\r", "");
                            HTML = HTML.replace("\\n", "");
                            HTML = HTML.replace("\\\"", "\"");
                            Log.i("TAG", "HTML::-->" + HTML);
                            webpdf.loadDataWithBaseURL("", HTML, "text/html", "UTF-8", "");
                        } else {
                            //result.substring(result.indexOf("\"message\":\""), result.indexOf("\""));
                            Utils.showToast(result.substring(result.indexOf("\"message\":\"") + 12, result.indexOf("\",\"data\"")), this);
                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
           Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else {
          Log.i("TAG " + tag + " -->", str);
        }
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void printPDF() {
        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = webpdf.createPrintDocumentAdapter();
        String jobName = getString(R.string.app_name) + " Report "
                + System.currentTimeMillis();
        PrintAttributes printAttrs = new PrintAttributes.Builder().
                setColorMode(PrintAttributes.COLOR_MODE_MONOCHROME).
                setMediaSize(PrintAttributes.MediaSize.NA_LETTER.asLandscape()).
                setMinMargins(new PrintAttributes.Margins(50, 0, 50, 0)).
                build();
        PrintJob printJob = printManager.print(jobName, printAdapter,
                printAttrs);
        //7mPrintJobs.add(printJob);
    }

}
