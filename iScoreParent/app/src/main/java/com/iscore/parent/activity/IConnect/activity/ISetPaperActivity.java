package com.iscore.parent.activity.IConnect.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.DashBoardActivity;
import com.iscore.parent.activity.IConnect.fragment.ISetPaperFragment;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.model.PrelimQuestionListModel;
import com.iscore.parent.model.SetPaperQuestionAndTypes;
import com.iscore.parent.R;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class ISetPaperActivity extends AppCompatActivity implements AsynchTaskListner {
    public ViewPager viewPager;
    public MyViewPagerAdapter myViewPagerAdapter;
    public FloatingActionButton float_right_button, float_left_button;
    public TextView tv_pager_count;
    public ImageView img_back, img_home, img_right, img_left;
    public TextView tv_title;
    public ISetPaperActivity instance;
    public Button btn_privew_paper, btn_model_paper;

    public static ArrayList<SetPaperQuestionAndTypes> tempQuestionTypesArray = new ArrayList<>();
    public static HashMap<String, ArrayList<SetPaperQuestionAndTypes>> stringArrayListHashMap = new HashMap<>();
    ArrayList<SetPaperQuestionAndTypes> values = null;
    String paper_id;
    public PrelimQuestionListModel preQueObj;
    public SetPaperQuestionAndTypes setQueType;
    public JsonParserUniversal jParser;
    public static ArrayList<PrelimQuestionListModel> questionsList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_generate_paper_solution);
        Utils.logUser();
        instance = this;
        paper_id = getIntent().getExtras().getString("id");
        paper_id = getIntent().getExtras().getString("id");



        new CallRequest(instance).get_single_question_paper(paper_id);
        jParser = new JsonParserUniversal();

        viewPager = (ViewPager) findViewById(R.id.pager);
        img_home = findViewById(R.id.img_home);

        btn_privew_paper = (Button) findViewById(R.id.btn_privew_paper);
        btn_model_paper = (Button) findViewById(R.id.btn_model_paper);
        tv_pager_count = (TextView) findViewById(R.id.tv_pager_count);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_right = (ImageView) findViewById(R.id.img_right);
        img_left = (ImageView) findViewById(R.id.img_left);
        tv_title = (TextView) findViewById(R.id.tv_title);
        float_right_button = (FloatingActionButton) findViewById(R.id.float_right_button);
        float_left_button = (FloatingActionButton) findViewById(R.id.float_left_button);

        img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDashboard();
            }
        });
        btn_privew_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, IPreviewPaperActivity.class)
                        .putExtra("paper_id", paper_id).putExtra("privew", "privew")
                );
            }
        });
        btn_model_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, IPreviewPaperActivity.class)
                        .putExtra("paper_id", paper_id).putExtra("model", "model"));
            }
        });
        tv_title.setText("Generate Paper");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        float_left_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        float_right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < 10) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });
        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos > 0) {
                    viewPager.setCurrentItem(Pos - 1);
                }
            }
        });
        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int Pos = viewPager.getCurrentItem();
                if (Pos < 10) {
                    viewPager.setCurrentItem(Pos + 1);
                }
            }
        });


    }

    public void gotoDashboard() {
        new AlertDialog.Builder(instance)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Alert")
                .setCancelable(false)

                .setMessage("Are you sure you want to visit Dashboard?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Utils.showProgressDialog(instance);

                        startActivity(new Intent(instance, DashBoardActivity.class));
                        finish();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case get_single_question_paper:

                    questionsList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {
                                parseData(jDataArray);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }


    private void parseData(JSONArray jsonArray) {

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                preQueObj = new PrelimQuestionListModel();
                preQueObj.setPos(jsonObject.getString("pos"));
                JSONArray jPrelimListArray = jsonObject.getJSONArray("prelimList");
                for (int j = 0; j < jPrelimListArray.length(); j++) {
                    JSONObject jPreListObjest = jPrelimListArray.getJSONObject(j);
                    setQueType = (SetPaperQuestionAndTypes) jParser.parseJson(jPreListObjest, new SetPaperQuestionAndTypes());
                    preQueObj.prelimSetList.add(setQueType);
                }
                questionsList.add(preQueObj);
                preQueObj.prelimSetList.get(0).isHeader = true;


                List<String> items = Arrays.asList(preQueObj.getPos().split("\\s*,\\s*"));
                stringArrayListHashMap.clear();
                for (int k = 0; k < items.size(); k++) {
                    stringArrayListHashMap.put(items.get(k), preQueObj.prelimSetList);
                }
                myViewPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager(), questionsList.size());
                viewPager.setAdapter(myViewPagerAdapter);
                final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                        .getDisplayMetrics());
                viewPager.setPageMargin(pageMargin);
                myViewPagerAdapter.notifyDataSetChanged();
                tv_pager_count.setText(String.valueOf(1 + "/" + questionsList.size()));

                viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        tv_pager_count.setText(String.valueOf(position + 1 + "/" + questionsList.size()));
                        if ((questionsList.size() - 1) == position) {

                            float_right_button.hide();
                        } else {
                            float_right_button.show();
                        }
                        if (position == 0) {
                            float_left_button.hide();
                        } else {
                            float_left_button.show();
                        }
                    }
                });
                Utils.hideProgressDialog();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (questionsList.size() == 1) {
            float_left_button.hide();
            float_right_button.hide();
        } else {
            float_left_button.hide();
        }
    }

    public class MyViewPagerAdapter extends FragmentPagerAdapter {
        int mNumOfTabs;
        ArrayList<SetPaperQuestionAndTypes> arrayList = new ArrayList<>();

        public MyViewPagerAdapter(FragmentManager fm, int i) {
            super(fm);
            this.mNumOfTabs = i;
        }

        @Override
        public Fragment getItem(int position) {
            return new ISetPaperFragment().newInstance(position);
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return String.valueOf(position + 1);
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
