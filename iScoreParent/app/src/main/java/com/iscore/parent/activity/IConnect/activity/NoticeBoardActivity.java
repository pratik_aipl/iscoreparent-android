package com.iscore.parent.activity.IConnect.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.iscore.parent.activity.DashBoardActivity;
import com.iscore.parent.activity.IConnect.adapter.NoticeBoardAdapter;
import com.iscore.parent.activity.IConnect.NoticeBoard;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.db.MyDBManager;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NoticeBoardActivity extends AppCompatActivity implements AsynchTaskListner {
    public NoticeBoardActivity instance;
    public NoticeBoard noticObj;
    public App app;
    public JsonParserUniversal jParser;
    public NoticeBoardAdapter adapter;
    public ArrayList<NoticeBoard> noticArray = new ArrayList<>();

    public RecyclerView recycler_notice;
    public ImageView img_back;
    public TextView tv_title;

    public MyDBManager mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_notice_board);
        Utils.logUser();

        instance = this;
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        mDb = MyDBManager.getInstance(instance);
        mDb.open(instance);
        mDb.dbQuery("UPDATE notification SET isTypeOpen = 1,isOpen=1 WHERE ModuleType = 'notice_board' ");
        recycler_notice = (RecyclerView) findViewById(R.id.recycler_notice);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("Notice Board");
        new CallRequest(instance).get_notice_board();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case get_notice_board:

                    noticArray.clear();
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (jDataArray != null && jDataArray.length() > 0) {

                                for (int i = 0; i < jDataArray.length(); i++) {
                                    JSONObject jpaper = jDataArray.getJSONObject(i);
                                    noticObj = (NoticeBoard) jParser.parseJson(jpaper, new NoticeBoard());
                                    noticArray.add(noticObj);
                                }
                                setupRecyclerView();
                                Utils.hideProgressDialog();

                            }

                        } else {
                            Utils.hideProgressDialog();
                            showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;

            }


        }
    }

    private void setupRecyclerView() {
        final Context context = recycler_notice.getContext();
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_notice.setLayoutAnimation(controller);
        recycler_notice.scheduleLayoutAnimation();
        recycler_notice.setLayoutManager(new LinearLayoutManager(context));
        adapter = new NoticeBoardAdapter(noticArray, context);
        recycler_notice.setAdapter(adapter);
        //recycler_notice.addItemDecoration(new ItemOffsetDecoration(spacing));
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}

