package com.iscore.parent.activity.IConnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.iscore.parent.activity.IConnect.activity.CctTestResultActivity;
import com.iscore.parent.activity.IConnect.EMcqPaper;
import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class CctTestAdapter extends RecyclerView.Adapter<CctTestAdapter.MyViewHolder> implements Filterable {
    private static final String TAG = "CctTestAdapter";
    private List<EMcqPaper> paperTypeList;
    private List<EMcqPaper> paperTypeListCopy;
    public Context context;
    public int SubjectID;
    public String SubjectName;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Log.d(TAG, "performFiltering: " + charString);
                if (charString.isEmpty()) {
                    paperTypeListCopy = paperTypeList;
                } else {
                    List<EMcqPaper> filteredList = new ArrayList<>();
                    for (EMcqPaper row : paperTypeList) {
                        if (row.getSubjectName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    paperTypeListCopy = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = paperTypeListCopy;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                paperTypeListCopy = (List<EMcqPaper>) filterResults.values;
                Log.d(TAG, "publishResults: " + paperTypeListCopy.size());
                notifyDataSetChanged();
            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_month, tv_expdate, tv_take_test, tv_date;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = (TextView) view.findViewById(R.id.tv_subject_name);
            tv_month = (TextView) view.findViewById(R.id.tv_month);
            tv_expdate = (TextView) view.findViewById(R.id.tv_expdate);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_take_test = (TextView) view.findViewById(R.id.tv_take_test);
        }
    }


    public CctTestAdapter(List<EMcqPaper> paperTypeList, Context context) {
        this.paperTypeList = paperTypeList;
        this.paperTypeListCopy = paperTypeList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_serach_imcq_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final EMcqPaper cons = paperTypeListCopy.get(position);
        holder.tv_subject_name.setText(cons.getSubjectName());
        String startDate = "";
        String endDate = "";
        try {
            startDate = cons.getModifiedOn();
            startDate = startDate.substring(0, 10);
            endDate = cons.getExpiryDate();
            endDate = endDate.substring(0, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startDate = Utils.changeDateToMMDDYYYY(startDate);
        holder.tv_month.setText(startDate.substring(3, 6));
        holder.tv_date.setText(startDate.substring(0, 2));
        holder.tv_expdate.setText(Utils.changeDateToMMDDYYYY(endDate));
        if (cons.getTakenTest().equalsIgnoreCase("0")) {
            holder.tv_take_test.setText("TAKE TEST");
            holder.tv_take_test.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.tv_take_test.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
             /*       Intent intent=new Intent(context,CctTestActivity.class);
                    intent.putExtra("id",cons.getPaperID());
                    context.startActivity(intent);
        */
                }
            });

        } else {
            //  holder.itemView.setVisibility(View.GONE);
            holder.tv_take_test.setText("VIEW REPORT");
            holder.tv_take_test.setTextColor(context.getResources().getColor(R.color.colorRed));
            holder.tv_take_test.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, CctTestResultActivity.class)
                            .putExtra("type", "1")
                            .putExtra("taken_id", cons.getPaperID())
                            .putExtra("subject_name", cons.getSubjectName()));
                }
            });

        }
    }


    @Override
    public int getItemCount() {
        return paperTypeListCopy.size();
    }

}



