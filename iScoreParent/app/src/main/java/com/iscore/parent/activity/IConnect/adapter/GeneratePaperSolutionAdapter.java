package com.iscore.parent.activity.IConnect.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iscore.parent.activity.IConnect.fragment.ISetPaperFragment;
import com.iscore.parent.model.SetPaperQuestionAndTypes;
import com.iscore.parent.R;
import com.iscore.parent.utils.IScoreWebView;
import com.iscore.parent.utils.Utils;

import java.util.ArrayList;

import static android.support.v7.widget.RecyclerView.ViewHolder;

/**
 * Created by empiere-vaibhav on 1/8/2018.
 */

public class GeneratePaperSolutionAdapter extends RecyclerView.Adapter<ViewHolder> {

    private ArrayList<SetPaperQuestionAndTypes> stringArrayListHashMap = new ArrayList<>();
    ;
    public Context context;
    int clickedPos = -1;
    private LayoutInflater mInflater;
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_FOOTER = 2;

    public GeneratePaperSolutionAdapter(ArrayList<SetPaperQuestionAndTypes> moviesList, Context context) {
        this.stringArrayListHashMap = moviesList;
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            return new ViewHolderHeader(mInflater.inflate(R.layout.custom_generate_paper_raw, parent, false));
        } else if (viewType == VIEW_TYPE_FOOTER) {
            return new ViewHolderFooter(mInflater.inflate(R.layout.custom_generate_paper_footer, parent, false));
        } else {
            return new ViewHolderContent(mInflater.inflate(R.layout.custom_generate_paper_child_raw, parent, false));
        }
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return stringArrayListHashMap.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderHeader offerHolder = (ViewHolderHeader) holder;
                bindHeaderHolder(offerHolder, position);
                break;
            case 1:
                ViewHolderContent addrHolder = (ViewHolderContent) holder;
                bindContentHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolderContent extends ViewHolder {
        TextView tv_question, tvQueNO, btn_solution;
        public LinearLayout lin_solution;
        public IScoreWebView qWebView, sWebView;

        public ViewHolderContent(View v1) {
            super(v1);
            btn_solution = (TextView) v1.findViewById(R.id.btn_solution);
            lin_solution = (LinearLayout) v1.findViewById(R.id.lin_solution);

            tv_question = (TextView) v1.findViewById(R.id.tv_question);
            tvQueNO = (TextView) v1.findViewById(R.id.tvQueNO);
            qWebView = (IScoreWebView) v1.findViewById(R.id.qWebView);
            sWebView = (IScoreWebView) v1.findViewById(R.id.sWebView);
        }
    }

    class ViewHolderHeader extends ViewHolder {
        TextView tv_question, tvQueNO, btn_solution, tv_que_type;
        public LinearLayout lin_solution;
        public IScoreWebView qWebView, sWebView;

        public ViewHolderHeader(View v2) {
            super(v2);
            btn_solution = (TextView) v2.findViewById(R.id.btn_solution);
            lin_solution = (LinearLayout) v2.findViewById(R.id.lin_solution);

            tv_question = (TextView) v2.findViewById(R.id.tv_question);
            tv_que_type = (TextView) v2.findViewById(R.id.tv_que_type);
            tvQueNO = (TextView) v2.findViewById(R.id.tvQueNO);
            qWebView = (IScoreWebView) v2.findViewById(R.id.qWebView);
            sWebView = (IScoreWebView) v2.findViewById(R.id.sWebView);
        }

    }

    public SetPaperQuestionAndTypes getItem(int position) {

        return stringArrayListHashMap.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).isHeader) {
            return VIEW_TYPE_HEADER;

        } else {
            if (position == (stringArrayListHashMap.size() - 1)) {
                return VIEW_TYPE_FOOTER;
            } else {
                return VIEW_TYPE_ITEM;
            }
        }
    }

    @SuppressLint("NewApi")
    public void bindHeaderHolder(final ViewHolderHeader headerHolder, final int pos) {
        SetPaperQuestionAndTypes types = getItem(pos);

        int queNo = pos + 1;
        headerHolder.tvQueNO.setText(String.valueOf((queNo++) + "."));
        headerHolder.tv_que_type.setText(Html.fromHtml("Q-" + (ISetPaperFragment.pageNumber + 1) + " " + stringArrayListHashMap.get(pos).getQuestionType()));
       /* if ((stringArrayListHashMap.get(pos).getQuestion().contains("<img")) || (stringArrayListHashMap.get(pos).getQuestion().contains("<math") || (stringArrayListHashMap.get(pos).getQuestion().contains("<table")))) {
            headerHolder.qWebView.setVisibility(View.VISIBLE);
            headerHolder.qWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getQuestion())));
            headerHolder.tv_question.setVisibility(View.GONE);
        } else {

            headerHolder.tv_question.setText(Html.fromHtml(stringArrayListHashMap.get(pos).getQuestion()));
        }*/
        headerHolder.qWebView.setVisibility(View.VISIBLE);

        headerHolder.tv_question.setVisibility(View.GONE);
       headerHolder.qWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getQuestion())));

        headerHolder.btn_solution.setOnClickListener(new View.OnClickListener()

        {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!stringArrayListHashMap.get(pos).isSolution) {
                    stringArrayListHashMap.get(pos).isSolution = true;

                    // headerHolder.lin_solution.setVisibility(View.VISIBLE);
                    headerHolder.btn_solution.setText("    Hide Solution    ");
                    headerHolder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorWhite));
                    headerHolder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_button));
                    headerHolder.sWebView.setVisibility(View.VISIBLE);
                    headerHolder.sWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getAnswer())));
                } else {
                    stringArrayListHashMap.get(pos).isSolution = false;
                    //  headerHolder.lin_solution.setVisibility(View.GONE);
                    headerHolder.btn_solution.setText("   View Solution   ");
                    headerHolder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    headerHolder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_line));

                }
            }
        });
//        headerHolder.qWebView.getSettings().setJavaScriptEnabled(true);
//
//        headerHolder.qWebView.getSettings().setAllowFileAccess(true);
//        headerHolder.qWebView.getSettings().setJavaScriptEnabled(true);
//        headerHolder.qWebView.getSettings().setBuiltInZoomControls(true);
//        headerHolder.qWebView.getSettings().setDomStorageEnabled(true);
//        headerHolder.qWebView.getSettings().setUseWideViewPort(true);
//        headerHolder.qWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
//        headerHolder.qWebView.getSettings().setLoadWithOverviewMode(true);
        headerHolder.qWebView.setInitialScale(1);
//        WebSettings settings = headerHolder.qWebView.getSettings();
//        settings.setAllowFileAccess(true);
//        settings.setAllowContentAccess(true);
//        settings.setAllowFileAccessFromFileURLs(true);
//        settings.setLoadWithOverviewMode(true);
//        settings.setUseWideViewPort(true);
//        settings.setBuiltInZoomControls(true);
//        settings.setJavaScriptEnabled(true);
//        settings.setSupportZoom(true);
//        settings.setDomStorageEnabled(true);
        Utils.webSettings(headerHolder.qWebView);
        headerHolder.qWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void bindContentHolder(final ViewHolderContent holder, final int pos) {
        final SetPaperQuestionAndTypes types = getItem(pos);
        int queNo = pos + 1;
        holder.tvQueNO.setText(String.valueOf((queNo++) + "."));

        if ((stringArrayListHashMap.get(pos).getQuestion().contains("<img")) || (stringArrayListHashMap.get(pos).getQuestion().contains("<math") || (stringArrayListHashMap.get(pos).getQuestion().contains("<table")))) {
            holder.qWebView.setVisibility(View.VISIBLE);
            holder.qWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getQuestion())));
            holder.tv_question.setVisibility(View.GONE);
        } else {
            holder.tv_question.setText(Html.fromHtml(stringArrayListHashMap.get(pos).getQuestion()));
        }

        holder.btn_solution.setOnClickListener(new View.OnClickListener()

        {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!stringArrayListHashMap.get(pos).isSolution) {
                    stringArrayListHashMap.get(pos).isSolution = true;

                    holder.lin_solution.setVisibility(View.VISIBLE);
                    holder.btn_solution.setText("    Hide Solution    ");
                    holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorWhite));
                    holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_button));
                    holder.sWebView.setVisibility(View.VISIBLE);
                    holder.sWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getAnswer())));
                } else {
                    stringArrayListHashMap.get(pos).isSolution = false;
                    holder.lin_solution.setVisibility(View.GONE);
                    holder.btn_solution.setText("   View Solution   ");
                    holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_line));

                }
            }
        });
//        holder.qWebView.getSettings().setJavaScriptEnabled(true);
//
//        holder.qWebView.getSettings().setAllowFileAccess(true);
//        holder.qWebView.getSettings().setJavaScriptEnabled(true);
//        holder.qWebView.getSettings().setBuiltInZoomControls(true);
//        holder.qWebView.getSettings().setDomStorageEnabled(true);
//        holder.qWebView.getSettings().setUseWideViewPort(true);
//        holder.qWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
//        holder.qWebView.getSettings().setLoadWithOverviewMode(true);
        holder.qWebView.setInitialScale(1);
        Utils.webSettings(holder.qWebView);
        holder.qWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
//        WebSettings settings = holder.qWebView.getSettings();
//        settings.setAllowFileAccess(true);
//        settings.setAllowContentAccess(true);
//        settings.setAllowFileAccessFromFileURLs(true);
//        settings.setLoadWithOverviewMode(true);
//        settings.setUseWideViewPort(true);
//        settings.setBuiltInZoomControls(true);
//        settings.setJavaScriptEnabled(true);
//        settings.setSupportZoom(true);
//        settings.setDomStorageEnabled(true);


    }

    class ViewHolderFooter extends ViewHolder {
        TextView tv_question, tvQueNO, btn_solution;
        public LinearLayout lin_solution;
        public IScoreWebView qWebView, sWebView;

        public ViewHolderFooter(View v1) {
            super(v1);
            btn_solution = (TextView) v1.findViewById(R.id.btn_solution);
            lin_solution = (LinearLayout) v1.findViewById(R.id.lin_solution);

            tv_question = (TextView) v1.findViewById(R.id.tv_question);
            tvQueNO = (TextView) v1.findViewById(R.id.tvQueNO);
            qWebView = (IScoreWebView) v1.findViewById(R.id.qWebView);
            sWebView = (IScoreWebView) v1.findViewById(R.id.sWebView);
        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {
        final SetPaperQuestionAndTypes types = getItem(pos);
        int queNo = pos + 1;
        holder.tvQueNO.setText(String.valueOf((queNo++) + "."));

        if ((stringArrayListHashMap.get(pos).getQuestion().contains("<img")) || (stringArrayListHashMap.get(pos).getQuestion().contains("<math") || (stringArrayListHashMap.get(pos).getQuestion().contains("<table")))) {
            holder.qWebView.setVisibility(View.VISIBLE);
            holder.qWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getQuestion())));
            holder.tv_question.setVisibility(View.GONE);
        } else {
            holder.tv_question.setText(Html.fromHtml(stringArrayListHashMap.get(pos).getQuestion()));
        }

        holder.btn_solution.setOnClickListener(new View.OnClickListener()

        {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!stringArrayListHashMap.get(pos).isSolution) {
                    stringArrayListHashMap.get(pos).isSolution = true;

                    holder.lin_solution.setVisibility(View.VISIBLE);
                    holder.btn_solution.setText("    Hide Solution    ");
                    holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorWhite));
                    holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_button));
                    holder.sWebView.setVisibility(View.VISIBLE);
                    holder.sWebView.loadHtmlFromLocal(getPrelimTestHeader(Utils.replaceImagesPath(stringArrayListHashMap.get(pos).getAnswer())));
                } else {
                    stringArrayListHashMap.get(pos).isSolution = false;
                    holder.lin_solution.setVisibility(View.GONE);
                    holder.btn_solution.setText("   View Solution   ");
                    holder.btn_solution.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.btn_solution.setBackground(context.getResources().getDrawable(R.drawable.rounded_blue_line));

                }
            }
        });
//        holder.qWebView.getSettings().setJavaScriptEnabled(true);
//
//        holder.qWebView.getSettings().setAllowFileAccess(true);
//        holder.qWebView.getSettings().setJavaScriptEnabled(true);
//        holder.qWebView.getSettings().setBuiltInZoomControls(true);
//        holder.qWebView.getSettings().setDomStorageEnabled(true);
//        holder.qWebView.getSettings().setUseWideViewPort(true);
//        holder.qWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
//        holder.qWebView.getSettings().setLoadWithOverviewMode(true);
        holder.qWebView.setInitialScale(1);
        Utils.webSettings(holder.qWebView);
        holder.qWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
//        WebSettings settings = holder.qWebView.getSettings();
//        settings.setAllowFileAccess(true);
//        settings.setAllowContentAccess(true);
//        settings.setAllowFileAccessFromFileURLs(true);
//        settings.setLoadWithOverviewMode(true);
//        settings.setUseWideViewPort(true);
//        settings.setBuiltInZoomControls(true);
//        settings.setJavaScriptEnabled(true);
//        settings.setSupportZoom(true);
//        settings.setDomStorageEnabled(true);
    }

    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else
            Log.i("TAG " + tag + " -->", str);
    }

    public static String getPrelimTestHeader(String html) {


        String HTML = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "    <head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "        <title>Question List</title>\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" +
               "        <style type=\"text/css\">\n" +
                "            body {" +
                " width: 100% !important } \n" +

                "            table td{ width: auto !important } \n" +
                "            table th{ width: auto !important } \n" +
                "            table{ width: 100% !important }\n" +
                "            .table {\n" +
                "                width: 100% !important \n" +
                "            }\n" +
                "            .table tr td {\n" +
                "                vertical-align: top;\n" +
                "            }\n" +
                "            .question p:first-child {\n" +
                "                margin-top: 0;\n" +
                "            }\n" +

                "            body * {\n" +
                "                font-size: 12px\"\n" +
                "            }\n" +

                "            .answer p:first-child {\n" +
                "                margin-top: 0;\n" +
                "            }\n" +
                "            body {\n" +
                //  "                width: 100%;\n" +
                "                height: auto;\n" +
                "                margin: 0;\n" +
                "                padding: 0;\n" +
                "                font: 12pt " +
                "            }\n" +
                "            * {\n" +
                "                box-sizing: border-box;\n" +
                "                -moz-box-sizing: border-box;\n" +
                "            }\n" +
                "            .page {\n" +
                "                width: 210mm;\n" +
                "                min-height: 297mm;\n" +
                "                padding: 0mm;\n" +
                "                margin: 10mm auto;\n" +
                "                /*border: 1px #D3D3D3 solid;*/\n" +
                "                /*border-radius: 5px;*/\n" +
                "                background: white;\n" +
                "                /*box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);*/\n" +
                "            }\n" +
                "            .subpage {\n" +
                "                padding: 1cm;\n" +
                "                /*border: 5px red solid;*/\n" +
                "                height: 257mm;\n" +
                "                /*outline: 2cm #FFEAEA solid;*/\n" +
                "            }\n" +
                "\n" +
                "            @page {\n" +
                "                size: A4;\n" +
                "                margin: 0;\n" +
                "            }\n" +
                "            @media print {\n" +
                "                html, body {\n" +
                "                    width: 210mm;\n" +
                "                    height: 297mm;        \n" +
                "                }\n" +
                "                .page {\n" +
                "                    margin: 0;\n" +
                "                    border: initial;\n" +
                "                    border-radius: initial;\n" +
                "                    width: initial;\n" +
                "                    min-height: initial;\n" +
                "                    /*box-shadow: initial;*/\n" +
                "                    background: initial;\n" +
                "                    /*page-break-after: always;*/\n" +
                "                }\n" +
                "            }\n" +
                "      .tablenew tr td {\n" +

                "                 vertical-align: top; \n" +
                "              } " +
                "      .table tr.mtc td p{\n" +
                "                    margin: 0;\n" +
                "                 display: inline-block; \n" +
                "              } " +
                "table img{\n" +
                "width:100% !important;\n" +
                "max-width:100% !important;\n" +
                "}\n" +

                "        </style>\n" +
                "\n" +
                "<link rel=\"stylesheet\" href=\"file:///android_asset/mathscribe/jqmath-0.4.3.css\">" +
                "<link rel=\"stylesheet\" href=\"file:///android_asset/stylesheet.css\">"+

                "\n" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jquery-1.4.3.min.js\"></script>\n" +
                "\n" +
                "<script type=\"text/javascript\" src=\"file:///android_asset/mathscribe/jqmath-etc-0.4.6.min.js\"></script>\n" +
                "    </head>\n" +
                "    <body>\n" +html+
                "     </body></html>";
        longInfo(HTML, "HTML1");

        return HTML;


    }

}
