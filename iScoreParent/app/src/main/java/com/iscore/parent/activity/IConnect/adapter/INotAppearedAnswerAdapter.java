package com.iscore.parent.activity.IConnect.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iscore.parent.model.EvaluterMcqTestMain;
import com.iscore.parent.model.McqQuestion;
import com.iscore.parent.model.TestSummary;
import com.iscore.parent.R;
import com.iscore.parent.utils.IScoreWebView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by empiere-vaibhav on 2/3/2018.
 */

public class INotAppearedAnswerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    private LayoutInflater mInflater;
    private ArrayList<EvaluterMcqTestMain> mItems;
    private View mHeaderView;
    McqQuestion mcqQueObj;
    private List<TestSummary> moviesList;
    public boolean isPosition = true;
    public static Context mContext;
    public static boolean isSolution = true;

    public INotAppearedAnswerAdapter(Context context, ArrayList<EvaluterMcqTestMain> items, View headerView) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        mContext = context;
        mHeaderView = headerView;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public EvaluterMcqTestMain getItem(int position) {

        return mItems.get(position);
    }

    @Override
    public int getItemViewType(int position) {

        if (getItem(position).isHeader) {
            return VIEW_TYPE_HEADER;
        } else {
            return VIEW_TYPE_ITEM;
        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            return new HeaderViewHolder(mHeaderView);
        } else {
            return new ItemViewHolder(mInflater.inflate(R.layout.custom_correct_question_raw, parent, false));
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {

            case 1:
                ItemViewHolder addrHolder = (ItemViewHolder) holder;
                bindItemHolder(mItems.get(position), addrHolder, position);
                break;
        }

    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public TextView tvQueNO, tv_solution,
                tv_question, tv_a, tv_b, tv_c, tv_d, ans_a, ans_b, ans_c, ans_d, tv_level;
        public IScoreWebView qWbeView, aWebView, bWebView, cWebView, dWebView;
        public LinearLayout lin_a, lin_b, lin_c, lin_d;
        public LinearLayout lin_solution;

        public ItemViewHolder(View view) {
            super(view);
            tvQueNO = (TextView) view.findViewById(R.id.tvQueNO);
            tv_question = (TextView) view.findViewById(R.id.tv_q);
            tv_solution = (TextView) view.findViewById(R.id.tv_solution);
            lin_solution = (LinearLayout) view.findViewById(R.id.lin_solution);

            qWbeView = (IScoreWebView) view.findViewById(R.id.qWebView);
            aWebView = (IScoreWebView) view.findViewById(R.id.aWebView);
            bWebView = (IScoreWebView) view.findViewById(R.id.bWebView);
            cWebView = (IScoreWebView) view.findViewById(R.id.cWebView);
            dWebView = (IScoreWebView) view.findViewById(R.id.dWebView);
            lin_a = (LinearLayout) view.findViewById(R.id.lin_a);
            lin_b = (LinearLayout) view.findViewById(R.id.lin_b);
            lin_c = (LinearLayout) view.findViewById(R.id.lin_c);
            lin_d = (LinearLayout) view.findViewById(R.id.lin_d);
            tv_question = (TextView) view.findViewById(R.id.tv_q);
            tv_a = (TextView) view.findViewById(R.id.tv_a);
            tv_b = (TextView) view.findViewById(R.id.tv_b);
            tv_c = (TextView) view.findViewById(R.id.tv_c);
            tv_d = (TextView) view.findViewById(R.id.tv_d);
            ans_a = (TextView) view.findViewById(R.id.a_ans);
            ans_b = (TextView) view.findViewById(R.id.b_ans);
            ans_c = (TextView) view.findViewById(R.id.c_ans);
            ans_d = (TextView) view.findViewById(R.id.d_ans);
            tv_level = (TextView) view.findViewById(R.id.tv_level);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    void bindItemHolder(EvaluterMcqTestMain mcqQueObj, final ItemViewHolder itemHolder, final int position) {



        itemHolder.tvQueNO.setText(position + "");
        if (mcqQueObj.getQuestion().contains("<img") || mcqQueObj.getQuestion().contains("<math") || mcqQueObj.getQuestion().contains("<table")) {
            itemHolder.qWbeView.setVisibility(View.VISIBLE);
            itemHolder.tv_question.setVisibility(View.GONE);
            itemHolder.qWbeView.loadHtmlFromLocal(mcqQueObj.getQuestion());
        } else {
            itemHolder.tv_question.setText(Html.fromHtml(mcqQueObj.getQuestion()));

        }
        itemHolder.tv_level.setText("(Level " + mcqQueObj.getLevel() + ")");

        itemHolder.tv_a.setBackgroundResource(0);
        itemHolder.tv_b.setBackgroundResource(0);
        itemHolder.tv_c.setBackgroundResource(0);
        itemHolder.tv_d.setBackgroundResource(0);

        isSolution = false;
        itemHolder.tv_solution.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        itemHolder.tv_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_line));

        try {
            if (!mcqQueObj.optionArray.get(0).getOptions().isEmpty() && !mcqQueObj.optionArray.get(0).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_a.setVisibility(View.VISIBLE);

                if (mcqQueObj.optionArray.get(0).getOptions().contains("<img") || mcqQueObj.optionArray.get(0).getOptions().contains("<math") || mcqQueObj.optionArray.get(0).getOptions().contains("<table")) {
                    itemHolder.aWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_a.setVisibility(View.GONE);
                    itemHolder.aWebView.loadHtmlFromLocal(mcqQueObj.optionArray.get(0).getOptions());
                } else {
                    itemHolder.ans_a.setText(Html.fromHtml(mcqQueObj.optionArray.get(0).getOptions()).toString().trim());

                }

                if (mcqQueObj.optionArray.get(0).isCorrect.equalsIgnoreCase("1")) {
                    Log.i("TAG", "Position :->" + position + "  -> 0");

                    itemHolder.tv_a.setBackgroundResource(R.drawable.circle_button_right);
                }
            }


            if (!mcqQueObj.optionArray.get(1).getOptions().isEmpty() && !mcqQueObj.optionArray.get(1).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_b.setVisibility(View.VISIBLE);

                if (mcqQueObj.optionArray.get(1).getOptions().contains("<img") || mcqQueObj.optionArray.get(1).getOptions().contains("<math") || mcqQueObj.optionArray.get(1).getOptions().contains("<table")) {
                    itemHolder.bWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_b.setVisibility(View.GONE);
                    itemHolder.bWebView.loadHtmlFromLocal(mcqQueObj.optionArray.get(1).getOptions());
                } else {
                    itemHolder.ans_b.setText(Html.fromHtml(mcqQueObj.optionArray.get(1).getOptions()).toString().trim());
                }

                if (mcqQueObj.optionArray.get(1).isCorrect.equalsIgnoreCase("1")) {
                    Log.i("TAG", "Position :->" + position + "  -> 1");
                    itemHolder.tv_b.setBackgroundResource(R.drawable.circle_button_right);
                }
            }


            if (!mcqQueObj.optionArray.get(2).getOptions().isEmpty() && !mcqQueObj.optionArray.get(2).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_c.setVisibility(View.VISIBLE);

                if (mcqQueObj.optionArray.get(2).getOptions().contains("<img") || mcqQueObj.optionArray.get(2).getOptions().contains("<math") || mcqQueObj.optionArray.get(2).getOptions().contains("<table")) {
                    itemHolder.cWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_c.setVisibility(View.GONE);
                    itemHolder.cWebView.loadHtmlFromLocal(mcqQueObj.optionArray.get(2).getOptions());
                } else {
                    // Html.fromHtml("<html><body text-align:center>"+mcqQueObj.optionArray.get(2).getOptions().toString().trim());
                    itemHolder.ans_c.setText(Html.fromHtml(mcqQueObj.optionArray.get(2).getOptions()).toString().trim());
                }

                if (mcqQueObj.optionArray.get(2).isCorrect.equalsIgnoreCase("1")) {
                    Log.i("TAG", "Position :->" + position + "  -> 2");
                    itemHolder.tv_c.setBackgroundResource(R.drawable.circle_button_right);
                }
            }


            if (!mcqQueObj.optionArray.get(3).getOptions().isEmpty() && !mcqQueObj.optionArray.get(3).getOptions().equalsIgnoreCase("")) {
                itemHolder.lin_d.setVisibility(View.VISIBLE);

                if (mcqQueObj.optionArray.get(3).getOptions().contains("<img") || mcqQueObj.optionArray.get(3).getOptions().contains("<math") || mcqQueObj.optionArray.get(3).getOptions().contains("<table")) {
                    itemHolder.dWebView.setVisibility(View.VISIBLE);
                    itemHolder.ans_d.setVisibility(View.GONE);
                    itemHolder.dWebView.loadHtmlFromLocal(mcqQueObj.optionArray.get(3).getOptions());
                } else {
                    itemHolder.ans_d.setText(Html.fromHtml(mcqQueObj.optionArray.get(3).getOptions()).toString().trim());
                }

                if (mcqQueObj.optionArray.get(3).isCorrect.equalsIgnoreCase("1")) {
                    Log.i("TAG", "Position :->" + position + "  -> 3");
                    itemHolder.tv_d.setBackgroundResource(R.drawable.circle_button_right);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        itemHolder.tv_solution.setOnClickListener(new View.OnClickListener()

        {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if (!isSolution) {
                    isSolution = true;

                    itemHolder.lin_solution.setVisibility(View.VISIBLE);
                    itemHolder.tv_solution.setText("    Back    ");
                    itemHolder.tv_solution.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
                    itemHolder.tv_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_button));


                } else {
                    isSolution = false;

                    itemHolder.lin_solution.setVisibility(View.GONE);
                    itemHolder.tv_solution.setText("  Solution  ");
                    itemHolder.tv_solution.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    itemHolder.tv_solution.setBackground(mContext.getResources().getDrawable(R.drawable.rounded_blue_line));

                }
                // notifyDataSetChanged();
            }
        });
    }


}

