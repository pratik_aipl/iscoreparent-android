package com.iscore.parent.activity.IConnect.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.androidquery.AQuery;
import com.iscore.parent.activity.IConnect.NoticeBoard;
import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

import java.util.List;

/**
 * Created by Karan - Empiere on 2/28/2018.
 */

public class NoticeBoardAdapter extends RecyclerView.Adapter<NoticeBoardAdapter.MyViewHolder> {
    public NoticeBoard noticObj;
    private List<NoticeBoard> noticArrayList;
    public Context context;
    public int SubjectID;
    public String SubjectName;
    public AQuery aQuery;


    public NoticeBoardAdapter(List<NoticeBoard> noticArrayList, Context context) {
        this.noticArrayList = noticArrayList;
        this.context = context;
        aQuery = new AQuery(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_notice_board_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_date, tv_discription;
        ImageView mNoticeImage;

        public MyViewHolder(View view) {
            super(view);
            mNoticeImage = view.findViewById(R.id.mNoticeImage);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_discription = (TextView) view.findViewById(R.id.tv_description);
        }
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        noticObj = noticArrayList.get(position);
        holder.tv_title.setText(noticObj.getTitle());
        holder.tv_date.setText(Utils.changeDateAndTimeFormet(noticObj.getCreatedOn()));
        holder.tv_discription.setText(noticObj.getDescription());
        if (!TextUtils.isEmpty(noticObj.getImage()) && !noticObj.getImage().equalsIgnoreCase("https://evaluater.parshvaa.com/notice_board/")) {
            int dimensionInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, context.getResources().getDisplayMetrics());
            holder.mNoticeImage.getLayoutParams().height = dimensionInDp;
//            holder.mNoticeImage.getLayoutParams().width = dimensionInDp;
            holder.mNoticeImage.requestLayout();
            aQuery.id(holder.mNoticeImage).progress(null).image( noticObj.getImage(), true, true, holder.mNoticeImage.getLayoutParams().width, R.drawable.profile, null, 0, 0.0f);
        } else {
            int dimensionInDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, context.getResources().getDisplayMetrics());
            holder.mNoticeImage.getLayoutParams().height = dimensionInDp;
            holder.mNoticeImage.requestLayout();
        }
    }

    @Override
    public int getItemCount() {
        return noticArrayList.size();
    }

}
