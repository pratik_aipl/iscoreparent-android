package com.iscore.parent.activity.IConnect.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iscore.parent.activity.IConnect.EMcqPaper;
import com.iscore.parent.activity.IConnect.activity.IGeneratePaperActivity;
import com.iscore.parent.activity.IConnect.activity.ISetPaperActivity;
import com.iscore.parent.activity.IConnect.ITestPaper;
import com.iscore.parent.R;
import com.iscore.parent.model.Subject;
import com.iscore.parent.utils.ProgressBarAnimation;
import com.iscore.parent.utils.Utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/**
 * Created by empiere-vaibhav on 1/31/2018.
 */

public class TestPaperAdapter extends RecyclerView.Adapter<TestPaperAdapter.MyViewHolder> implements Filterable {
    private static final String TAG = "TestPaperAdapter";
    public List<ITestPaper> paperTypeList;
    public List<ITestPaper> paperTypeListCopy;
    public Context context;
    public int SubjectID;
    public String SubjectName;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Log.d(TAG, "performFiltering: " + charString);
                if (charString.isEmpty()) {
                    paperTypeListCopy = paperTypeList;
                } else {
                    List<ITestPaper> filteredList = new ArrayList<>();
                    for (ITestPaper row : paperTypeList) {
                        if (row.getSubjectName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    paperTypeListCopy = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = paperTypeListCopy;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                paperTypeListCopy = (ArrayList<ITestPaper>) filterResults.values;
                Log.d(TAG, "publishResults: " + paperTypeListCopy.size());
                notifyDataSetChanged();
            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_month, tv_marks, tv_date;
        public ImageView img_watch;
        public ProgressBar pbar_accuracy;
        public RelativeLayout rel_img;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = (TextView) view.findViewById(R.id.tv_subject_name);
            tv_month = (TextView) view.findViewById(R.id.tv_month);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_marks = (TextView) view.findViewById(R.id.tv_marks);
            img_watch = (ImageView) view.findViewById(R.id.img_watch);

            pbar_accuracy = (ProgressBar) view.findViewById(R.id.pbar_accuracy);
            rel_img = (RelativeLayout) view.findViewById(R.id.rel_img);
        }
    }


    public TestPaperAdapter(List<ITestPaper> paperTypeList, Context context) {
        this.paperTypeList = paperTypeList;
        this.paperTypeListCopy = paperTypeList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_serach_i_practies_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ITestPaper cons = paperTypeListCopy.get(position);
        holder.tv_subject_name.setText(cons.getSubjectName());
        String startDate = "";
        String endDate = "";
        try {
            startDate = cons.getPaperDate();
            startDate = startDate.substring(0, 10);
            endDate = cons.getPaperDate();
            endDate = endDate.substring(0, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startDate = Utils.changeDateToMMDDYYYY(startDate);
        holder.tv_month.setText(startDate.substring(3, 6));
        holder.tv_date.setText(startDate.substring(0, 2));
        try {
            if (cons.getIsStudentMarkUploaded().equals("0") || cons.getIsStudentMarkUploaded().equals("")) {
                holder.pbar_accuracy.setVisibility(View.GONE);
                holder.rel_img.setVisibility(View.GONE);
                holder.tv_marks.setText(cons.getTotalMarks() + " Marks");
            } else {
                holder.pbar_accuracy.setMax(Integer.parseInt(cons.getTotalMarks()));
                ProgressBarAnimation anim = new ProgressBarAnimation(holder.pbar_accuracy, 0, Float
                        .parseFloat(cons.getStudentMark()));
                anim.setDuration(1000);
                holder.pbar_accuracy.startAnimation(anim);
                holder.tv_marks.setText(cons.getStudentMark() + "/" + cons.getTotalMarks() + " Marks");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        holder.tv_marks.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cons.getPaperTypeName().equalsIgnoreCase("Ready Paper")) {
                    Intent intent = new Intent(context, IGeneratePaperActivity.class);
                    intent.putExtra("id", cons.getPaperID());
                    context.startActivity(intent);

                } else if (cons.getPaperTypeName().equalsIgnoreCase("Prelim Paper")) {
                    Intent intent = new Intent(context, IGeneratePaperActivity.class);
                    intent.putExtra("id", cons.getPaperID());
                    context.startActivity(intent);

                } else {
                    Intent intent = new Intent(context, ISetPaperActivity.class);
                    intent.putExtra("id", cons.getPaperID());
                    context.startActivity(intent);

                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return paperTypeListCopy.size();
    }


}



