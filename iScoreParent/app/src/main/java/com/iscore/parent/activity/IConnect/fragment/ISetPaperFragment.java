package com.iscore.parent.activity.IConnect.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;

import com.nineoldandroids.view.ViewPropertyAnimator;
import com.iscore.parent.activity.IConnect.activity.ISetPaperActivity;
import com.iscore.parent.activity.IConnect.adapter.GeneratePaperSolutionAdapter;
import com.iscore.parent.model.GeneratePaper;
import com.iscore.parent.model.SetPaperQuestionAndTypes;
import com.iscore.parent.R;
import com.iscore.parent.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by empiere-vaibhav on 1/31/2018.
 */

public class ISetPaperFragment extends Fragment {
    public View v;
    private static final String ARG_PAGE_NUMBER = "page_number";
    private List<GeneratePaper> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private GeneratePaperSolutionAdapter mAdapter;
    private ArrayList<String> mKeys;

    private ArrayList<SetPaperQuestionAndTypes> typesArrayList = new ArrayList<>();
    private boolean mFabIsShown;
    public ArrayList<SetPaperQuestionAndTypes> tempArray = new ArrayList<>();
    public static int pageNumber = 0;

    public Fragment newInstance(int page) {
        ISetPaperFragment fragment = new ISetPaperFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE_NUMBER, page);
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(
                R.layout.fragment_paper_solution, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_paper);
        final int spacing = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(spacing));


        setupRecyclerView();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    hideFab();
                } else if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    hideFab();

                } else {
                    showFab();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


            }
        });

        return v;

    }
    @Override
    public void setMenuVisibility(final boolean visible) {
        try {
            if (visible) {
                //  pageNumber = ((PreviewRevisionNoteActivity) getActivity()).viewPager.getCurrentItem();
                pageNumber = getArguments().getInt(ARG_PAGE_NUMBER);
                typesArrayList = ISetPaperActivity.questionsList.get(pageNumber).prelimSetList;

                 setupRecyclerView();


            }
        } catch (Exception e) {
            e.printStackTrace();

            typesArrayList = ISetPaperActivity.questionsList.get(pageNumber).prelimSetList;

        }
    }
    public String getKey(int position) {
        return (String) mKeys.get(position);
    }
    private void showFab() {
        if (!mFabIsShown) {
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_left_button).scaleX(1).scaleY(1).setDuration(200).start();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_right_button).scaleX(1).scaleY(1).setDuration(200).start();
            mFabIsShown = true;
        }
    }

    private void hideFab() {
        if (mFabIsShown) {
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_right_button).cancel();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_right_button).scaleX(0).scaleY(0).setDuration(200).start();

            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_left_button).cancel();
            ViewPropertyAnimator.animate(((ISetPaperActivity) getActivity()).float_left_button).scaleX(0).scaleY(0).setDuration(200).start();
            mFabIsShown = false;
        }
    }



    private void setupRecyclerView() {



   //     final Context context = recyclerView.getA();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new GeneratePaperSolutionAdapter(typesArrayList, getActivity());
        recyclerView.setAdapter(mAdapter);

    }
}

