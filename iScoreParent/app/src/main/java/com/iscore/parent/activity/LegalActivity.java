package com.iscore.parent.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

import io.fabric.sdk.android.Fabric;

public class LegalActivity extends AppCompatActivity {
    public LegalActivity indtance;
    public ImageView img_back;
    public TextView tv_title, tv_terms, tv_privacy, tv_linces,tv_refund;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_legal);
        Utils.logUser();
        indtance = this;
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_terms = (TextView) findViewById(R.id.tv_terms);
        tv_privacy = (TextView) findViewById(R.id.tv_privacy);
        tv_linces = (TextView) findViewById(R.id.tv_linces);
        tv_refund = (TextView) findViewById(R.id.tv_refund);
        tv_title.setText("Legal");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        WebView webView = (WebView) findViewById(R.id.web_legal);
        webView.loadUrl("file:///android_asset/Licenses&Copyrights.html");


        tv_linces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(indtance, StaticWebPageActivity.class)
                .putExtra("action","l"));
            }
        });
        tv_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(indtance, StaticWebPageActivity.class)
                        .putExtra("action","p"));
            }
        });
        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(indtance, StaticWebPageActivity.class)
                        .putExtra("action","t"));
            }
        });
        tv_refund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(indtance, StaticWebPageActivity.class)
                        .putExtra("action","r"));
            }
        });
    }
}
