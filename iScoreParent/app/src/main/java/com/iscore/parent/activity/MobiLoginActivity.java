package com.iscore.parent.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.App;
import com.iscore.parent.BuildConfig;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.db.DBTables;
import com.iscore.parent.db.MyDBManager;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.MySharedPref;
import com.iscore.parent.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class MobiLoginActivity extends AppCompatActivity implements AsynchTaskListner {
    private static final String TAG = "MobiLoginActivity";
    public MobiLoginActivity instance;
    public Button btn_login;
    public EditText etMobileNo;
    public String data;
    public App app;
    public String PleyaerID = "";
    public JsonParserUniversal jParser;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE/*, Manifest.permission.READ_PHONE_STATE*/, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION/*,
            Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS*/};
    int PERMISSION_ALL = 1;
    public MyDBManager mDb;
    String otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_mobi_login);
        Utils.logUser();
        if (!Utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

        }
        instance = this;
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);

        jParser = new JsonParserUniversal();
        PleyaerID = Utils.OneSignalPlearID();
        etMobileNo = (EditText) findViewById(R.id.et_mobile);

        if (BuildConfig.DEBUG) {
            etMobileNo.setText("9904365884");
            etMobileNo.setText("7710004570");
        }
        app.mySharedPref = new MySharedPref(MobiLoginActivity.this);
        app = App.getInstance();
        PleyaerID = Utils.OneSignalPlearID();
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etMobileNo.getText().toString().equals("")) {
                    etMobileNo.setError("Enter Valid Mobile Number");
                } else {
                    if (Utils.isNetworkAvailable(MobiLoginActivity.this)) {
                        if (PleyaerID.equalsIgnoreCase("")) {
                            PleyaerID = Utils.OneSignalPlearID();
                            Utils.showProgressDialog(instance);
                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    Utils.removeWorkingDialog(instance);
                                }

                            }, 5000);
                        } else {
                            new CallRequest(MobiLoginActivity.this).getLogin(etMobileNo.getText().toString(), PleyaerID);
                        }
                    } else {
                        Utils.showToast(getResources().getString(R.string.conect_internet), instance);
                    }
                }
            }
        });
    }

    public void removeAllTables() {


        mDb.removeTable("boards");
        mDb.removeTable("boards_paper");
        mDb.removeTable("boards_paper_data");
        mDb.removeTable("board_paper_files");
        mDb.removeTable("chapters");
        mDb.removeTable("examtypes");
        mDb.removeTable("examtype_subject");
        mDb.removeTable("masterquestion");
        mDb.removeTable("mcqoption");
        mDb.removeTable("mcqquestion");
        mDb.removeTable("mediums");

        mDb.removeTable("question_types");
        mDb.removeTable("standards");
        mDb.removeTable("mcqquestion");
        mDb.removeTable("subjects");


        mDb.removeTable("board_paper_download");
        mDb.removeTable("student_set_paper_question_type");
        mDb.removeTable("student_set_paper_detail");
        mDb.removeTable("notification");
        mDb.removeTable("class_evaluater_mcq_test_temp");
        mDb.removeTable("class_evaluater_cct_count");
        mDb.removeTable("updated_masterquetion");
        mDb.removeTable("student_mcq_test_level");
        mDb.removeTable("temp_mcq_test_level");
        mDb.removeTable("student_not_appeared_question");
        mDb.removeTable("student_incorrect_question");
        mDb.removeTable("temp_mcq_test_hdr");
        mDb.removeTable("temp_mcq_test_dtl");
        mDb.removeTable("temp_mcq_test_chapter");
        mDb.removeTable("student_mcq_test_hdr");
        mDb.removeTable("student_mcq_test_dtl");
        mDb.removeTable("student_mcq_test_chapter");
        mDb.removeTable("paper_type");
        mDb.removeTable("exam_type_pattern");
        mDb.removeTable("exam_type_pattern_detail");
        mDb.removeTable("student_question_paper");
        mDb.removeTable("student_question_paper_detail");
        mDb.removeTable("student_question_paper_chapter");


        DBTables dbTables = new DBTables();
        dbTables.createAll(mDb);


    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                switch (request) {
                    case getLogin:
                        Log.d(TAG, "onTaskCompleted: " + result);
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            removeAllTables();
                            Utils.hideProgressDialog();
                            app.mainUser.setStudent_id(jObj.getString("StudentID"));
                            app.mainUser.setPLID(jObj.getString("PLID"));
                            //  otp=jObj.getString("otp");
                            app.mainUser.setRegMobNo(etMobileNo.getText().toString());
                            goAhead();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("message"), this);
                        }
                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Something getting wrong! Please try again later.", instance);

                e.printStackTrace();
            }
        }
    }

    public void goAhead() {
        if (Utils.isNetworkAvailable(MobiLoginActivity.this)) {
            startActivity(new Intent(MobiLoginActivity.this, VerifyOtpActivity.class)
                    .putExtra("MobLogin", "MobLogin")
                    .putExtra("PleyaerID", PleyaerID));
        } else {
            Utils.showToast(getResources().getString(R.string.conect_internet), instance);
        }
    }

}
