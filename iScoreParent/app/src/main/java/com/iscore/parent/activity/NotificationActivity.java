package com.iscore.parent.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.IConnect.activity.IGeneratePaperActivity;
import com.iscore.parent.activity.IConnect.activity.ISetPaperActivity;
import com.iscore.parent.activity.IConnect.activity.NoticeBoardActivity;
import com.iscore.parent.activity.library.Activity.LibrarySubjectActivity;
import com.iscore.parent.App;
import com.iscore.parent.model.NotificationType;
import com.iscore.parent.R;
import com.iscore.parent.db.DBNewQuery;
import com.iscore.parent.db.MyDBManager;
import com.iscore.parent.db.MyDBManagerOneSignal;
import com.iscore.parent.utils.CustomDatabaseQuery;
import com.iscore.parent.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import io.fabric.sdk.android.Fabric;

public class NotificationActivity extends AppCompatActivity {
    public RecyclerView rel_noti;
    public NotiAdapter adapter;
    public NotificationType notiObj;
    public ArrayList<NotificationType> mainArray = new ArrayList<>();
    public App app;
    public ImageView img_back;
    public TextView tv_title;
    public ArrayList<String> NOTIFICATION_ID = new ArrayList<>();
    private MyDBManager mDb;
    public static MyDBManagerOneSignal mDbOneSignal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_notification);
        Utils.logUser();
        app = App.getInstance();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        app.notiCount = 0;
        img_back = findViewById(R.id.img_back);
        tv_title = findViewById(R.id.tv_title);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_title.setText("Notification Center");
        rel_noti = (RecyclerView) findViewById(R.id.rel_noti);
        /* select notification_id from notification where opened=0*/

        // Log.i("TAG", "Query :-> Select * from notification where opened=0");

        mDbOneSignal = MyDBManagerOneSignal.getInstance(this);
        mDbOneSignal.open(this);
        Cursor c = mDbOneSignal.getAllRows("select android_notification_id from notification where opened=0");
        // Log.i("TAG", "Query :-> Select * from notification where opened=0");


        if (c != null && c.moveToFirst()) {
            do {
                NOTIFICATION_ID.add(c.getString(c.getColumnIndex("android_notification_id")));
            } while (c.moveToNext());
            c.close();
        }

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        for (int i = 0; i < NOTIFICATION_ID.size(); i++) {

            int ID = Integer.parseInt(NOTIFICATION_ID.get(i));
            Log.i("TAG", "NOTIFICATION_ID :-> " + ID);
            notificationManager.cancel(ID);
        }


        mDbOneSignal.dbQuery("UPDATE notification SET opened = 1");
        mDb.dbQuery("UPDATE notification SET isOpen = 1");


        try {
            //   app.bubblesManager.recycle();

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {

            //  System.out.println("Total Rows on try");
            mainArray.clear();
            mainArray = (ArrayList) new CustomDatabaseQuery(this, new NotificationType())
                    .execute(new String[]{DBNewQuery.getNotificaton()}).get();


        } catch (InterruptedException e) {

            e.printStackTrace();
        } catch (ExecutionException e) {

            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        final Context context = rel_noti.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rel_noti.setLayoutAnimation(controller);
        rel_noti.scheduleLayoutAnimation();
        rel_noti.setLayoutManager(new LinearLayoutManager(context));
        Collections.reverse(mainArray);
        adapter = new NotiAdapter(mainArray, context);
        rel_noti.setAdapter(adapter);

    }


    public class NotiAdapter extends RecyclerView.Adapter<NotiAdapter.MyViewHolder> {

        private List<NotificationType> notiList;
        public Context context;
        public int SubjectID;
        public String SubjectName;


        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView tv_title, tv_discription, tv_date;

            public MyViewHolder(View view) {
                super(view);
                tv_title = (TextView) view.findViewById(R.id.tv_title);
                tv_discription = (TextView) view.findViewById(R.id.tv_discription);
                tv_date = (TextView) view.findViewById(R.id.tv_date);
            }
        }


        public NotiAdapter(List<NotificationType> notiList, Context context) {
            this.notiList = notiList;

            this.context = context;

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_notification, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            final NotificationType cons = notiList.get(position);
            holder.tv_title.setText(cons.getTitle());
            holder.tv_discription.setText(cons.getMessage());
            try {
                holder.tv_date.setText(printDifference(cons.getCreatedOn()));

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (cons.getModuleType().equals("notice_board")) {
                            startActivity(new Intent(context, NoticeBoardActivity.class));
                        } else if (cons.getModuleType().equals("cct")) {
                            if (cons.getCctSubmit().equals("0")) {
                           /*     Intent intent = new Intent(context, CctTestActivity.class);
                                intent.putExtra("id", cons.getNotification_type_id());
                                intent.putExtra("NotificationID", cons.getAndroid_notification_id());
                                context.startActivity(intent);
                      */
                            } else {
                            /*    context.startActivity(new Intent(context, CctTestResultActivity.class)
                                        .putExtra("type", "1")
                                        .putExtra("taken_id", cons.getNotification_type_id())
                                        .putExtra("subject_name", cons.getSubjectName()));
                       */
                            }

                        } else if (cons.getModuleType().equals("test_paper")) {
                            if (cons.getPaperType().equals("1") || cons.getPaperType().equals("2")) {
                                Intent intent = new Intent(context, IGeneratePaperActivity.class);
                                intent.putExtra("id", cons.getNotification_type_id());
                                context.startActivity(intent);
                                finish();
                            } else if (cons.getPaperType().equals("3")) {
                                Intent intent = new Intent(context, ISetPaperActivity.class);
                                intent.putExtra("id", cons.getNotification_type_id());
                                context.startActivity(intent);
                                finish();
                            }

                        } else if (cons.getModuleType().equals("library")) {
                            Intent intent = new Intent(context, LibrarySubjectActivity.class);

                            context.startActivity(intent);
                        } else if (cons.getModuleType().equals("zooki")) {
                            Intent intent = new Intent(context, ZookiOfflineActivity.class);

                            context.startActivity(intent);
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }


        }


        @Override
        public int getItemCount() {
            return notiList.size();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public CharSequence printDifference(String start) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        long time = 0;

        try {
            time = sdf.parse(start).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long now = System.currentTimeMillis();

        CharSequence ago =
                DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
        return ago;
    }
}
