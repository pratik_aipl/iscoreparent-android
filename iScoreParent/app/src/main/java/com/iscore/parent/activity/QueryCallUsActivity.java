package com.iscore.parent.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class QueryCallUsActivity extends AppCompatActivity implements AsynchTaskListner {

    public ImageView img_callNow;
    public Button btn_send;
    public EditText et_message;
    public QueryCallUsActivity instance;
    public App app;
    public ImageView img_back;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_query_call_us);
        Utils.logUser();

        instance = this;
        app = App.getInstance();
        img_callNow = (ImageView) findViewById(R.id.img_callNow);
        btn_send = (Button) findViewById(R.id.btn_send);
        et_message = (EditText) findViewById(R.id.et_message);
        img_callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = "+917710012112";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        });

        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("Query");

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_message.getText().toString().equals("")) {
                    Utils.showToast("Please fill the message", instance);
                } else {
                    new CallRequest(instance).querySendMail(app.mainUser.getEmailID(), et_message.getText().toString());
                }

            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i(result, "RESULT");
            Utils.hideProgressDialog();
            try {
                switch (request) {

                    case qurySendMail:

                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            et_message.setText("");
                            Utils.showAlert("Thank you for contacting us. We will shortly get back to you.", instance);
                        } else {
                            Utils.showAlert(jObj.getString("message"), instance);

                        }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
