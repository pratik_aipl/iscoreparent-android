package com.iscore.parent.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.IConnect.activity.CctTestResultActivity;
import com.iscore.parent.activity.IConnect.activity.NoticeBoardActivity;
import com.iscore.parent.activity.library.Activity.LibrarySubjectActivity;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.OnTaskCompletedDashboard;
import com.iscore.parent.model.MainUser;
import com.iscore.parent.model.NotificationOneSignal;
import com.iscore.parent.model.NotificationType;
import com.iscore.parent.R;
import com.iscore.parent.db.DBNewQuery;
import com.iscore.parent.db.DBTables;
import com.iscore.parent.db.MyDBManager;
import com.iscore.parent.db.MyDBManagerOneSignal;
import com.iscore.parent.utils.CursorParserUniversal;
import com.iscore.parent.utils.Utils;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends Activity implements OnTaskCompletedDashboard {
    private final int SPLASH_DISPLAY_LENGTH = 4000;
    public int DATE_TIME_REQUEST = 999;
    public SplashActivity instance;
    public App app;
    public String LastVersion;
    String version = "";
    public ImageView img_class_logo;
    public static ArrayList<NotificationOneSignal> OneSignalArray = new ArrayList<>();
    public MyDBManager mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        instance = this;
        app = App.getInstance();
        int verCode = Utils.currentAppVersionCode(instance);
        version = String.valueOf(verCode);
        try {
            mDb = MyDBManager.getInstance(this);
            mDb.open(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (app.mySharedPref.getIsLoggedIn()) {
            if (app.isNoti) {
                new getRecorrd(instance).execute();
            }
        }


        //------------  NotificationOneSignal  END --------------------//
        if (app.mySharedPref.getIsLoggedIn()) {
            if (!app.mySharedPref.getClassLogo().isEmpty()) {
                File imageFile = new File(app.mySharedPref.getClassLogo());
                if (imageFile.exists()) {
                    setContentView(R.layout.activity_class_splash);
                    img_class_logo = (ImageView) findViewById(R.id.img_class_logo);
                    img_class_logo.setImageBitmap(BitmapFactory.decodeFile(app.mySharedPref.getClassLogo()));
                } else {

                    setContentView(R.layout.activity_splash);
                    Utils.logUser();
                }
            } else {
                setContentView(R.layout.activity_splash);
                Utils.logUser();
            }
            if (Utils.checkOutDated()) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DataSherdToAppClass();
                        App.app_Key = app.mySharedPref.getReg_key();

                        if (!version.equalsIgnoreCase(app.mySharedPref.getVersioCode())) {
                            goDownloadDataAndSync(app.mySharedPref.getVersioCode());
                        } else {
                            if (Utils.isTimeAutomatic(instance)) {
                                manageLogin();
                            } else {
                                setAutomaticTimeDateAlert();
                            }
                        }

                    }
                }, SPLASH_DISPLAY_LENGTH);
            } else {
                new AlertDialog.Builder(SplashActivity.this)
                        .setTitle("Expired")
                        .setMessage("Your iSccore Product has Expired, Kindly contact Parshvaa Publications")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }

        } else {
            setContentView(R.layout.activity_splash);
            Utils.logUser();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, TourActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeManually();
    }

    public void goDownloadDataAndSync(String LastVersion) {
        if (app.mySharedPref.getIsLoggedIn()) {
            if (app.mySharedPref.getFirstName().isEmpty() && app.mySharedPref.getLastName().isEmpty()) {
                redirectToLogin();
            } else {

                DataSherdToAppClass();

                Log.i("TAG", "SPlASH : " + LastVersion);
                startActivity(new Intent(instance, DashBoardActivity.class)
                        .putExtra("LastVersion", LastVersion)
                        .putExtra("sync", "y"));
            }
        } else {
            redirectToLogin();
        }
    }

    public void redirectToLogin() {
        Intent i = new Intent(SplashActivity.this, MobiLoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    public void goToDownloadOrHome(boolean isCompleteDownloaded) {
        if (!isCompleteDownloaded) {
            LastVersion = app.mySharedPref.getVersioCode();
            Log.i("TAG", "SPlASH : " + LastVersion);
            Intent i = new Intent(SplashActivity.this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("LastVersion", LastVersion);
            startActivity(i);
            finish();
        } else {
            app.Target = app.mySharedPref.getTarget();
            if (App.isNoti) {
                if (App.isType.equalsIgnoreCase("cct")) {
                    String id = getIntent().getExtras().getString("Notification_type_id");
                    int NotificationID = getIntent().getExtras().getInt("NotificationID");

                    Intent intent = new Intent(instance, CctTestResultActivity.class);
                    intent.putExtra("id", id);
                    intent.putExtra("type", "0");

                    intent.putExtra("NotificationID", NotificationID);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("notice_board")) {
                    Intent intent = new Intent(instance, NoticeBoardActivity.class);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("library")) {
                    Intent intent = new Intent(instance, LibrarySubjectActivity.class);
                    startActivity(intent);
                    finish();
                } else if (App.isType.equalsIgnoreCase("zooki")) {
                    Intent intent = new Intent(instance, ZookiNotificationActivity.class);
                    intent.putExtra("title", getIntent().getExtras().getString("title"));
                    intent.putExtra("bigPicture", getIntent().getExtras().getString("bigPicture"));
                    intent.putExtra("created_on", getIntent().getExtras().getString("created_on"));
                    intent.putExtra("body", getIntent().getExtras().getString("body"));
                    startActivity(intent);
                    finish();
                } else {
                    Intent i = new Intent(SplashActivity.this, DashBoardActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }
            } else {

                Intent i = new Intent(SplashActivity.this, DashBoardActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        }
    }

    public void DataSherdToAppClass() {
        app.mainUser = new MainUser();
        app.mainUser.setFirstName(app.mySharedPref.getFirstName());
        app.mainUser.setLastName(app.mySharedPref.getLastName());
        app.mainUser.setRegKey(app.mySharedPref.getReg_key());
        app.mainUser.setGuid(app.mySharedPref.getGuuid());
        app.mainUser.setClassID(app.mySharedPref.getClassID());
        app.mainUser.setBoardID(app.mySharedPref.getBoardID());
        app.mainUser.setMediumID(app.mySharedPref.getMediumID());
        app.mainUser.setStandardID(app.mySharedPref.getStandardID());
        app.mainUser.setEmailID(app.mySharedPref.getEmailID());
        app.mainUser.setRegMobNo(app.mySharedPref.getRegMobNo());
        app.mainUser.setSubjects(app.mySharedPref.getSubjects());
        app.mainUser.setStudent_id(app.mySharedPref.getStudentID());
        app.mainUser.setStanderdName(app.mySharedPref.getStandardName());
        app.mainUser.setClassName(app.mySharedPref.getClassName());
        app.mainUser.setKeyStatus(app.mySharedPref.getKeyStatus());
        app.mainUser.setBranchID(app.mySharedPref.getBranchID());
        app.mainUser.setBatchID(app.mySharedPref.getBatchID());
        app.mainUser.setSchoolName(app.mySharedPref.getSchoolName());
        app.mainUser.setVersion(app.mySharedPref.getVersion());
        app.mainUser.setBoardName(app.mySharedPref.getBoardName());
        app.mainUser.setMediumName(app.mySharedPref.getMediumName());
        app.mainUser.setStudentCode(app.mySharedPref.getStudentCode());
        app.mainUser.setPreviousYearScore(app.mySharedPref.getlblPrviousTarget());


    }

    public void setAutomaticTimeDateAlert() {
        new AlertDialog.Builder(this)
                .setTitle("Date Time ")
                .setMessage("Automatic Date time is not enabled, Do you want to enable it?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), DATE_TIME_REQUEST);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.showToast("Please enable automatic date time for use this app", SplashActivity.this);
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void manageLogin() {
        if (app.mySharedPref.getDownloadStatus().equals("n")) {
            if (!Utils.isNetworkAvailable(this)) {
                new AlertDialog.Builder(SplashActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("oops..!")
                        .setCancelable(false)
                        .setMessage("kindly switch on internet connection!")
                        .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                manageLogin();
                            }
                        })
                        .show();
            } else {
                goToDownloadOrHome(true);
            }
        } else {

            goToDownloadOrHome(true);

        }
    }

    @Override
    public void onGetRecorComplite() {

    }

    @Override
    public void onInsertRecorComplite() {

    }

    public class getRecorrd extends AsyncTask<String, String, String> {


        Context context;
        private MyDBManager mDb;
        public MyDBManagerOneSignal mDbOneSignal;
        public App app;
        public CursorParserUniversal cParse;
        public OnTaskCompletedDashboard listener;
        public String SubjectName = "", created_on = "", notification_type_id = "", type = "";


        private getRecorrd(Context context) {

            this.context = context.getApplicationContext();
            app = App.getInstance();
            this.cParse = new CursorParserUniversal();
            mDb = MyDBManager.getInstance(context);
            mDb.open(context);
            mDbOneSignal = MyDBManagerOneSignal.getInstance(context);
            mDbOneSignal.open(context);
            this.listener = (OnTaskCompletedDashboard) context;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... id) {

            int N_ID = mDb.getLastRecordIDForRequest("notification", "OneSignalID");

            Cursor noti = mDbOneSignal.getAllRows(DBNewQuery.getNotificatonDash(N_ID));
            if (noti != null && noti.moveToFirst()) {
                do {
                    NotificationOneSignal notiObj = (NotificationOneSignal) cParse.parseCursor(noti, new NotificationOneSignal());
                    OneSignalArray.add(notiObj);
                } while (noti.moveToNext());
                noti.close();
            }

            if (OneSignalArray != null && OneSignalArray.size() > 0) {
                for (int i = 0; i < OneSignalArray.size(); i++) {
                    try {
                        NotificationType typeObj = new NotificationType();

                        JSONObject jObj = new JSONObject(OneSignalArray.get(i).getFull_data());
                        String custom = jObj.getString("custom");
                        JSONObject jcustom = new JSONObject(custom);
                        String a = jcustom.getString("a");
                        JSONObject jtype = new JSONObject(a);
                        type = jtype.getString("type");
                        notification_type_id = jtype.getString("notification_type_id");
                        created_on = jtype.getString("created_on");
                        SubjectName = jtype.getString("subject_name");
                        typeObj.setOneSignalID(Integer.parseInt(OneSignalArray.get(i).get_id()));
                        typeObj.setCreatedOn(created_on);
                        typeObj.setIsOpen(OneSignalArray.get(i).getOpened());
                        typeObj.setIsTypeOpen("0");
                        typeObj.setSubjectName(SubjectName);
                        typeObj.setNotification_type_id(notification_type_id);
                        typeObj.setModuleType(type);
                        typeObj.setMessage(OneSignalArray.get(i).getMessage());
                        typeObj.setTitle(OneSignalArray.get(i).getTitle());
                        mDb.insertWebRow(typeObj, "notification");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listener.onGetRecorComplite();
        }
    }

    public void resumeManually() {

        DBTables dbTables = new DBTables();
        dbTables.createAll(mDb);

    }
}

