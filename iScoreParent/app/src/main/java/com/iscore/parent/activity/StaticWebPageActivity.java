package com.iscore.parent.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

import io.fabric.sdk.android.Fabric;

public class StaticWebPageActivity extends AppCompatActivity {
    public StaticWebPageActivity indtance;
    public ImageView img_back;
    public TextView tv_title;
    public String action;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_static_web_page);
        Utils.logUser();

        indtance = this;
        action = getIntent().getExtras().getString("action");
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        WebView webView = (WebView) findViewById(R.id.web_legal);

        if(action.equalsIgnoreCase( "l")){
            tv_title.setText(R.string.Licenses);
            webView.loadUrl("file:///android_asset/Licenses&Copyrights.html");
        }else if(action.equalsIgnoreCase("t")){
            tv_title.setText(R.string.termsAndCondition);
            webView.loadUrl("file:///android_asset/Terms&Conditions.html");
        }else if(action.equalsIgnoreCase("p")) {
            tv_title.setText(R.string.privacy);
            webView.loadUrl("file:///android_asset/privacy-policy.html");
        }else{
            tv_title.setText(R.string.refund);
            webView.loadUrl("file:///android_asset/Refund&Cancellation.html");

        }


    }
}
