package com.iscore.parent.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.adapter.MyViewPagerAdapter;
import com.iscore.parent.model.Tour;
import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

import io.fabric.sdk.android.Fabric;

public class TourActivity extends Activity {

    ViewPager viewPager;
    public LinearLayout pager_indicator, lin_next;
    private int dotsCount;
    public TextView tv_skip, tv_next;
    private ImageView[] dots;
    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    public TourActivity instance;
    private static final int LOCATION_PERMISSION_ID = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_tour);
        Utils.logUser();


        instance = this;

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tv_skip = (TextView) findViewById(R.id.tv_skip);
        tv_next = (TextView) findViewById(R.id.tv_next);
        lin_next = (LinearLayout) findViewById(R.id.lin_next);
        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MobiLoginActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });
        lin_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int Pos = viewPager.getCurrentItem();
                if (Pos + 1 < dotsCount) {
                    Log.i("TAG IF", "POS : " + Pos + " DOTSCOUNT : " + dotsCount);
                    viewPager.setCurrentItem(Pos + 1);
                } else {
                    Log.i("TAG ELSE", "POS : " + Pos + " DOTSCOUNT : " + dotsCount);
                    startActivity(new Intent(instance, MobiLoginActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }


            }
        });
        viewPager.setAdapter(new MyViewPagerAdapter(this));
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        setUiPageViewController();

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 4) {
                    tv_next.setText("JOIN NOW");

                } else {
                    tv_next.setText("NEXT");
                }
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
                }
                dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setUiPageViewController() {

        dotsCount = Tour.values().length;
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(8, 0, 8, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_ID && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (Utils.checkPhoneStatePermission(this)) {
                if (Utils.checkStoragePermission(this)) {
                }
            }


        } else {
            Utils.showAlert("Permission Denied, Need Permision for procced ahead", this);
            if (Utils.checkPhoneStatePermission(this)) {
                if (Utils.checkStoragePermission(this)) {
                }
            }


        }

    }
}

