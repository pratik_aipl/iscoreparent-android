package com.iscore.parent.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.interfaceparent.OTPListener;
import com.iscore.parent.model.KeyData;
import com.iscore.parent.MySMSBroadCastReceiver;
import com.iscore.parent.OtpView;
import com.iscore.parent.R;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.FileDownloader;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.MySharedPref;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;


public class VerifyOtpActivity extends AppCompatActivity implements OTPListener, AsynchTaskListner {
    public VerifyOtpActivity instance;
    public Button btn_resend, btn_confirm, btn_cancel;
    private OtpView mOtpView;

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public String Msg_otp = "";
    public String name, str_amount = "", OTP = "", surname, mobile, email, boardID, mediumID,
            standardID, packageID, version, RegKey, Guid, PleyaerID = "";
    public App app;
    public boolean isFromMobileNoLogin = false;
    public Handler customHandler = new Handler();
    public JsonParserUniversal jParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_verify_otp);
        Utils.logUser();
        instance = this;
        app = App.getInstance();
        jParser = new JsonParserUniversal();
        app.mySharedPref = new MySharedPref(VerifyOtpActivity.this);
        if (checkAndRequestPermissions()) {
            // carry on the normal flow, as the case of  permissions  granted.
        }
        if (!app.mySharedPref.getGuuid().isEmpty()) {
            Guid = app.mySharedPref.getGuuid();
        } else {
            Guid = UUID.randomUUID().toString();
        }

        mOtpView = (OtpView) findViewById(R.id.otp_view);
        btn_resend = (Button) findViewById(R.id.btn_resend);
        btn_confirm = (Button) findViewById(R.id.btn_confirm);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);


        if (getIntent().hasExtra("MobLogin")) {
            mobile = app.mainUser.getRegMobNo();
            PleyaerID = getIntent().getStringExtra("PleyaerID");
            isFromMobileNoLogin = true;
        } else {

        }
        int verCode = Utils.currentAppVersionCode(VerifyOtpActivity.this);
        version = String.valueOf(verCode);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });
        MySMSBroadCastReceiver.bind(instance, "KARAN");
        new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                SimpleDateFormat tempDf = new SimpleDateFormat("mm:ss", Locale.ENGLISH);
                btn_resend.setText("" + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

            }

            public void onFinish() {
                btn_resend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        start();
                        OTP = "";
                        new CallRequest(VerifyOtpActivity.this).resend_otp(mobile);
                    }
                });
                btn_resend.setText("Resend");
            }
        }.start();


        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isNetworkAvailable(instance)) {
                    verfyOTPandFrowedNext();
                } else {
                    Utils.showToast("Please connect internet to Verify OTP", instance);
                }
            }


        });
    }

    public void verfyOTPandFrowedNext() {

        if (isFromMobileNoLogin) {
            if (PleyaerID.equalsIgnoreCase("")) {
                PleyaerID = Utils.OneSignalPlearID();
                Utils.showProgressDialog(instance);
                customHandler.postDelayed(updateTimerThread, 0);

            } else {
                new CallRequest(instance).confirmOtp(mOtpView.getOTP().toString(), PleyaerID);
            }
        } else {
            Utils.showToast("INVALID OTP", VerifyOtpActivity.this);
        }

    }

    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            if (!PleyaerID.equalsIgnoreCase("")) {
                //   new CallRequest(instance).update_last_sync_time(PleyaerID);
                customHandler.removeCallbacks(updateTimerThread);

            } else {
                PleyaerID = Utils.OneSignalPlearID();
                customHandler.postDelayed(this, 1000);
            }

        }
    };

    @Override
    public void onBackPressed() {

        this.finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void otpReceived(String messageText) {
        Msg_otp = messageText.substring(0, 4);
        mOtpView.setOTP(messageText.substring(0, 4));
        if (!this.isDestroyed()) {
            if (Utils.isNetworkAvailable(instance)) {
                    verfyOTPandFrowedNext();
            } else {
                Utils.showToast("Please connect internet to Verify OTP", instance);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {
                case resend_otp:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                        } else {
                            Utils.showToast(jObj.getString("message"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case confirmOtp:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        JSONObject jData = jObj.getJSONObject("data");
                        app.mySharedPref.setIsLoggedIn(true);

                        app.mainUser.setStudent_id(jData.getString("StudentID"));
                        app.mainUser.setFirstName(jData.getString("FirstName"));
                        app.mainUser.setLastName(jData.getString("LastName"));
                        app.mainUser.setGuid(jData.getString("IMEINo"));
                        app.mainUser.setEmailID(jData.getString("EmailID"));
                        app.mainUser.setRegMobNo(jData.getString("RegMobNo"));
                        app.mainUser.setSchoolName(jData.getString("SchoolName"));
                        app.mainUser.setProfile_image(jData.getString("ProfileImage"));
                        app.mainUser.setStudentCode(jData.getString("StudentCode"));

                        app.mySharedPref.setStudent_id(app.mainUser.getStudent_id());
                        app.mySharedPref.setFirst_name(app.mainUser.getFirstName());
                        app.mySharedPref.setLast_name(app.mainUser.getLastName());
                        app.mySharedPref.setGuuid(app.mainUser.getGuid());
                        app.mySharedPref.setEmailID(app.mainUser.getEmailID());
                        app.mySharedPref.setRegMobNo(app.mainUser.getRegMobNo());
                        app.mySharedPref.setStudentCode(app.mainUser.getStudentCode());
                        // app.mySharedPref.setProfileImage(app.mainUser.getProfile_image());

                        app.mySharedPref.setVersioCode(version);

                        KeyData keydata;
                        JSONArray jsonArray = jData.getJSONArray("KeyData");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jKey = jsonArray.getJSONObject(i);
                            keydata = (KeyData) jParser.parseJson(jsonArray.getJSONObject(i), new KeyData());
                            app.mainUser.keyDataArray.add(keydata);

                            app.mainUser.setRegKey(app.mainUser.keyDataArray.get(i).getStudentKey());
                            app.mainUser.setPreviousYearScore(app.mainUser.keyDataArray.get(i).getPreviousYearScore());

                            app.mySharedPref.setStudent_id(app.mainUser.keyDataArray.get(i).getStudentID());
                            app.mySharedPref.setBoardID(app.mainUser.keyDataArray.get(i).getBoardID());
                            app.mySharedPref.setMediumID(app.mainUser.keyDataArray.get(i).getMediumID());
                            app.mySharedPref.setStandardID(app.mainUser.keyDataArray.get(i).getStandardID());
                            app.mySharedPref.setClassID(app.mainUser.keyDataArray.get(i).getClassID());
                            app.mySharedPref.setBranchID(app.mainUser.keyDataArray.get(i).getBranchID());
                            app.mySharedPref.setBatchID(app.mainUser.keyDataArray.get(i).getBatchID());
                            app.mySharedPref.setReg_key(app.mainUser.keyDataArray.get(i).getStudentKey());
                            app.mySharedPref.setVersion(app.mainUser.keyDataArray.get(i).getIsDemo());
                            app.mySharedPref.setStandardName(app.mainUser.keyDataArray.get(i).getStandardName());
                            app.mySharedPref.setClassName(app.mainUser.keyDataArray.get(i).getClassName());
                            app.mySharedPref.setBoardName(app.mainUser.keyDataArray.get(i).getBoardName());
                            app.mySharedPref.setMediumName(app.mainUser.keyDataArray.get(i).getMediumName());
                            app.mySharedPref.setClassLogo(app.mainUser.keyDataArray.get(i).getClassLogo());
                            app.mySharedPref.setClassRoundLogo(app.mainUser.keyDataArray.get(i).getClassRoundLogo());
                            app.mySharedPref.setCreatedOn(app.mainUser.keyDataArray.get(i).getCreatedOn());
                            app.mySharedPref.setlblTarget(app.mainUser.keyDataArray.get(i).getSetTarget());

                            app.mySharedPref.setlblPrviousTarget(app.mainUser.keyDataArray.get(i).getPreviousYearScore());

                        }
                        String fileName = app.mainUser.getProfile_image().substring(app.mainUser.getProfile_image().lastIndexOf("/") + 1);
                        String ClassLogoName = app.mySharedPref.getClassLogo().substring(app.mySharedPref.getClassLogo().lastIndexOf("/") + 1);
                        String fileNameCircle = app.mainUser.getProfile_image().substring(app.mainUser.getProfile_image().lastIndexOf("/") + 1);
                        String ClassLogoNameCircle = app.mySharedPref.getClassRoundLogo().substring(app.mySharedPref.getClassRoundLogo().lastIndexOf("/") + 1);


                        if (Utils.checkOutDated()) {
                            new DownloadFile().execute(app.mainUser.getProfile_image(), fileName, "profile");
                            new DownloadFile().execute(app.mySharedPref.getClassLogo(), ClassLogoName, "class");
                            new DownloadFile().execute(app.mySharedPref.getClassRoundLogo(), ClassLogoNameCircle, "classRound");
                            goAhead();
                        } else {
                            new AlertDialog.Builder(this)
                                    .setTitle("Expired")
                                    .setMessage("Your iSccore Product has Expired, Kindly contact Parshvaa Publications")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            VerifyOtpActivity.this.finish();
                                            dialog.dismiss();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, String> {
        String fileName = "", ImgURL = "";
        File qDirectory;
        String qLocalUrl = "";
        String Type = "";

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {
            ImgURL = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            fileName = strings[1];
            Type = strings[2];// -> maven.pdf

            if (Type.equals("profile")) {
                String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/profile";
                qDirectory = new File(SDCardPath);
                if (!qDirectory.exists()) qDirectory.mkdirs();
                Log.i("TAG ", "IMAGE URL :-> " + ImgURL);

                qLocalUrl = SDCardPath + "/" + fileName;
            } else {
                String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/ClassLogo";
                qDirectory = new File(SDCardPath);
                if (!qDirectory.exists()) qDirectory.mkdirs();
                Log.i("TAG ", "IMAGE URL :-> " + ImgURL);

                qLocalUrl = SDCardPath + "/" + fileName;
            }


            File qLocalFile = new File(qLocalUrl);
            if (!qLocalFile.exists()) {
                FileDownloader.downloadFile(ImgURL, qLocalFile, getApplicationContext());
            }

            return Type;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            Utils.hideProgressDialog();
            if (aVoid.equals("profile")) {
                if (!fileName.equals("")) {
                    app.mySharedPref.setProfileImage(Constant.LOCAL_IMAGE_PATH + "/profile/" + fileName);
                } else {
                    app.mySharedPref.setProfileImage("");
                }
            }

            if (aVoid.equals("class")) {
                if (!fileName.equals("")) {
                    app.mySharedPref.setClassLogo(Constant.LOCAL_IMAGE_PATH + "/ClassLogo/" + fileName);

                } else {
                    app.mySharedPref.setClassLogo("");
                }
            }
            if (aVoid.equals("classRound")) {
                if (!fileName.equals("")) {
                    app.mySharedPref.setClassRoundLogo(Constant.LOCAL_IMAGE_PATH + "/ClassLogo/" + fileName);
                } else {
                    app.mySharedPref.setClassRoundLogo("");
                }
            }

            Log.i("TAG onPostExecute", "IMAGE URL :-> " + Constant.LOCAL_IMAGE_PATH + "/profile/" + fileName + " +");


        }
    }

    public void goAhead() {

        if (Utils.isNetworkAvailable(VerifyOtpActivity.this)) {
            startActivity(new Intent(VerifyOtpActivity.this, DashBoardActivity.class)

                    .putExtra("MobLogin", "MobLogin"));
        } else {
            Utils.showToast("Please connect Internet to Singup", VerifyOtpActivity.this);
        }


    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.SEND_SMS);

        int receiveSMS = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.RECEIVE_SMS);

        int readSMS = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (receiveSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.RECEIVE_MMS);
        }
        if (readSMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_SMS);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

}
