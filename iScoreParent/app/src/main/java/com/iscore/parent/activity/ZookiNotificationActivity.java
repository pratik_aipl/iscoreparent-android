package com.iscore.parent.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.crashlytics.android.Crashlytics;
import com.iscore.parent.App;
import com.iscore.parent.observscroll.ObservableScrollView;
import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

import io.fabric.sdk.android.Fabric;

public class ZookiNotificationActivity extends AppCompatActivity {
    private ImageView mImageView;
    private TextView tv_FullDesc, tv_title, tv_date, tv_header_title;
    public App app;
    public ImageView img_back;

    private ObservableScrollView mScrollView;
    public int mParallaxImageHeight;
    public FrameLayout frame_content, frame_viewMore;
    ZookiNotificationActivity instance;
    public AQuery aQuery;
    public ProgressBar pbar;
    public String title = "", bigPicture = "", created_on = "", body = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_zooki_notification);
        Utils.logUser();
        aQuery = new AQuery(this);
        title = getIntent().getExtras().getString("title");
        bigPicture = getIntent().getExtras().getString("bigPicture");
        created_on = getIntent().getExtras().getString("created_on");
        body = getIntent().getExtras().getString("body");


        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_FullDesc = (TextView) findViewById(R.id.tv_FullDesc);
        tv_date = (TextView) findViewById(R.id.tv_date);
        frame_content = (FrameLayout) findViewById(R.id.frame_content);
        frame_viewMore = (FrameLayout) findViewById(R.id.frame_viewMore);
        mImageView = (ImageView) findViewById(R.id.image);
        pbar = findViewById(R.id.pbar);
        Log.i("TAG", "URL :- " + bigPicture);
        aQuery.id(mImageView).progress(pbar).image(bigPicture, true, true, 0, R.drawable.zooki_image, null, 0, 0.0f);
        img_back=(ImageView) findViewById(R.id.img_back);
        tv_title.setText(title);
        tv_FullDesc.setText(body);

        try {
            created_on = created_on.substring(0, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }
        tv_date.setText(Utils.changeDateToDDMMYYYY(created_on));
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }

    }
}
