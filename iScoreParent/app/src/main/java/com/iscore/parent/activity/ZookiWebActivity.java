package com.iscore.parent.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.App;
import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

import io.fabric.sdk.android.Fabric;

public class ZookiWebActivity extends Activity {
    public WebView web_newslastter;
    public App app;
    public SharedPreferences shared;
    String url = "";
    public ImageView img_back;
    public TextView tv_title;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_zooki);

        Utils.logUser();
        Utils.showProgressDialog(ZookiWebActivity.this);
        try {
            Intent i = getIntent();
            url = i.getExtras().getString("url");
            Log.i("TAG", "URL::->" + url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        web_newslastter = (WebView) findViewById(R.id.web_newslastter);
        web_newslastter.setWebViewClient(new myWebClient());

        Utils.webSettings(web_newslastter);


        try {
            if (!url.equals("")) {
                Log.i("TAG", "URL::->" + url);
                web_newslastter.loadUrl(url);
            } else {
                web_newslastter.loadUrl("https://parshvaa.com/blog_copy/");

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            web_newslastter.loadUrl("https://parshvaa.com/blog_copy/");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        web_newslastter.onPause();
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                Utils.hideProgressDialog();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

}
