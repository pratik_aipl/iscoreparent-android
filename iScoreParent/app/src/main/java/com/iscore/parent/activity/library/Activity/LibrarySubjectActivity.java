package com.iscore.parent.activity.library.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.DashBoardActivity;
import com.iscore.parent.activity.library.Adapter.LibrarySubjectAdapter;
import com.iscore.parent.activity.library.Library;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.db.MyDBManager;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class LibrarySubjectActivity extends AppCompatActivity implements AsynchTaskListner {

    private RecyclerView recyclerView;
    private LibrarySubjectAdapter mAdapter;
    public ImageView img_back;
    public TextView tv_title;
    ArrayList<Library> libraryArrayList = new ArrayList<>();
     public Library library;
    public JsonParserUniversal jParser;
    public static MyDBManager mDb;

    public RelativeLayout rel_library, rel_subject, rel_chapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_library_subject);
        Utils.logUser();
         jParser = new JsonParserUniversal();
        mDb = MyDBManager.getInstance(this);
        mDb.open(this);
        mDb.dbQuery("UPDATE notification SET isTypeOpen = 1,isOpen=1  WHERE ModuleType = 'library'");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        rel_library = (RelativeLayout) findViewById(R.id.rel_library);
        rel_subject = (RelativeLayout) findViewById(R.id.rel_subject);
        rel_chapter = (RelativeLayout) findViewById(R.id.rel_chapter);

        rel_subject.setVisibility(View.GONE);
        rel_chapter.setVisibility(View.GONE);

        tv_title.setText("Library");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        new CallRequest(LibrarySubjectActivity.this).get_library_parent_list();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (App.isNoti) {
            App.isNoti = false;
            Intent i = new Intent(this, DashBoardActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }
    }

    private void setupRecyclerView() {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new LibrarySubjectAdapter(libraryArrayList, context);
        recyclerView.setAdapter(mAdapter);
        // prepareMovieData();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case get_library_parent_list:

                    libraryArrayList.clear();
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {
                            JSONArray jDataArray = jObj.getJSONArray("library");
                            if (jDataArray != null && jDataArray.length() > 0) {

                                for (int i = 0; i < jDataArray.length(); i++) {
                                    JSONObject jpaper = jDataArray.getJSONObject(i);
                                    library = (Library) jParser.parseJson(jpaper, new Library());
                                    libraryArrayList.add(library);
                                }
                                setupRecyclerView();
                                Utils.hideProgressDialog();

                            }

                        } else {
                            Utils.hideProgressDialog();
                            showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}

