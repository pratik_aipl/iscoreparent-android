package com.iscore.parent.activity.library.Activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.library.Adapter.NotesAdapter;
import com.iscore.parent.activity.library.Adapter.SystemFileAdapter;
import com.iscore.parent.activity.library.Adapter.VideoAdapter;
import com.iscore.parent.activity.library.ImageFile;
import com.iscore.parent.activity.library.SystemFiles;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class LibrarySystemFilesActivity extends AppCompatActivity implements AsynchTaskListner {

    public LibrarySystemFilesActivity instance;
    public App app;
    public ImageFile imgObj;
    public ArrayList<ImageFile> imgArray = new ArrayList<>();
    SystemFiles sysObj;
    public JsonParserUniversal jParser;
    public SystemFileAdapter adapter;
    public String FolderID = "", SubjectID = "", Subject = "", ChapterID = "", Chapter = "", date = "";
    public RecyclerView recycler_img;
    public RelativeLayout rel_library, rel_subject, rel_chapter;
    public TextView tv_lib, tv_subject, tv_chapter, tv_notes, tv_video, tv_picture;
    public ImageView img_picture, img_video, img_notes;

    public LinearLayout lin_picture, lin_videos, lin_notes;
    public HorizontalScrollView lin_breadcum;
    public ArrayList<SystemFiles> fileArray = new ArrayList<>();
    public ArrayList<SystemFiles> tempArray = new ArrayList<>();
    public String FileType = "";
    public NotesAdapter notesAdapter;
    public VideoAdapter videoAdapter;
    public ImageView img_back;
    public TextView tv_title;
    private LinearLayout emptyView;
    public TextView tv_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_library_system_files);
        Utils.logUser();

        instance = this;
        jParser = new JsonParserUniversal();

        recycler_img = (RecyclerView) findViewById(R.id.recycler_img);
        lin_breadcum = (HorizontalScrollView) findViewById(R.id.lin_breadcum);
        rel_library = (RelativeLayout) findViewById(R.id.rel_library);
        rel_subject = (RelativeLayout) findViewById(R.id.rel_subject);
        rel_chapter = (RelativeLayout) findViewById(R.id.rel_chapter);
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);

        tv_lib = (TextView) findViewById(R.id.tv_lib);
        tv_subject = (TextView) findViewById(R.id.tv_subject);
        tv_chapter = (TextView) findViewById(R.id.tv_chapter);
        tv_notes = (TextView) findViewById(R.id.tv_notes);
        tv_video = (TextView) findViewById(R.id.tv_video);
        tv_picture = (TextView) findViewById(R.id.tv_picture);
        lin_picture = (LinearLayout) findViewById(R.id.lin_picture);
        lin_videos = (LinearLayout) findViewById(R.id.lin_videos);
        lin_notes = (LinearLayout) findViewById(R.id.lin_notes);
        img_picture = (ImageView) findViewById(R.id.img_picture);
        img_video = (ImageView) findViewById(R.id.img_video);
        img_notes = (ImageView) findViewById(R.id.img_notes);
        emptyView = (LinearLayout) findViewById(R.id.empty_view);
        tv_empty = (TextView) findViewById(R.id.tv_empty);

        emptyView.setVisibility(View.GONE);
        FolderID = getIntent().getStringExtra("FolderID");
        SubjectID = getIntent().getStringExtra("SubjectID");
        Subject = getIntent().getStringExtra("Subject");
        ChapterID = getIntent().getStringExtra("ChapterID");
        Chapter = getIntent().getStringExtra("Chapter");
        tv_subject.setText(Subject);
        tv_chapter.setText(Chapter);
        tv_title.setText(Chapter);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });
        tv_lib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LibrarySystemFilesActivity.this, LibrarySubjectActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        lin_breadcum.post(new Runnable() {
            @Override
            public void run() {
                lin_breadcum.fullScroll(HorizontalScrollView.FOCUS_RIGHT);

            }
        });
        new CallRequest(instance).get_system_library_files(FolderID, "image");

        lin_picture.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                lin_picture.setBackground(getResources().getDrawable(R.drawable.bg_left));
                lin_videos.setBackground(getResources().getDrawable(R.drawable.bg_blue_border));
                lin_notes.setBackground(getResources().getDrawable(R.drawable.bg_right_border));

                img_picture.setImageResource(R.drawable.images_white);
                img_video.setImageResource(R.drawable.video_black);
                img_notes.setImageResource(R.drawable.note_black);

                tv_picture.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_video.setTextColor(getResources().getColor(R.color.black));
                tv_notes.setTextColor(getResources().getColor(R.color.black));
                FileType = "image";
                new CallRequest(instance).get_system_library_files(FolderID, "image");

            }
        });
        lin_videos.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {
                lin_picture.setBackground(getResources().getDrawable(R.drawable.bg_left_border));
                lin_videos.setBackground(getResources().getDrawable(R.drawable.bg_blue));
                lin_notes.setBackground(getResources().getDrawable(R.drawable.bg_right_border));
                img_picture.setImageResource(R.drawable.images_black);
                img_video.setImageResource(R.drawable.video_white);
                img_notes.setImageResource(R.drawable.note_black);
                tv_picture.setTextColor(getResources().getColor(R.color.black));
                tv_video.setTextColor(getResources().getColor(R.color.colorWhite));
                tv_notes.setTextColor(getResources().getColor(R.color.black));

                FileType = "video";

                new CallRequest(instance).get_system_library_files(FolderID, "video");

            }
        });
        lin_notes.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                lin_picture.setBackground(getResources().getDrawable(R.drawable.bg_left_border));
                lin_videos.setBackground(getResources().getDrawable(R.drawable.bg_blue_border));
                lin_notes.setBackground(getResources().getDrawable(R.drawable.bg_right));
                img_picture.setImageResource(R.drawable.images_black);
                img_video.setImageResource(R.drawable.video_black);
                img_notes.setImageResource(R.drawable.note_white);
                tv_picture.setTextColor(getResources().getColor(R.color.black));
                tv_video.setTextColor(getResources().getColor(R.color.black));
                tv_notes.setTextColor(getResources().getColor(R.color.colorWhite));

                FileType = "pdf";

                new CallRequest(instance).get_system_library_files(FolderID, "pdf");

            }
        });
    }

    private void setupRecyclerView() {
        final Context context = recycler_img.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recycler_img.setLayoutAnimation(controller);
        recycler_img.scheduleLayoutAnimation();
        recycler_img.setLayoutManager(new LinearLayoutManager(context));
        adapter = new SystemFileAdapter(imgArray, context, SubjectID, ChapterID);
        recycler_img.setAdapter(adapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {

            switch (request) {

                case get_system_library_files:

                    imgArray.clear();
                   /* if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }*/
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {

                            emptyView.setVisibility(View.GONE);
                            recycler_img.setVisibility(View.VISIBLE);
                            JSONArray jDataArray = jObj.getJSONArray("data");
                            if (FileType.equalsIgnoreCase("image")) {
                                ImageParsing(jDataArray);
                            } else if (FileType.equalsIgnoreCase("video")) {
                                VideoParsing(jDataArray);
                            } else if (FileType.equalsIgnoreCase("pdf")) {
                                NotesParsing(jDataArray);
                            } else {
                                ImageParsing(jDataArray);
                            }

                        } else {
                            Utils.hideProgressDialog();
                            tv_empty.setText(jObj.getString("message"));
                            recycler_img.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                          //  showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    break;

            }
        }

    }

    private void VideoParsing(JSONArray jDataArray) throws JSONException {
        fileArray.clear();
        tempArray.clear();
        if (jDataArray != null && jDataArray.length() > 0) {

            for (int k = 0; k < jDataArray.length(); k++) {
                JSONObject jfile = jDataArray.getJSONObject(k);
                sysObj = (SystemFiles) jParser.parseJson(jfile, new SystemFiles());
                tempArray.add(sysObj);
            }
            String tempDate = "";
            for (SystemFiles obj : tempArray) {
                if (tempDate.equalsIgnoreCase(obj.getPublishOn())) {
                    fileArray.add(obj);

                } else {
                    SystemFiles sobj = new SystemFiles();
                    sobj.setPublishOn(obj.getPublishOn());
                    sobj.isHeader = true;
                    tempDate = obj.getPublishOn();
                    fileArray.add(sobj);
                    fileArray.add(obj);

                }
            }
            App.video_list = tempArray;
            Log.i("FileType", "==>" + fileArray.get(0).getFileType());
            final Context context = recycler_img.getContext();
            final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
            recycler_img.setLayoutAnimation(controller);
            recycler_img.scheduleLayoutAnimation();
            recycler_img.setLayoutManager(new LinearLayoutManager(context));
            videoAdapter = new VideoAdapter(fileArray, context, SubjectID, ChapterID);

            recycler_img.setAdapter(videoAdapter);

            // DatewiseConvert();
        }
    }

    private void NotesParsing(JSONArray jDataArray) throws JSONException {
        tempArray.clear();
        fileArray.clear();
        if (jDataArray != null && jDataArray.length() > 0) {

            for (int k = 0; k < jDataArray.length(); k++) {
                JSONObject jfile = jDataArray.getJSONObject(k);
                sysObj = (SystemFiles) jParser.parseJson(jfile, new SystemFiles());
                tempArray.add(sysObj);
            }
            String tempDate = "";
            for (SystemFiles obj : tempArray) {
                if (tempDate.equalsIgnoreCase(obj.getPublishOn())) {
                    fileArray.add(obj);

                } else {
                    SystemFiles sobj = new SystemFiles();
                    sobj.setPublishOn(obj.getPublishOn());
                    sobj.isHeader = true;
                    tempDate = obj.getPublishOn();
                    fileArray.add(sobj);
                    fileArray.add(obj);
                }
            }
            Log.i("FileType", "==>" + fileArray.get(0).getFileType());
            final Context context = recycler_img.getContext();
            final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
            recycler_img.setLayoutAnimation(controller);
            recycler_img.scheduleLayoutAnimation();
            recycler_img.setLayoutManager(new LinearLayoutManager(context));
            notesAdapter = new NotesAdapter(fileArray, context, SubjectID, ChapterID);
            recycler_img.setAdapter(notesAdapter);
            Utils.hideProgressDialog();
            //    DatewiseConvert();
        }
    }

    private void ImageParsing(JSONArray jDataArray) throws JSONException {
        if (jDataArray != null && jDataArray.length() > 0) {

            for (int i = 0; i < jDataArray.length(); i++) {
                JSONObject jpaper = jDataArray.getJSONObject(i);
                imgObj = new ImageFile();
                imgObj.setDate(jpaper.getString("Date"));
                JSONArray jfileArray = jpaper.getJSONArray("FileData");
                for (int k = 0; k < jfileArray.length(); k++) {
                    JSONObject jfile = jfileArray.getJSONObject(k);
                    sysObj = (SystemFiles) jParser.parseJson(jfile, new SystemFiles());
                    imgObj.fileArray.add(sysObj);
                }
                imgArray.add(imgObj);


            }
            setupRecyclerView();
            Utils.hideProgressDialog();
        }
    }



    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else
            Log.i("TAG " + tag + " -->", str);
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
