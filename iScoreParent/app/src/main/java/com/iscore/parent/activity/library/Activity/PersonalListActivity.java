package com.iscore.parent.activity.library.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.activity.library.Adapter.FolderAdapter;
import com.iscore.parent.activity.library.Adapter.PersonalListAdapter;
import com.iscore.parent.activity.library.PersonalFiles;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.interfaceparent.BredcumbListner;
import com.iscore.parent.interfaceparent.PersonalFolderCallback;
import com.iscore.parent.R;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.JsonParserUniversal;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class PersonalListActivity extends AppCompatActivity implements AsynchTaskListner, BredcumbListner, PersonalFolderCallback {
    ArrayList<Folder> foldersArray = new ArrayList<>();
    ArrayList<Folder> tempArray = new ArrayList<>();
    ArrayList<PersonalFiles> personalFilesArrayList = new ArrayList<>();
    public Folder folders;
    public PersonalFiles personalFiles;
    public JsonParserUniversal jParser;
    public String FolderID = "", SubjectID = "", Subject = "", FolderPath = "", FilePath = "", TempFilePath = "";

    public FolderAdapter mAdapter;
    public PersonalListAdapter adapter;
    private RecyclerView recyclerView, rvBredcumb;
    public ImageView img_back;
    public TextView tv_title;
    private String currentLocation;
    public BredcumbAdapter bAdapter;
    public RecyclerView.LayoutManager mLayoutManager1;
    public LinearLayout emptyView;
    public TextView tv_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_personal_list);
        Utils.logUser();


        jParser = new JsonParserUniversal();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        rvBredcumb = (RecyclerView) findViewById(R.id.rvBredcumb);
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        emptyView = (LinearLayout) findViewById(R.id.empty_view);
        tv_empty = (TextView) findViewById(R.id.tv_empty);
        tv_title.setText("Library");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        FolderID = getIntent().getStringExtra("FolderID");
        SubjectID = getIntent().getStringExtra("SubjectID");
        Subject = getIntent().getStringExtra("Subject");
        folders = new Folder();
        folders.setFodlerID("-1");
        folders.setFolderName("Library");
        foldersArray.add(folders);
        folders = new Folder();
        folders.setFodlerID(FolderID);
        folders.setFolderName(Subject);
        foldersArray.add(folders);
        RecyclerViewBreadCumb();
        new CallRequest(PersonalListActivity.this).get_personal_library_sub_folder_files_list(FolderID);


    }


    @Override
    public void onBackPressed() {
        if (foldersArray.size() > 1) {

            foldersArray.remove(foldersArray.size() - 1);
            String temp = TempFilePath;
            for (int i = 0; i < foldersArray.size(); i++) {
                if(!foldersArray.get(i).getFodlerID().equals("-1")){
                    temp += foldersArray.get(i).getFodlerID() + "/";
                }

            }
            FilePath = temp;
            Log.i("TAG", "ON BACK :-> " + foldersArray.get(foldersArray.size() - 1).getFodlerID());
            if (foldersArray.get(foldersArray.size() - 1).getFodlerID().equals("-1")) {
                finish();
            } else {
                new CallRequest(PersonalListActivity.this).get_personal_library_sub_folder_files_list(foldersArray.get(foldersArray.size() - 1).getFodlerID());

            }

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Utils.hideProgressDialog();

            switch (request) {

                case get_personal_library_sub_folder_files_list:

                    personalFilesArrayList.clear();


                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {

                            recyclerView.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);
                            if (FolderPath.isEmpty() || FolderPath.equals("")) {
                                FolderPath = jObj.getString("personal_folder_icon");

                            }
                            if (FilePath.isEmpty() || FilePath.equals("")) {
                                FilePath = jObj.getString("personal_file_path");
                                FilePath += FolderID + "/";
                            }
                            TempFilePath = jObj.getString("personal_file_path");

                            JSONArray jLibObj = jObj.getJSONArray("library");
                            if (jLibObj != null && jLibObj.length() > 0) {
                                for (int i = 0; i < jLibObj.length(); i++) {
                                    JSONObject jFiles = jLibObj.getJSONObject(i);
                                    personalFiles = (PersonalFiles) jParser.parseJson(jFiles, new PersonalFiles());
                                    personalFilesArrayList.add(personalFiles);
                                    if (personalFiles.getFileType().equals("video")) {
                                        App.perrsonal_video_list.add(personalFiles);
                                    }
                                }
                            }

                            setupFilesRecyclerView();
                            bAdapter.notifyDataSetChanged();
                            rvBredcumb.scrollToPosition(personalFilesArrayList.size() - 1);
                        } else {
                            Utils.hideProgressDialog();
                            recyclerView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                            tv_empty.setText(jObj.getString("message"));
                            bAdapter.notifyDataSetChanged();
                            rvBredcumb.scrollToPosition(personalFilesArrayList.size() - 1);
                            // showAlert(jObj.getString("message"));
                        }

                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        recyclerView.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        tv_empty.setText("Not Available");
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    private void setupFilesRecyclerView() {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new PersonalListAdapter(personalFilesArrayList, recyclerView, context, FolderID, FolderPath, FilePath);
        recyclerView.setAdapter(adapter);

    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onBackPressed();
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onItemClick(PersonalFiles file) {
        if (adapter != null) {
            if (file.getFileType().equals("folder")) {
                folders = new Folder();
                folders.setFodlerID(file.getFolderID());
                folders.setFolderName(file.getFolderName());
                foldersArray.add(folders);
                FilePath += file.getFolderID() + "/";
                new CallRequest(PersonalListActivity.this).get_personal_library_sub_folder_files_list(file.getFolderID());

            }
        }

    }

    @Override
    public void onBreadcumbClick(String ID) {
        String temp = TempFilePath;
        tempArray.clear();

        for (int i = 0; i < foldersArray.size(); i++) {
            if (foldersArray.get(i).getFodlerID().equals(ID)) {
                temp += foldersArray.get(i).getFodlerID() + "/";
                tempArray.add(foldersArray.get(i));
                break;
            } else {
                temp += foldersArray.get(i).getFodlerID() + "/";
                tempArray.add(foldersArray.get(i));
            }
        }
        FilePath = temp;
        foldersArray.clear();
        for (int i = 0; i < tempArray.size(); i++) {
            foldersArray.add(tempArray.get(i));
        }

        if (foldersArray != null && foldersArray.size() > 0) {
            bAdapter = new BredcumbAdapter(this, foldersArray);
            rvBredcumb.setAdapter(bAdapter);
            new CallRequest(PersonalListActivity.this).get_personal_library_sub_folder_files_list(foldersArray.get(foldersArray.size() - 1).getFodlerID());

        } else {
            folders = new Folder();
            folders.setFodlerID(FolderID);
            folders.setFolderName("Folder");
            foldersArray.add(folders);
            bAdapter = new BredcumbAdapter(this, foldersArray);
            rvBredcumb.setAdapter(bAdapter);
            new CallRequest(PersonalListActivity.this).get_personal_library_sub_folder_files_list(FolderID);

        }
        //  Log.i("TAG", "SIZE  :-> " + foldersArray.size());
    }


    public class Folder implements Serializable {
        public String FodlerID = "", FolderName = "";

        public String getFodlerID() {
            return FodlerID;
        }

        public void setFodlerID(String fodlerID) {
            FodlerID = fodlerID;
        }

        public String getFolderName() {
            return FolderName;
        }

        public void setFolderName(String folderName) {
            FolderName = folderName;
        }
    }

    public void RecyclerViewBreadCumb() {


        mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvBredcumb.setLayoutManager(mLayoutManager1);
        rvBredcumb.setHasFixedSize(true);

        RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                rvBredcumb.removeOnScrollListener(this);

                rvBredcumb.addOnScrollListener(this);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        rvBredcumb.addOnScrollListener(scrollListener);
        bAdapter = new BredcumbAdapter(this, foldersArray);

        rvBredcumb.setAdapter(bAdapter);


    }

    public class BredcumbAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        public Context mContext;
        LayoutInflater inflater;
        Folder Obj;
        private ArrayList<Folder> fArray;
        private static final int VIEW_TYPE_DEFULT = 0;
        public BredcumbListner listner;

        public BredcumbAdapter(Activity context, ArrayList<Folder> fArray) {
            mContext = context;
            inflater = LayoutInflater.from(mContext);
            this.fArray = fArray;
            listner = (BredcumbListner) context;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            return new ViewHolderMenu(inflater.inflate(R.layout.personal_breadcumb_row, parent, false));

        }


        public Folder getItem(int position) {
            return fArray.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return fArray.size();
        }


        public class ViewHolderMenu extends RecyclerView.ViewHolder {
            public TextView tv_lib;
            public ImageView img_video;
            public ProgressBar pBar;


            public ViewHolderMenu(View v) {
                super(v);
                tv_lib = (TextView) v.findViewById(R.id.tv_lib);
            }
        }


        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            switch (holder.getItemViewType()) {
                case 0:
                    ViewHolderMenu offerHolder = (ViewHolderMenu) holder;
                    bindSpaHolder(offerHolder, position);
                    break;
            }

        }

        @Override
        public int getItemViewType(int position) {
            return VIEW_TYPE_DEFULT;
        }


        public void bindSpaHolder(final ViewHolderMenu holder, final int pos) {
            Obj = getItem(pos);
            holder.tv_lib.setText(Obj.getFolderName());
            if (pos == (fArray.size() - 1)) {
                holder.itemView.setAlpha(0.5F);
            } else {
                holder.itemView.setAlpha(1.0F);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pos == 0) {
                        finish();

                    } else {
                        if (pos != (fArray.size() - 1)) {
                            listner.onBreadcumbClick(fArray.get(pos).getFodlerID());
                        }
                    }
                }
            });
        }
    }
}
