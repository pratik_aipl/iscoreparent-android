package com.iscore.parent.activity.library.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.androidquery.AQuery;
import com.iscore.parent.activity.library.SystemFiles;
import com.iscore.parent.R;
import com.iscore.parent.utils.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 2/28/2018.
 */

public class ImageFileAdapter extends RecyclerView.Adapter<ImageFileAdapter.ViewHolder> {

    public Context context;
    public LinearLayoutManager lln;
    public ArrayList<SystemFiles> imgArray;
    public AQuery aQuery;
    public LayoutInflater inflater;

    public String SubjectID = "", ChapterID = "";
    // private CAdapter checkBoxAdapter;


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public ProgressBar pBar;

        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
            pBar = (ProgressBar) itemView.findViewById(R.id.pBar);
        }
    }

    public ImageFileAdapter(ArrayList<SystemFiles> imgArray, Context context, String SubjectID, String ChapterID) {
        this.imgArray = imgArray;
        this.context = context;
        this.SubjectID = SubjectID;
        this.ChapterID = ChapterID;
        aQuery = new AQuery(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_image, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        lln = new LinearLayoutManager(context);
        //aQuery.id(holder.img).progress(holder.pBar).image(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + imgArray.get(position).getFileName(), true, true, 0, R.drawable.profile, null, 0, 0.0f);
        Picasso.with(context)
                .load(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + imgArray.get(position).getFileName())
                .error( R.drawable.profile) //this is optional the image to display while the url image is downloading
                .into(holder.img);
Log.i("Image Path","==>"+Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + imgArray.get(position).getFileName());
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertOffer(imgArray.get(position).getFileName(), view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imgArray.size();
    }

    public void alertOffer(String fileName, View view) {

        // This method will be executed once the ti
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final AlertDialog alertTarge = dialogBuilder.create();
        inflater = LayoutInflater.from(view.getContext());

        View alertDialogView = inflater.inflate(R.layout.alert_image, null);
        ProgressBar pBar = (ProgressBar) alertDialogView.findViewById(R.id.pBar);
        alertTarge.setView(alertDialogView);
        alertTarge.setCancelable(true);

        ImageView imageView = (ImageView) alertDialogView.findViewById(R.id.imageView);
        Picasso.with(context)
                .load(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + fileName)
                .error( R.drawable.profile) //this is optional the image to display while the url image is downloading
                .into(imageView);
       // aQuery.id(imageView).progress(pBar).image(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + fileName, true, true, 0, R.drawable.profile, null, 0, 0.0f);
        alertTarge.show();


    }
}
