package com.iscore.parent.activity.library.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.iscore.parent.activity.library.Activity.LibrarySystemFilesActivity;
import com.iscore.parent.activity.library.Library;
import com.iscore.parent.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by empiere-vaibhav on 2/27/2018.
 */

public class LibraryChapterAdapter extends RecyclerView.Adapter<LibraryChapterAdapter.MyViewHolder> {

    private List<Library> subjectList;
    public Context context;
    public AQuery aQuery;
    public String SubjectID = "", Subject = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name;
        public ImageView img_sub;
        public View lineview;
        public ProgressBar pBar;


        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = (TextView) view.findViewById(R.id.lbl_level_one);
            img_sub = (ImageView) view.findViewById(R.id.img_sub);
            lineview = (View) view.findViewById(R.id.view);

            pBar = (ProgressBar) view.findViewById(R.id.pBar);
        }
    }


    public LibraryChapterAdapter(List<Library> moviesList, Context context, String subID, String Subject) {
        this.subjectList = moviesList;
        this.context = context;
        aQuery = new AQuery(context);
        this.Subject = Subject;
        this.SubjectID = subID;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_subject_revision_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_subject_name.setText(subjectList.get(position).getFolderName());
        //  File image = new File(Constant.LOCAL_IMAGE_PATH + "/subject_icon/" + subjectList.get(position).getSubjectIcon());
      /*  if (image.exists()) {
            try {
                holder.img_sub.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
            } catch (NullPointerException e) {
            }
        }*/

        Picasso.with(context)

                .load(subjectList.get(position).getFolderIcon())
                .error(R.drawable.profile) //this is optional the image to display while the url image is downloading
                .into(holder.img_sub, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.pBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });

      //  aQuery.id(holder.img_sub).progress(null).image(subjectList.get(position).getFolderIcon(), true, true, 0, R.drawable.profile, null, 0, 0.0f);

        holder.lineview.setVisibility(View.GONE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, LibrarySystemFilesActivity.class)
                        .putExtra("FolderID", subjectList.get(position).getFolderID())
                        .putExtra("SubjectID", SubjectID)
                        .putExtra("Subject", Subject)
                        .putExtra("ChapterID", subjectList.get(position).getChapterID())
                        .putExtra("Chapter", subjectList.get(position).getFolderName()));
            }
        });

    }


    @Override
    public int getItemCount() {
        return subjectList.size();
    }
}




