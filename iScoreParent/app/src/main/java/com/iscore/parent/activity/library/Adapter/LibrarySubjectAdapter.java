package com.iscore.parent.activity.library.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.iscore.parent.activity.library.Activity.LibraryChapterActivity;
import com.iscore.parent.activity.library.Activity.PersonalListActivity;
import com.iscore.parent.activity.library.Library;
import com.iscore.parent.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by empiere-vaibhav on 2/23/2018.
 */

public class LibrarySubjectAdapter extends RecyclerView.Adapter<LibrarySubjectAdapter.MyViewHolder> {

    private List<Library> subjectList;
    public Context context;
    public AQuery aQuery;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name;
        public ImageView img_sub;
        public View lineview;
        public ProgressBar pBar;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = (TextView) view.findViewById(R.id.lbl_level_one);
            img_sub = (ImageView) view.findViewById(R.id.img_sub);
            lineview = (View) view.findViewById(R.id.view);
            pBar = (ProgressBar) view.findViewById(R.id.pBar);
        }
    }


    public LibrarySubjectAdapter(List<Library> moviesList, Context context) {
        this.subjectList = moviesList;
        this.context = context;
        aQuery = new AQuery(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_subject_revision_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv_subject_name.setText(subjectList.get(position).getFolderName());

        Picasso.with(context)

                .load(subjectList.get(position).getFolderIcon())
                .error(R.drawable.profile) //this is optional the image to display while the url image is downloading
                .into(holder.img_sub, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.pBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });

        // aQuery.id(holder.img_sub).progress(null).image(subjectList.get(position).getFolderIcon(), true, true, 0, R.drawable.profile, null, 0, 0.0f);
        Log.i("TAG", "SUBJECT  :-> " + subjectList.get(position).getFolderIcon());
        holder.lineview.setVisibility(View.GONE);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (subjectList.get(position).getFolderType().equalsIgnoreCase("system")) {

                    context.startActivity(new Intent(context, LibraryChapterActivity.class)
                            .putExtra("FolderID", subjectList.get(position).getFolderID())
                            .putExtra("SubjectID", subjectList.get(position).getSubjectID())
                            .putExtra("Subject", subjectList.get(position).getFolderName())
                    );
                } else if (subjectList.get(position).getFolderType().equalsIgnoreCase("personal")) {
                    context.startActivity(new Intent(context, PersonalListActivity.class)
                            .putExtra("FolderID", subjectList.get(position).getFolderID())
                            .putExtra("SubjectID", subjectList.get(position).getSubjectID())
                            .putExtra("Subject", subjectList.get(position).getFolderName()));

                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return subjectList.size();
    }
}



