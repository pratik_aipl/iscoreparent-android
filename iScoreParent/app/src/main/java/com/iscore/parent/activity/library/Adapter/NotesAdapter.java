package com.iscore.parent.activity.library.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.iscore.parent.activity.library.Activity.NotesViewActivity;
import com.iscore.parent.activity.library.SystemFiles;
import com.iscore.parent.R;
import com.iscore.parent.utils.ArrowDownloadButton;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.FileDownloader;
import com.iscore.parent.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by empiere-vaibhav on 3/5/2018.
 */

public class NotesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<SystemFiles> stringArrayListHashMap = new ArrayList<>();

    public Context context;
    private LayoutInflater mInflater;
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    public String SubjectID = "", ChapterID = "";
    public String FileUrl, FileName;
    int count = 0;
    int progress = 0;

    public NotesAdapter(ArrayList<SystemFiles> moviesList, Context context, String SubjectID, String ChapterID) {
        this.stringArrayListHashMap = moviesList;
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.SubjectID = SubjectID;
        this.ChapterID = ChapterID;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            return new ViewHolderHeader(mInflater.inflate(R.layout.custom_header_raw, parent, false));
        } else {
            return new ViewHolderContent(mInflater.inflate(R.layout.custom_content_raw, parent, false));
        }
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return stringArrayListHashMap.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderHeader offerHolder = (ViewHolderHeader) holder;
                bindHeaderHolder(offerHolder, position);
                break;
            case 1:
                ViewHolderContent addrHolder = (ViewHolderContent) holder;
                bindContentHolder(addrHolder, position);
                break;
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolderContent extends RecyclerView.ViewHolder {
        TextView tv_file_title, tv_fileSize, tv_date;
        RelativeLayout rel_item;
        LinearLayout rel_download;
        ImageView img_pdf;
        VideoView img_video;
        ArrowDownloadButton button;

        public ViewHolderContent(View v1) {
            super(v1);
            tv_file_title = (TextView) v1.findViewById(R.id.tv_file_title);
            tv_fileSize = (TextView) v1.findViewById(R.id.tv_fileSize);
            tv_date = (TextView) v1.findViewById(R.id.tv_date);
            rel_item = (RelativeLayout) v1.findViewById(R.id.rel_item);
            rel_download = (LinearLayout) v1.findViewById(R.id.rel_download);
            img_video = (VideoView) v1.findViewById(R.id.img_video);
            img_pdf = (ImageView) v1.findViewById(R.id.img_pdf);
            button = (ArrowDownloadButton) v1.findViewById(R.id.arrow_download_button);
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView tv_date;

        public ViewHolderHeader(View v2) {
            super(v2);
            tv_date = (TextView) v2.findViewById(R.id.tv_date);
        }

    }

    public SystemFiles getItem(int position) {
        return stringArrayListHashMap.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).isHeader) {
            return VIEW_TYPE_HEADER;
        } else {

            return VIEW_TYPE_ITEM;
        }
    }

    @SuppressLint("NewApi")
    public void bindHeaderHolder(final ViewHolderHeader headerHolder, final int pos) {
        SystemFiles types = getItem(pos);
        SimpleDateFormat Input = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output = new SimpleDateFormat("MMMM dd, yyyy");
        headerHolder.tv_date.setText(Utils.DateFormetChangeUniversal(Input, output, types.getPublishOn()));
        int queNo = pos + 1;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void bindContentHolder(final ViewHolderContent holder, final int pos) {
        final SystemFiles types = getItem(pos);
        holder.tv_date.setText(types.getDisplayDate());
        holder.tv_fileSize.setText(formatFileSize(types.getFileSize()));
        holder.img_video.setVisibility(View.GONE);
        holder.img_pdf.setVisibility(View.VISIBLE);
        holder.tv_file_title.setText(types.getFileTitle());
        holder.rel_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileUrl = Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + types.getFileName();
                FileName = FileUrl.substring(FileUrl.lastIndexOf("/") + 1);

                new DownloadFile().execute(FileUrl, FileName);

            }
        });
        holder.rel_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NotesViewActivity.class);
                intent.putExtra("URL", Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + types.getFileName());

                context.startActivity(intent);


            }
        });
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FileUrl = Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + types.getFileName();
                FileName = FileUrl.substring(FileUrl.lastIndexOf("/") + 1);

                new DownloadFile().execute(FileUrl, FileName);

                if ((count % 2) == 0) {
                    holder.button.startAnimating();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress = progress + 1;
                                    holder.button.setProgress(progress);

                                }
                            });
                        }
                    }, 800, 20);
                } else {
                    holder.button.reset();
                }
                count++;
            }
        });


    }

    private class DownloadFile extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            FileUrl = strings[0];
            FileName = strings[1];// -> http://maven.apache.org/maven-1.x/maven.pdf
            // -> maven.pdf
            String SDCardPath = Constant.LOCAL_Library_PATH;

            File qDirectory = new File(SDCardPath);
            if (!qDirectory.exists()) qDirectory.mkdirs();
            String qLocalUrl = SDCardPath + "/" + FileName;
            File qLocalFile = new File(qLocalUrl);
            if (!qLocalFile.exists()) {
                FileDownloader.downloadFile(FileUrl, qLocalFile, context.getApplicationContext());

            } else {
                msg = "you have alerady downloaded";

            }
            return msg;
        }

        @Override
        protected void onPostExecute(final String aVoid) {

            Runnable r = new Runnable() {
                @Override
                public void run() {
                    if (aVoid.equals("")) {
                        Utils.showToast("Download Complete", context);

                    } else {
                        Utils.showToast(aVoid, context);
                    }

                }
            };
            r.run();


            //Utils.hideProgressDialog();

        }
    }

    public static String formatFileSize(String size) {
        long Lsize = (long) Double.parseDouble(size);
        String hrSize = "";
        double m = (Lsize / 1024.0);
        DecimalFormat dec = new DecimalFormat("0.00");
        hrSize = dec.format(m).concat(" MB");
        //Log.i("TAG", "Size :-> " + hrSize);
        return hrSize;
    }

    public static class FileOpen {

        public static void openFile(Context context, File url) throws IOException {
            // Create URI
            File file = url;
            Uri uri = Uri.fromFile(file);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}

