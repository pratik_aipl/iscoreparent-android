package com.iscore.parent.activity.library.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.androidquery.AQuery;
import com.iscore.parent.activity.library.Activity.NotesViewActivity;
import com.iscore.parent.activity.library.PersonalFiles;
import com.iscore.parent.activity.library.VideoViewActivity;
import com.iscore.parent.interfaceparent.PersonalFolderCallback;
import com.iscore.parent.R;
import com.iscore.parent.utils.ArrowDownloadButton;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.FileDownloader;
import com.iscore.parent.utils.Utils;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Karan - Empiere on 3/10/2018.
 */

public class PersonalListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<PersonalFiles> stringArrayListHashMap = new ArrayList<>();
    private ArrayList<String> folderArray = new ArrayList<>();
    public Context context;
    private LayoutInflater mInflater;
    private static final int VIEW_TYPE_FOLDER = 0;
    private static final int VIEW_TYPE_IMAGE = 1;
    private static final int VIEW_TYPE_VIDEO = 2;
    private static final int VIEW_TYPE_FILE = 3;
    public String FolderID = "", FolderPath = "", FilePath = "";
    ;
    public String FileUrl, FileName;
    public AQuery aQuery;
    public PersonalFolderCallback callback;
    public LayoutInflater inflater;

    int count = 0;
    int progress = 0;

    public PersonalListAdapter(ArrayList<PersonalFiles> moviesList, RecyclerView recyclerView, Context context, String FolderID, String Folderpath, String FilePath) {
        this.stringArrayListHashMap = moviesList;
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.FolderID = FolderID;
        this.FolderPath = Folderpath;
        this.FilePath = FilePath;
        callback = (PersonalFolderCallback) context;
        aQuery = new AQuery(context);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_FOLDER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_personal_folder, parent, false);
            return new ViewHolderFolder(view);
        } else if (viewType == VIEW_TYPE_IMAGE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_personal_file_row, parent, false);
            return new ViewHolderImage(view);
        } else if (viewType == VIEW_TYPE_VIDEO) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_personal_file_row, parent, false);
            return new ViewHolderVideo(view);
        } else if (viewType == VIEW_TYPE_FILE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_personal_file_row, parent, false);
            return new ViewHolderPdf(view);
        } else {
            return null;
        }
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return stringArrayListHashMap.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderFolder offerHolder = (ViewHolderFolder) holder;
                bindFolder(offerHolder, position);
                break;
            case 1:
                ViewHolderImage imageHolder = (ViewHolderImage) holder;
                bindImage(imageHolder, position);
                break;
            case 2:
                ViewHolderVideo videoHolder = (ViewHolderVideo) holder;
                bindVideo(videoHolder, position);
                break;
            case 3:
                ViewHolderPdf pdfHolder = (ViewHolderPdf) holder;
                bindPdf(pdfHolder, position);
                break;


        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolderFolder extends RecyclerView.ViewHolder {
        ImageView img_folder;
        TextView tv_folder;

        public ViewHolderFolder(View v2) {
            super(v2);
            img_folder = (ImageView) v2.findViewById(R.id.img_folder);
            tv_folder = (TextView) v2.findViewById(R.id.tv_folder);

            v2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callback != null) {
                        int position = getAdapterPosition();
                        if (position < stringArrayListHashMap.size()) {
                            callback.onItemClick(stringArrayListHashMap.get(position));
                        } else {
                        }
                    }
                }
            });
        }

    }

    class ViewHolderImage extends RecyclerView.ViewHolder {
        TextView tv_file_title, tv_fileSize, tv_date;
        RelativeLayout rel_item;
        LinearLayout rel_download;
        ImageView img_pdf;
        VideoView img_video;

        public ViewHolderImage(View v2) {
            super(v2);
            tv_file_title = (TextView) v2.findViewById(R.id.tv_file_title);
            tv_fileSize = (TextView) v2.findViewById(R.id.tv_fileSize);
            tv_date = (TextView) v2.findViewById(R.id.tv_date);
            rel_item = (RelativeLayout) v2.findViewById(R.id.rel_item);
            rel_download = (LinearLayout) v2.findViewById(R.id.rel_download);
            img_video = (VideoView) v2.findViewById(R.id.img_video);
            img_pdf = (ImageView) v2.findViewById(R.id.img_pdf);
        }

    }

    class ViewHolderVideo extends RecyclerView.ViewHolder {
        TextView tv_file_title, tv_fileSize, tv_date;
        RelativeLayout rel_item;
        LinearLayout rel_download;
        ImageView img_pdf;
        VideoView img_video;
        ProgressBar progressBar;
        ArrowDownloadButton button;

        public ViewHolderVideo(View v2) {
            super(v2);
            tv_file_title = (TextView) v2.findViewById(R.id.tv_file_title);
            tv_fileSize = (TextView) v2.findViewById(R.id.tv_fileSize);
            tv_date = (TextView) v2.findViewById(R.id.tv_date);
            rel_item = (RelativeLayout) v2.findViewById(R.id.rel_item);
            rel_download = (LinearLayout) v2.findViewById(R.id.rel_download);
            img_video = (VideoView) v2.findViewById(R.id.img_video);
            progressBar = (ProgressBar) v2.findViewById(R.id.progressbar);
            img_pdf = (ImageView) v2.findViewById(R.id.img_pdf);
            button = (ArrowDownloadButton) v2.findViewById(R.id.arrow_download_button);
        }

    }

    class ViewHolderPdf extends RecyclerView.ViewHolder {
        TextView tv_file_title, tv_fileSize, tv_date;
        RelativeLayout rel_item;
        LinearLayout rel_download;
        ImageView img_pdf;
        VideoView img_video;

        public ViewHolderPdf(View v2) {
            super(v2);
            tv_file_title = (TextView) v2.findViewById(R.id.tv_file_title);
            tv_fileSize = (TextView) v2.findViewById(R.id.tv_fileSize);
            tv_date = (TextView) v2.findViewById(R.id.tv_date);
            rel_item = (RelativeLayout) v2.findViewById(R.id.rel_item);
            rel_download = (LinearLayout) v2.findViewById(R.id.rel_download);
            img_video = (VideoView) v2.findViewById(R.id.img_video);
            img_pdf = (ImageView) v2.findViewById(R.id.img_pdf);
        }

    }

    public PersonalFiles getItem(int position) {
        return stringArrayListHashMap.get(position);
    }


    @Override
    public int getItemViewType(int position) {
        if (getItem(position).getFileType().equals("folder")) {
            return VIEW_TYPE_FOLDER;
        } else if (getItem(position).getFileType().equals("image")) {
            return VIEW_TYPE_IMAGE;
        } else if (getItem(position).getFileType().equals("video")) {
            return VIEW_TYPE_VIDEO;
        } else {
            return VIEW_TYPE_FILE;
        }


    }


    public void bindFolder(final ViewHolderFolder holder, final int pos) {
        PersonalFiles types = getItem(pos);
        aQuery.id(holder.img_folder).progress(null).image(FolderPath, true, true, 0, R.drawable.profile, null, 0, 0.0f);
        holder.tv_folder.setText(types.getFolderName());

    }

    public void bindImage(final ViewHolderImage holder, final int pos) {
        final PersonalFiles types = getItem(pos);
        holder.img_video.setVisibility(View.GONE);
        holder.tv_date.setText(types.getDisplayDate());
        Log.i("TAG", "URL :-> " + FilePath + types.getFileName());
        aQuery.id(holder.img_pdf).progress(null).image(FilePath + types.getFileName(), true, true, 0, R.drawable.profile, null, 0, 0.0f);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertOffer(types.getFileName(), v);
            }
        });

        holder.tv_file_title.setText(types.getFileTitle());
        holder.rel_download.setVisibility(View.GONE);
    }

    public void bindVideo(final ViewHolderVideo holder, final int pos) {
        final PersonalFiles types = getItem(pos);
        holder.img_pdf.setVisibility(View.GONE);

        holder.tv_date.setText(types.getDisplayDate());
        holder.img_video.setVideoURI(Uri.parse(FilePath + types.getFileName()));
        holder.img_video.seekTo(5000);
        holder.progressBar.setVisibility(View.VISIBLE);


        holder.tv_fileSize.setText(formatFileSize(types.getFileSize()));
        holder.img_video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.start();
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int arg1,
                                                   int arg2) {
                        // TODO Auto-generated method stub
                        holder.progressBar.setVisibility(View.GONE);
                        mp.pause();
                    }
                });
            }
        });

        holder.rel_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VideoViewActivity.class);
                intent.putExtra("Type", "personal");
                intent.putExtra("VideoID", types.getFileID());
                intent.putExtra("Path", FilePath);
                context.startActivity(intent);

            }
        });

        if (stringArrayListHashMap.size() == (pos + 1)) {
            Utils.hideProgressDialog();
            //   Log.i("TAg :-> ", "https://test.pemevaluater.parshvaa.com/library_system/1/1/" + types.getFileName());

        }

        holder.rel_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FileUrl = FilePath + types.getFileName();
                FileName = FileUrl.substring(FileUrl.lastIndexOf("/") + 1);

                new DownloadFile().execute(FileUrl, FileName);

                if ((count % 2) == 0) {
                    holder.button.startAnimating();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress = progress + 1;
                                    holder.button.setProgress(progress);

                                }
                            });
                        }
                    }, 800, 20);
                } else {
                    holder.button.reset();
                }
                count++;
            }
        });
        holder.tv_file_title.setText(types.getFileTitle());


    }

    public void bindPdf(final ViewHolderPdf holder, final int pos) {
        final PersonalFiles types = getItem(pos);
        holder.img_video.setVisibility(View.GONE);
        aQuery.id(holder.img_pdf).progress(null).image(types.getFileName(), true, true, 0, R.drawable.pdf_icon, null, 0, 0.0f);

        holder.tv_date.setText(types.getDisplayDate());
        holder.tv_fileSize.setText(formatFileSize(types.getFileSize()));
        holder.img_video.setVisibility(View.GONE);
        holder.img_pdf.setVisibility(View.VISIBLE);
        holder.tv_file_title.setText(types.getFileTitle());
        holder.rel_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileUrl = FilePath + types.getFileName();
                FileName = types.getFileName();

                new DownloadFile().execute(FileUrl, FileName);
            }
        });
        holder.rel_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NotesViewActivity.class);
                intent.putExtra("URL", FilePath + types.getFileName());
                context.startActivity(intent);


            }
        });

    }

    public static String formatFileSize(String size) {
        long Lsize = (long) Double.parseDouble(size);
        String hrSize = "";
        double m = (Lsize / 1024.0);
        DecimalFormat dec = new DecimalFormat("0.00");
        hrSize = dec.format(m).concat(" MB");
        //Log.i("TAG", "Size :-> " + hrSize);
        return hrSize;
    }

    public void alertOffer(String fileName, View view) {

        // This method will be executed once the ti
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final AlertDialog alertTarge = dialogBuilder.create();
        inflater = LayoutInflater.from(view.getContext());

        View alertDialogView = inflater.inflate(R.layout.alert_image, null);
        ProgressBar pBar = (ProgressBar) alertDialogView.findViewById(R.id.pBar);
        alertTarge.setView(alertDialogView);
        alertTarge.setCancelable(true);

        ImageView imageView = (ImageView) alertDialogView.findViewById(R.id.imageView);
        aQuery.id(imageView).progress(pBar).image(FilePath + fileName, true, true, 0, R.drawable.profile, null, 0, 0.0f);
        alertTarge.show();


    }

    private class DownloadFile extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            FileUrl = strings[0];
            FileName = strings[1];// -> http://maven.apache.org/maven-1.x/maven.pdf
            // -> maven.pdf
            String SDCardPath = Constant.LOCAL_Library_PATH;

            File qDirectory = new File(SDCardPath);
            if (!qDirectory.exists()) qDirectory.mkdirs();
            String qLocalUrl = SDCardPath + "/" + FileName;
            File qLocalFile = new File(qLocalUrl);
            if (!qLocalFile.exists()) {
                FileDownloader.downloadFile(FileUrl, qLocalFile, context.getApplicationContext());

            } else {
                msg = "you have alerady downloaded";

            }
            return msg;
        }

        @Override
        protected void onPostExecute(final String aVoid) {

            Runnable r = new Runnable() {
                @Override
                public void run() {
                    if (aVoid.equals("")) {
                        Utils.showToast("Download Complete", context);

                    } else {
                        Utils.showToast(aVoid, context);
                    }

                }
            };
            r.run();


            //Utils.hideProgressDialog();

        }
    }
}

