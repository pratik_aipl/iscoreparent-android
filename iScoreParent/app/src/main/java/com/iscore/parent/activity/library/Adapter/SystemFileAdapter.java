package com.iscore.parent.activity.library.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.iscore.parent.activity.library.ImageFile;
import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 2/28/2018.
 */

public class SystemFileAdapter extends RecyclerView.Adapter<SystemFileAdapter.ViewHolder> {

    public Context context;
    public LinearLayoutManager lln;
    public ArrayList<ImageFile> imgArray;
    private ImageFileAdapter checkBoxAdapter;
    public String SubjectID = "", ChapterID = "";

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_date;
        public RecyclerView recycler_img;

        public ViewHolder(View itemView) {
            super(itemView);
            recycler_img = (RecyclerView) itemView.findViewById(R.id.recycler_img);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
        }
    }

    public SystemFileAdapter(ArrayList<ImageFile> imgArray, Context context, String SubjectID, String ChapterID) {
        this.imgArray = imgArray;
        this.context = context;

        this.SubjectID = SubjectID;
        this.ChapterID = ChapterID;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_lib_image, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        lln = new LinearLayoutManager(context);
        holder.recycler_img.setLayoutManager(lln);
        StaggeredGridLayoutManager sgaggeredGridLayoutManager = new StaggeredGridLayoutManager(3, 1);
        holder.recycler_img.setLayoutManager(sgaggeredGridLayoutManager);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_from_right);
        holder.recycler_img.setLayoutAnimation(controller);
        holder.recycler_img.setHasFixedSize(true);

        SimpleDateFormat Input = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output = new SimpleDateFormat("MMMM dd, yyyy");
        holder.tv_date.setText(Utils.DateFormetChangeUniversal(Input, output, imgArray.get(position).getDate()));
        checkBoxAdapter = new ImageFileAdapter(imgArray.get(position).fileArray, context, SubjectID, ChapterID);
        holder.recycler_img.setAdapter(checkBoxAdapter);


    }

    @Override
    public int getItemCount() {
        return imgArray.size();
    }


}
