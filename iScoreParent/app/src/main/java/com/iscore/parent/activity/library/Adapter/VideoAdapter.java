package com.iscore.parent.activity.library.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.androidquery.AQuery;
import com.iscore.parent.activity.library.SystemFiles;
import com.iscore.parent.activity.library.VideoViewActivity;
import com.iscore.parent.R;
import com.iscore.parent.utils.ArrowDownloadButton;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.FileDownloader;
import com.iscore.parent.utils.Utils;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by empiere-vaibhav on 3/5/2018.
 */

public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<SystemFiles> stringArrayListHashMap = new ArrayList<>();
    ;
    public Context context;
    int clickedPos = -1;
    private LayoutInflater mInflater;
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    public AQuery aQuery;
    public String SubjectID = "", ChapterID = "";
    Bitmap bitmap = null;
    public String FileUrl, FileName;

    int count = 0;
    int progress = 0;

    public VideoAdapter(ArrayList<SystemFiles> moviesList, Context context, String SubjectID, String ChapterID) {
        this.stringArrayListHashMap = moviesList;
        this.context = context;
        this.SubjectID = SubjectID;
        this.ChapterID = ChapterID;
        mInflater = LayoutInflater.from(context);
        aQuery = new AQuery(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_HEADER) {
            return new ViewHolderHeader(mInflater.inflate(R.layout.custom_header_raw, parent, false));
        } else {
            return new ViewHolderContent(mInflater.inflate(R.layout.custom_content_raw, parent, false));
        }
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return stringArrayListHashMap.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderHeader offerHolder = (ViewHolderHeader) holder;
                bindHeaderHolder(offerHolder, position);
                break;
            case 1:
                ViewHolderContent addrHolder = (ViewHolderContent) holder;
                bindContentHolder(addrHolder, position);
                break;
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolderContent extends RecyclerView.ViewHolder {
        TextView tv_file_title, tv_fileSize, tv_date;
        VideoView img_video;
        RelativeLayout rel_item, rel_video;
        ProgressBar progressBar;
        ImageView img_pdf;


        ArrowDownloadButton button;

        public ViewHolderContent(View v1) {
            super(v1);
            tv_file_title = (TextView) v1.findViewById(R.id.tv_file_title);
            tv_fileSize = (TextView) v1.findViewById(R.id.tv_fileSize);
            tv_date = (TextView) v1.findViewById(R.id.tv_date);
            rel_item = (RelativeLayout) v1.findViewById(R.id.rel_item);
            rel_video = (RelativeLayout) v1.findViewById(R.id.rel_video);
            img_pdf = (ImageView) v1.findViewById(R.id.img_pdf);
            img_video = (VideoView) v1.findViewById(R.id.img_video);
            progressBar = (ProgressBar) v1.findViewById(R.id.progressbar);
            button = (ArrowDownloadButton) v1.findViewById(R.id.arrow_download_button);
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        TextView tv_date;

        public ViewHolderHeader(View v2) {
            super(v2);
            tv_date = (TextView) v2.findViewById(R.id.tv_date);
        }

    }

    public SystemFiles getItem(int position) {

        return stringArrayListHashMap.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position).isHeader) {
            return VIEW_TYPE_HEADER;

        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @SuppressLint("NewApi")
    public void bindHeaderHolder(final ViewHolderHeader headerHolder, final int pos) {
        SystemFiles types = getItem(pos);
        SimpleDateFormat Input = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat output = new SimpleDateFormat("MMMM dd, yyyy");
        headerHolder.tv_date.setText(Utils.DateFormetChangeUniversal(Input, output, types.getPublishOn()));

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void bindContentHolder(final ViewHolderContent holder, final int pos) {
        final SystemFiles types = getItem(pos);

        holder.tv_date.setText(types.getDisplayDate());
        holder.img_video.setVideoURI(Uri.parse(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + types.getFileName()));
        holder.img_video.seekTo(5000);
        holder.progressBar.setVisibility(View.VISIBLE);

        holder.tv_fileSize.setText(formatFileSize(types.getFileSize()));
        holder.img_video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mp.start();
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int arg1,
                                                   int arg2) {
                        // TODO Auto-generated method stub
                        holder.progressBar.setVisibility(View.GONE);
                        mp.pause();
                    }
                });
            }
        });

        holder.rel_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VideoViewActivity.class);
                intent.putExtra("Type", "System");
                intent.putExtra("LFID", types.getLFID());
                intent.putExtra("SubjectID", SubjectID);
                intent.putExtra("ChapterID", ChapterID);
                context.startActivity(intent);

            }
        });

        if (stringArrayListHashMap.size() == (pos + 1)) {
            Utils.hideProgressDialog();
            //   Log.i("TAg :-> ", "https://test.pemevaluater.parshvaa.com/library_system/1/1/" + types.getFileName());

        }

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FileUrl = Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + ChapterID + "/" + types.getFileName();
                FileName = FileUrl.substring(FileUrl.lastIndexOf("/") + 1);

                new DownloadFile().execute(FileUrl, FileName);

                if ((count % 2) == 0) {
                    holder.button.startAnimating();
                    Timer timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            ((Activity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress = progress + 1;
                                    holder.button.setProgress(progress);

                                }
                            });
                        }
                    }, 800, 20);
                } else {
                    holder.button.reset();
                }
                count++;
            }
        });
        holder.tv_file_title.setText(types.getFileTitle());


    }

    public static String formatFileSize(String size) {
        long Lsize = (long) Double.parseDouble(size);
        String hrSize = "";
        double m = (Lsize / 1024.0);
        DecimalFormat dec = new DecimalFormat("0.00");
        hrSize = dec.format(m).concat(" MB");
        //Log.i("TAG", "Size :-> " + hrSize);
        return hrSize;
    }

    private class DownloadFile extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {


        }

        @Override
        protected String doInBackground(String... strings) {
            String msg = "";
            FileUrl = strings[0];
            FileName = strings[1];// -> http://maven.apache.org/maven-1.x/maven.pdf
            // -> maven.pdf
            String SDCardPath = Constant.LOCAL_Library_PATH;

            File qDirectory = new File(SDCardPath);
            if (!qDirectory.exists()) qDirectory.mkdirs();
            String qLocalUrl = SDCardPath + "/" + FileName;
            File qLocalFile = new File(qLocalUrl);
            if (!qLocalFile.exists()) {
                FileDownloader.downloadFile(FileUrl, qLocalFile, context.getApplicationContext());

            } else {
                msg = "you have alerady downloaded";

            }
            return msg;
        }

        @Override
        protected void onPostExecute(final String aVoid) {

            Runnable r = new Runnable() {
                @Override
                public void run() {
                    if (aVoid.equals("")) {
                        Utils.showToast("Download Complete", context);

                    } else {
                        Utils.showToast(aVoid, context);
                    }

                }
            };
            r.run();


            //Utils.hideProgressDialog();

        }
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }
}


