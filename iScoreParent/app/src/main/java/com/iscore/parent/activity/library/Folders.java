package com.iscore.parent.activity.library;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/10/2018.
 */

public class Folders implements Serializable {
    public String FolderID = "";
    public String StandardID = "";
    public String ClassID = "";
    public String ParentID = "";
    public int mType;


    public String getFolderID() {
        return FolderID;
    }

    public void setFolderID(String folderID) {
        FolderID = folderID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getParentID() {
        return ParentID;
    }

    public void setParentID(String parentID) {
        ParentID = parentID;
    }

    public String getFolderName() {
        return FolderName;
    }

    public void setFolderName(String folderName) {
        FolderName = folderName;
    }

    public String getFileCount() {
        return FileCount;
    }

    public void setFileCount(String fileCount) {
        FileCount = fileCount;
    }

    public String FolderName = "";
    public String FileCount = "";
}
