package com.iscore.parent.activity.library;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 2/28/2018.
 */

public class ImageFile implements Serializable {
    public String Date = "";

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public ArrayList<SystemFiles> fileArray = new ArrayList<>();
}
