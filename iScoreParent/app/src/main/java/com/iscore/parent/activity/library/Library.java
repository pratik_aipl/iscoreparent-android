package com.iscore.parent.activity.library;

import java.io.Serializable;

public class Library implements Serializable {
    public String FolderID = "";
    public String ParentID = "";
    public String FolderName = "";
    public String ChapterID="";

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getFolderID() {
        return FolderID;
    }

    public void setFolderID(String folderID) {
        FolderID = folderID;
    }

    public String getParentID() {
        return ParentID;
    }

    public void setParentID(String parentID) {
        ParentID = parentID;
    }

    public String getFolderName() {
        return FolderName;
    }

    public void setFolderName(String folderName) {
        FolderName = folderName;
    }

    public String getFolderIcon() {
        return FolderIcon;
    }

    public void setFolderIcon(String folderIcon) {
        FolderIcon = folderIcon;
    }

    public String getFileCount() {
        return FileCount;
    }

    public void setFileCount(String fileCount) {
        FileCount = fileCount;
    }

    public String getFolderType() {
        return FolderType;
    }

    public void setFolderType(String folderType) {
        FolderType = folderType;
    }

    public String FolderIcon = "";
    public String FileCount = "";
    public String FolderType = "";
    public String SubjectID = "";

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }
}
