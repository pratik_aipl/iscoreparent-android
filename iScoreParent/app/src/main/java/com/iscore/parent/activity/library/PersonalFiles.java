package com.iscore.parent.activity.library;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/10/2018.
 */

public class PersonalFiles implements Serializable {

    public String FolderID = "";

    public String getFileID() {
        return FileID;
    }

    public void setFileID(String fileID) {
        FileID = fileID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getParentID() {
        return ParentID;
    }

    public void setParentID(String parentID) {
        ParentID = parentID;
    }

    public String getFolderName() {
        return FolderName;
    }

    public void setFolderName(String folderName) {
        FolderName = folderName;
    }

    public String getFileCount() {
        return FileCount;
    }

    public void setFileCount(String fileCount) {
        FileCount = fileCount;
    }

    public String FileID = "";
    public String StandardID = "";
    public String ClassID = "";

    public String ParentID = "";
    public String FolderName = "";

    public String FolderType = "";
    public String FileType = "";
    public String FileCount = "";
    public String FileName = "";
    public String FileTitle = "";
    public String FileSize = "";
    public String DisplayDate = "";
    public String PublishOn = "";

    public String getDisplayDate() {
        return DisplayDate;
    }

    public void setDisplayDate(String displayDate) {
        DisplayDate = displayDate;
    }

    public String getPublishOn() {
        return PublishOn;
    }

    public void setPublishOn(String publishOn) {
        PublishOn = publishOn;
    }

    public PersonalFiles() {
    }

    public String getFolderID() {
        return FolderID;
    }

    public void setFolderID(String folderID) {
        FolderID = folderID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getFolderType() {
        return FolderType;
    }

    public void setFolderType(String folderType) {
        FolderType = folderType;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileTitle() {
        return FileTitle;
    }

    public void setFileTitle(String fileTitle) {
        FileTitle = fileTitle;
    }

    public String getFileSize() {
        return FileSize;
    }

    public void setFileSize(String fileSize) {
        FileSize = fileSize;
    }


}
