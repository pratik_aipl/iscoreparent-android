package com.iscore.parent.activity.library;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 2/28/2018.
 */

public class SystemFiles implements Serializable {

    public String LFID = "";
    public String FolderType = "";
    public String FileType = "";
    public String FileName = "";
    public String FileTitle = "";
    public String DisplayDate="";

    public String getDisplayDate() {
        return DisplayDate;
    }

    public void setDisplayDate(String displayDate) {
        DisplayDate = displayDate;
    }

    public String PublishOn = "";
    public String FileSize = "";
    public boolean isHeader=false;

    public String getFolderType() {
        return FolderType;
    }

    public void setFolderType(String folderType) {
        FolderType = folderType;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getLFID() {
        return LFID;
    }

    public void setLFID(String LFID) {
        this.LFID = LFID;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileTitle() {
        return FileTitle;
    }

    public void setFileTitle(String fileTitle) {
        FileTitle = fileTitle;
    }

    public String getPublishOn() {
        return PublishOn;
    }

    public void setPublishOn(String publishOn) {
        PublishOn = publishOn;
    }

    public String getFileSize() {
        return FileSize;
    }

    public void setFileSize(String fileSize) {
        FileSize = fileSize;
    }


}
