package com.iscore.parent.activity.library;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.iscore.parent.activity.library.videoPleyar.BetterVideoPlayer;
import com.iscore.parent.App;
import com.iscore.parent.R;
import com.iscore.parent.utils.Constant;

public class VideoViewActivity extends AppCompatActivity {
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    public String SubjectID = "", ChapterID = "";
    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private BetterVideoPlayer mBetterVideoPlayer;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mBetterVideoPlayer.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    };
    private boolean mVisible;
    public String video_id, Type = "", personal_Path = "";
    public int pos = 0;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };
    public RelativeLayout rel_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);
        Type = getIntent().getStringExtra("Type");
        rel_main = (RelativeLayout) findViewById(R.id.rel_main);
        mBetterVideoPlayer = (BetterVideoPlayer) findViewById(R.id.bvp);
        if (Type.equals("personal")) {
            mVisible = true;
            video_id = getIntent().getStringExtra("VideoID");
            personal_Path = getIntent().getStringExtra("Path");
            for (int i = 0; i < App.perrsonal_video_list.size(); i++) {
                if (video_id.equalsIgnoreCase(App.perrsonal_video_list.get(i).getFileID())) {
                    mBetterVideoPlayer.setSource(Uri.parse(personal_Path + App.perrsonal_video_list.get(i).getFileName()));
                    pos = i;
                }
            }


        } else {
            video_id = getIntent().getStringExtra("LFID");
            SubjectID = getIntent().getStringExtra("SubjectID");
            ChapterID = getIntent().getStringExtra("ChapterID");
            mVisible = true;

            for (int i = 0; i < App.video_list.size(); i++) {
                if (video_id.equalsIgnoreCase(App.video_list.get(i).getLFID())) {
                    mBetterVideoPlayer.setSource(Uri.parse(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + SubjectID + "/" + App.video_list.get(i).getFileName()));
                    pos = i;
                }
            }
        }
        if (pos == 0) {
            mBetterVideoPlayer.btnPrev.setAlpha(0.5F);
            mBetterVideoPlayer.btnPrev.setClickable(false);
        }

        mBetterVideoPlayer.pause();
        mBetterVideoPlayer.btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos--;

                if (pos > 0) {
                    mBetterVideoPlayer.reset();
                    mBetterVideoPlayer.mProgressBar.setVisibility(View.VISIBLE);
                    if (Type.equals("personal")) {
                        mBetterVideoPlayer.setSource(Uri.parse(personal_Path + App.perrsonal_video_list.get(pos).getFileName()));
                        if ((pos - 1) == 0) {
                            mBetterVideoPlayer.btnPrev.setAlpha(0.5F);
                            mBetterVideoPlayer.btnPrev.setClickable(false);
                        } else {
                            mBetterVideoPlayer.btnPrev.setAlpha(1.0F);
                            mBetterVideoPlayer.btnPrev.setClickable(true);
                        }


                    } else {
                        mBetterVideoPlayer.setSource(Uri.parse(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + SubjectID + "/" + App.video_list.get(pos).getFileName()));

                    }
                    mBetterVideoPlayer.start();
                    //      Log.i("TAG", " NEXT  :-> " + "https://test.pemevaluater.parshvaa.com/library_system/" + SubjectID + "/" + SubjectID + "/" + App.video_list.get(pos).getFileName());

                } else {
                    pos++;
                    //   Log.i("TAG", " NEXT ELSE :-> " + pos);
                }


            }
        });

        mBetterVideoPlayer.btnNext.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                pos++;
                mBetterVideoPlayer.reset();
                mBetterVideoPlayer.mProgressBar.setVisibility(View.VISIBLE);
                if (Type.equals("personal")) {
                    if (App.perrsonal_video_list.size() > pos) {
                        mBetterVideoPlayer.setSource(Uri.parse(personal_Path + App.perrsonal_video_list.get(pos).getFileName()));
                        mBetterVideoPlayer.btnPrev.setAlpha(1.0F);
                        mBetterVideoPlayer.btnPrev.setClickable(true);
                        if (pos == (App.perrsonal_video_list.size() - 1)) {
                            mBetterVideoPlayer.btnNext.setAlpha(0.5F);
                            mBetterVideoPlayer.btnNext.setClickable(false);
                        } else {
                            mBetterVideoPlayer.btnNext.setAlpha(1.0F);
                            mBetterVideoPlayer.btnNext.setClickable(true);
                        }
                    } else {
                        mBetterVideoPlayer.mProgressBar.setVisibility(View.GONE);
                        pos--;
                        //  Log.i("TAG", " NEXT ELSE :-> " + pos);
                    }
                } else {
                    if (App.video_list.size() > pos) {
                        mBetterVideoPlayer.setSource(Uri.parse(Constant.EVEL_LIB_SYSTEM_IMAGE_PATH + SubjectID + "/" + SubjectID + "/" + App.video_list.get(pos).getFileName()));

                    } else {
                        pos--;
                        //  Log.i("TAG", " NEXT ELSE :-> " + pos);
                    }
                }


                mBetterVideoPlayer.start();
                //    Log.i("TAG", " NEXT  :-> " + "https://test.pemevaluater.parshvaa.com/library_system/" + SubjectID + "/" + SubjectID + "/" + App.video_list.get(pos).getFileName());


            }
        });
        mBetterVideoPlayer.enableSwipeGestures(

                getWindow());
        mBetterVideoPlayer.getToolbar().

                setTitle("FullScreen Sample");
        mBetterVideoPlayer.getToolbar()
                .

                        setNavigationIcon(android.support.v7.appcompat.R.drawable.abc_ic_ab_back_material);
        mBetterVideoPlayer.getToolbar().

                setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        rel_main.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        // findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mVisible = false;
        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mBetterVideoPlayer.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;
        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}

