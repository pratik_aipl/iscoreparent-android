package com.iscore.parent.activity.library.videoPleyar;

/**
 * @author Aidan Follestad
 * Modified by Halil Ozercan
 */
public interface BetterVideoProgressCallback {

    void onVideoProgressUpdate(int position, int duration);
}
