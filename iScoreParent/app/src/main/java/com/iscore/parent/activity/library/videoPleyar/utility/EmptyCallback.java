package com.iscore.parent.activity.library.videoPleyar.utility;



import com.iscore.parent.activity.library.videoPleyar.BetterVideoCallback;
import com.iscore.parent.activity.library.videoPleyar.BetterVideoPlayer;

public class EmptyCallback implements BetterVideoCallback {
    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {

    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }
}
