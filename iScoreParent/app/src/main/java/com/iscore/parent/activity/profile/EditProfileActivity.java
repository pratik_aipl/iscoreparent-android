package com.iscore.parent.activity.profile;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.utils.CallRequest;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity implements AsynchTaskListner {

    public EditProfileActivity instance;

    public EditText et_target_goal, et_name, et_last_name, et_previous_goal;
    public CircleImageView img_profile;
    Uri selectedUri;
    public String profImagePath = "", PleyaerID = "";
    ;
    public App app;
    public Button btn_update;
    public ImageView img_back;
    public TextView tv_title, tv_email, tv_mob_no;
    Bitmap bitmap;
    public AQuery aQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_edit_profile);
        Utils.logUser();

        instance = this;
        aQuery = new AQuery(instance);

        app = App.getInstance();
        img_back = (ImageView) findViewById(R.id.img_back);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("Edit Profile");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        img_profile = (CircleImageView) findViewById(R.id.img_profile);

        et_target_goal = (EditText) findViewById(R.id.et_target_goal);
        et_previous_goal = (EditText) findViewById(R.id.tv_previous_goal);
        et_name = (EditText) findViewById(R.id.et_name);
        et_last_name = (EditText) findViewById(R.id.et_last_name);
        tv_email = (TextView) findViewById(R.id.tv_email);
        tv_mob_no = (TextView) findViewById(R.id.tv_mob_no);
        btn_update = (Button) findViewById(R.id.btn_update);
        PleyaerID = Utils.OneSignalPlearID();

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        et_target_goal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {

                    String n = et_target_goal.getText().toString();

                    if (n.length() > 0) {
                        int number = Integer.parseInt(n);
                        if (number > 100) {

                            et_target_goal.setText(n.substring(0, n.length() - 1));
                            et_target_goal.setSelection(et_target_goal.getText().length());
                            Utils.showToast("Target should be only Lessthen or equal 100", instance);
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        et_previous_goal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {

                    String n = et_previous_goal.getText().toString();

                    if (n.length() > 0) {
                        int number = Integer.parseInt(n);
                        if (n.length() > 3) {
                            if (number > 100) {
                                et_previous_goal.setText(n.substring(0, n.length() - 1));
                                et_previous_goal.setSelection(et_target_goal.getText().length());
                                Utils.showToast("Target should be only Lessthen or equal 100", instance);
                            }
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_name.setText(app.mainUser.getFirstName());
        et_last_name.setText(app.mainUser.getLastName());
        tv_mob_no.setText(app.mainUser.getRegMobNo());
        tv_email.setText(app.mainUser.getEmailID());
        et_target_goal.setText(app.mySharedPref.getlblTarget());
        et_previous_goal.setText(app.mySharedPref.getlblPrviousTarget());
        if (!app.mySharedPref.getProf_image().isEmpty()) {
            File imageFile = new File(app.mySharedPref.getProf_image());
            if (imageFile.isFile()) {
                img_profile.setImageBitmap(BitmapFactory.decodeFile(app.mySharedPref.getProf_image()));

            } else {
                img_profile.setImageResource(R.drawable.profile);
            }

        } else {
            aQuery.id(img_profile).progress(null).image(app.mainUser.getProfile_image(), true, true, 0, R.drawable.profile, null, 0, 0.0f);
        }
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (profImagePath != "") {
                    app.mySharedPref.setProfileImage(profImagePath);
                }
                String SDCardPath = Constant.LOCAL_IMAGE_PATH + "/" + profImagePath + "";

                File f = new File(SDCardPath);
                if (!f.exists()) {
                    f.mkdirs();

                }

                if (et_name.getText().toString().equals("")) {
                    et_name.setError("Plase Enter Firstname");
                } else if (et_last_name.getText().toString().equals("")) {
                    et_last_name.setError("Plase Enter LastName");
                } else if (et_target_goal.getText().toString().equals("")) {
                    et_target_goal.setError("Plase Enter Target");
                } else if (et_previous_goal.getText().toString().equals("")) {
                    et_previous_goal.setError("Plase Enter Previous goal");
                } else if (PleyaerID.equalsIgnoreCase("")) {
                    et_previous_goal.setError("Please Try Again");
                } else {
                    new CallRequest(instance).upadateProfile(et_name.getText().toString(), et_last_name.getText().toString(), et_target_goal.getText().toString(), et_previous_goal.getText().toString(), profImagePath, PleyaerID);
                }
            }
        });


    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath().toString();
                Savefile(profImagePath);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                img_profile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath().toString();
                Savefile(profImagePath);
                app.Image = selectedUri.getPath().toString();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    public void Savefile(String path) {
        File direct = new File(Environment.getExternalStorageDirectory() + "/MyAppFolder/iScore/");
        File file = new File(Environment.getExternalStorageDirectory() + "/MyAppFolder/iScore/" + ".png");

        if (!direct.exists()) {
            direct.mkdir();
        }

        if (!file.exists()) {
            try {
                file.createNewFile();
                FileChannel src = new FileInputStream(path).getChannel();
                FileChannel dst = new FileOutputStream(file).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Utils.hideProgressDialog();
            try {
                switch (request) {
                    case upadateProfile:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            app.mainUser.setFirstName(et_name.getText().toString());
                            app.mainUser.setLastName(et_last_name.getText().toString());
                            app.mainUser.setProfileLastSync(jObj.getString("ProfileLastSync"));

                            app.mySharedPref.setFirst_name(app.mainUser.getFirstName());
                            app.mySharedPref.setLast_name(app.mainUser.getLastName());
                            app.mySharedPref.setlblTarget(et_target_goal.getText().toString());

                            app.mySharedPref.setlblPrviousTarget(et_previous_goal.getText().toString());

                            String fileName = app.Image.substring(app.Image.lastIndexOf("/") + 1);
                            String inputpath = app.Image.substring(0, app.Image.lastIndexOf("/") + 1);
                            copyFile(inputpath, fileName, Constant.LOCAL_IMAGE_PATH + "/profile/");

                            Utils.showToast("Your Profile Update successfully....  !", instance);
                            onBackPressed();


                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void copyFile(String inputPath, String fileName, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + fileName);
            out = new FileOutputStream(outputPath + fileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;
            app.mySharedPref.setProfileImage(outputPath + fileName);
            Log.i("tag", "mySharedPref :-> " + app.mySharedPref.getProf_image() + "  PATH :-> " + outputPath + fileName);
        } catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }
}
