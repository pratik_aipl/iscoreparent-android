package com.iscore.parent.activity.profile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonArray;
import com.iscore.parent.activity.DashBoardActivity;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

public class MyProfileActivity extends AppCompatActivity implements AsynchTaskListner {
    public MyProfileActivity instance;
    public static MyProfileActivity Staticinstance;
    public App app;

    public WebView progress;
    public ImageView img_edit_prof,img_edit_info;
    public TextView btn_subscrib,tv_licence_status,tv_title, tv_days, tv_medium, tv_board, tv_std, tv_key, tv_mobile, tv_email, tv_name, tv_full_name, tv_stud_id, tv_previous_score, tv_sync_time;

    public ImageView img_back;
    public CircleImageView img_profile;
    public AQuery aQuery;

    public String testHeaderIDS = "";
    public FloatingActionButton fab_sync;



    public static JsonArray jStudQuePaperArray;
    public static JsonArray jStudHdrArray;
    public static JsonArray jBoardPaperArray;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_my_profile);
        Utils.logUser();


        instance = this;
        Staticinstance = this;
        aQuery = new AQuery(instance);

        app = App.getInstance();
         progress = (WebView) findViewById(R.id.progress);
        img_edit_prof = (ImageView) findViewById(R.id.img_edit_prof);
        img_edit_info = (ImageView) findViewById(R.id.img_edit_info);
        img_profile = (CircleImageView) findViewById(R.id.img_profile);

        tv_key = (TextView) findViewById(R.id.tv_key);
        tv_sync_time = (TextView) findViewById(R.id.tv_sync_time);
        tv_days = (TextView) findViewById(R.id.tv_days);
        tv_medium = (TextView) findViewById(R.id.tv_medium);
        tv_board = (TextView) findViewById(R.id.tv_board);
        tv_std = (TextView) findViewById(R.id.tv_std);
        fab_sync = (FloatingActionButton) findViewById(R.id.fab_sync);
        tv_mobile = (TextView) findViewById(R.id.tv_mobile);
        tv_email = (TextView) findViewById(R.id.tv_email);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_full_name = (TextView) findViewById(R.id.tv_full_name);
        tv_stud_id = (TextView) findViewById(R.id.tv_stud_id);
        tv_licence_status= (TextView) findViewById(R.id.tv_licence_status);
        btn_subscrib = (TextView) findViewById(R.id.btn_subscrib);
        tv_previous_score = (TextView) findViewById(R.id.tv_previous_score);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(instance, DashBoardActivity.class);
                startActivity(intent);
            }
        });

        tv_title = (TextView) findViewById(R.id.tv_title);
        setData();
        img_edit_prof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(instance, EditProfileActivity.class);
                startActivity(intent);

            }
        });
        img_edit_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(instance, EditProfileActivity.class);
                startActivity(intent);

            }
        });
        fab_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(instance)) {
                 } else {
                    Utils.showToast(getResources().getString(R.string.conect_internet), instance);

                }
            }
        });

        if (App.mySharedPref.getVersion().equals("0")) {
            tv_licence_status.setText("Licence : Purchased");
          //  btn_subscrib.setVisibility(View.GONE);
        } else {
            tv_licence_status.setText("Licence : Demo");
            btn_subscrib.setVisibility(View.VISIBLE);

        }
        DayCont();
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onResume() {
        super.onResume();
        setData();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setData() {

        App.mcqTotalAccuracy = 0;


        tv_title.setText("My Profile");

        tv_name.setText(app.mainUser.getFirstName() + " " + app.mainUser.getLastName());
        tv_full_name.setText(app.mainUser.getFirstName() + " " + app.mainUser.getLastName());
        tv_stud_id.setText(app.mainUser.getStudentCode());
        tv_email.setText(app.mainUser.getEmailID());
        tv_key.setText(app.mainUser.getRegKey());
        tv_mobile.setText(app.mainUser.getRegMobNo());
        tv_sync_time.setText(Utils.changeDateAndTimeAMPMFormet(app.mySharedPref.getLastSyncTime()));
        tv_medium.setText(app.mainUser.getMediumName());
        tv_std.setText(app.mainUser.getStanderdName());
        tv_board.setText(app.mainUser.getBoardName());
        tv_previous_score.setText(Html.fromHtml("Your Previous Year School Result: " + "<font color=\"#0d4568\">" + app.mySharedPref.getlblPrviousTarget() + "%" + "</font>"));
        if (!app.mySharedPref.getProf_image().isEmpty()) {
            File imageFile = new File(app.mySharedPref.getProf_image());
            if (imageFile.isFile()) {
                img_profile.setImageBitmap(BitmapFactory.decodeFile(app.mySharedPref.getProf_image()));

            } else {
                img_profile.setImageResource(R.drawable.profile);
            }

        }

        WebViewSetting(progress, String.valueOf(App.mcqTotalAccuracy), App.mySharedPref.getlblTarget());
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void WebViewSetting(WebView webview, String mcqTotalAccuracy, String Goal) {
        Utils.webSettings(webview);
//        WebSettings settings = webview.getSettings();
//        //settings.setMinimumFontSize(30);
//        settings.setAllowFileAccess(true);
//        settings.setAllowContentAccess(true);
//        settings.setAllowFileAccessFromFileURLs(true);
//        settings.setLoadWithOverviewMode(true);
//        settings.setUseWideViewPort(true);
//        settings.setJavaScriptEnabled(true);
//        settings.setDomStorageEnabled(true);
        webview.setWebViewClient(new myWebClient());
        //webview.loadUrl("file:///android_asset/progressbar/progress.html");
        webview.loadDataWithBaseURL("", ProgressBarHtmL(mcqTotalAccuracy, Goal), "text/html", "UTF-8", "");

    }

    public void DayCont() {
        Calendar start_calendar = Calendar.getInstance();
        Calendar end_calendar = Calendar.getInstance();

        long start_millis = start_calendar.getTimeInMillis(); //get the start time in milliseconds
        end_calendar.set(2019, 3, 30);
        long end_millis = end_calendar.getTimeInMillis();
      //  Log.i("TAG ", "MIll :-> " + "Start :-> " + start_millis + " End :" + end_millis + " " + (end_millis - start_millis));// 10 = November, month start at 0 = January
        long total_millis = (end_millis - start_millis); //total time in milliseconds

        //1000 = 1 second interval
        CountDownTimer cdt = new CountDownTimer(total_millis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long days = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                millisUntilFinished -= TimeUnit.DAYS.toMillis(days);

                long hours = TimeUnit.MILLISECONDS.toHours(millisUntilFinished);
                millisUntilFinished -= TimeUnit.HOURS.toMillis(hours);

                long minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
                millisUntilFinished -= TimeUnit.MINUTES.toMillis(minutes);

                long seconds = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished);
                tv_days.setText(days + "");
            //    Log.i("TAG", "Counter:-> " + days + ":" + hours + ":" + minutes + ":" + seconds); //You can compute the millisUntilFinished on hours/minutes/seconds
            }

            @Override
            public void onFinish() {
                Log.i("TAG", "Finish!");
            }
        };
        cdt.start();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                JSONObject jObj;
                switch (request) {
                    case sendMcqData:
                        jStudHdrArray = null;


                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {
                            if (jObj.has("MCQLastSync")) {
                                String lastMCQSyncTime = jObj.getString("MCQLastSync");
                                app.mySharedPref.setLastMCQlastSync(lastMCQSyncTime);
                              //  app.mySharedPref.setLastSyncTime(lastMCQSyncTime);
                            }
                            try {
                                if (jObj.getJSONArray("data") != null) {
                                    JSONArray jData = jObj.getJSONArray("data");
                               }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }


                        }

                        break;


                    case sendGanreatPaper:


                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {

                            if (jObj.has("GeneratePaperLastSync")) {
                                String lastGeneratePaperSyncTime = jObj.getString("GeneratePaperLastSync");
                                app.mySharedPref.setLastGeneratePaperlastSync(lastGeneratePaperSyncTime);
                             //   app.mySharedPref.setLastSyncTime(lastGeneratePaperSyncTime);

                            }
                            try {
                                if (jObj.getJSONArray("data") != null) {
                                    JSONArray jData = jObj.getJSONArray("data");
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }

                        break;

                    case sendBoardPaperDownload:

                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("status")) {


                            if (jObj.has("BoardPaperLastSync")) {
                                String setLastBoardPaperDownloadSyncTime = jObj.getString("BoardPaperLastSync");
                                app.mySharedPref.setLastBoardPaperDownloadSyncTime(setLastBoardPaperDownloadSyncTime);
                                app.mySharedPref.setLastSyncTime(setLastBoardPaperDownloadSyncTime);
                            }
                            try {
                                if (jObj.getJSONArray("data") != null) {
                                    JSONArray jData = jObj.getJSONArray("data");
                                 }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                        tv_sync_time.setText(Utils.changeDateAndTimeAMPMFormet(app.mySharedPref.getLastSyncTime()));
                        Utils.hideProgressDialog();
                        break;


                }
            } catch (Exception e) {
                Utils.hideProgressDialog();
                e.printStackTrace();
            }
        }
    }




    private class myWebClient extends WebViewClient {
        ProgressDialog progressDialog;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                // Utils.hideProgressDialog();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public String ProgressBarHtmL(String mcqTotalAccuracy, String Goal) {
        return "<!DOCTYPE html>" +
                "<head>  " +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" +
                "<meta charset=\"utf-8\" />" +
                "  <link rel='stylesheet' type='text/css' href='file:///android_asset/bootstrap.min.css'/>" +
/*
                "  <link rel=\"stylesheet\" type=\"text/css\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\" />" +
               */ "  <script type=\"text/javascript\" src=\"file:///android_asset/jquery.min.js\"></script>" +
                "  <script type=\"text/javascript\" src=\"file:///android_asset/bootstrap.min.js\"></script>" +

                "<Script>" +
                "  $(document).ready(function() {" +
                "    $(function () {" +
                "       $('[data-toggle=\"tooltip\"]').tooltip({trigger: 'manual'}).tooltip('show');\n" +
                "  });" +
                "  $(\".progress-bar\").each(function(){" +
                "    each_bar_width = $(this).attr('aria-valuenow');" +
                "    $(this).width(each_bar_width + '%');" +
                "  });" +
                "   });" +
                "  </Script>" +
                "<style>" +
                ".barWrapper{height: 70px;margin-top: 50px;}" +
                "" +
                ".tooltip{ " +
                "  position:relative;" +
                "  float:right;" +
                "} " +
                ".tooltip > .tooltip-inner {" +
                "  padding: 3px 6px;" +
                "  color: #fff;" +
                "  font-weight: bold;" +
                "  font-size: 11px;" +
                "  width: auto;" +
                "  height: auto;" +
                "  border-radius: 4px;" +
                "  line-height: 14px;" +
                "  display: table-cell;" +
                "  vertical-align: middle;" +
                "}" +
                ".tooltip.top{top:-34px !important; opacity: 1; margin-top: 0; width:auto;}" +
                "" +
                ".tooltip.top > .tooltip-inner {  background-color: #f49738; }" +
                "" +
                ".popOver + .tooltip.top > .tooltip-arrow {  border-left: 6px solid transparent; border-right: 6px solid transparent; border-top: 6px solid #f49738; left:50% !important}" +
                "" +
                "" +
                "" +
                ".tooltip.bottom > .tooltip-inner {  background-color: #206e9e; }" +
                ".tooltip.bottom{top:12px !important; opacity: 1; margin-top: 0;}" +
                ".popOver + .tooltip.bottom > .tooltip-arrow {  border-left: 6px solid transparent; border-right: 6px solid transparent; border-bottom: 6px solid #206e9e;  left:50% !important}" +
                "" +
                "section{" +
                "  margin:100px auto; " +
                "  height:1000px;" +
                "}" +
                ".progress{" +
                "  border-radius:0;" +
                "  overflow:visible;" +
                "  height: 10px;" +
                "  margin-bottom: 0;" +
                "}" +
                ".progress-bar{ " +
                "  -webkit-transition: width 1.5s ease-in-out;" +
                "  transition: width 1.5s ease-in-out;" +
                "}" +
                "" +
                ".me-at.progress{ position: relative; z-index: 11; background: rgba(0,0,0,0);}" +
                ".me-at.progress .progress-bar{   background:#f49738; opacity: 1; z-index: 11}" +
                "" +
                ".mygoal.progress{position: relative;top: -10px; z-index: 1}" +
                "" +
                ".mygoal.progress .progress-bar{   background:#206e9e; opacity: .9; z-index: 11}" +
                "" +
                "</style>" +
                "" +
                "</head>" +
                "<body>" +
                "     " +
                "  <!--<h2 class=\"text-center\">Scroll down the page a bit</h2><br><br> -->" +


                "<div class=\"container\">" +
                "  " +
                "    " +
                "    " +
                "       " +
                "      <div class=\"barWrapper\"> " +
                "        <div class=\"progress me-at\">" +
                "          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"" + mcqTotalAccuracy + "\" aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"ME (" + mcqTotalAccuracy + "%)\"> </span>     " +
                "          </div> " +
                "        </div>" +
                "        <div class=\"progress mygoal\">" +
                "          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"" + App.mySharedPref.getlblTarget() + "\" aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"MY GOAL (" + App.mySharedPref.getlblTarget() + "%)\"> </span>     " +
                "          </div>" +
                "        </div>" +
                "      </div>" +
                " " +
                "</div>   " +



               /* "<div class=\"container\">" +
                "      <div class=\"barWrapper\"> " +
                "        <div class=\"progress me-at\">" +
                "          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"50\" aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"ME (50%)\"> </span>     " +
                "          </div> " +
                "        </div>" +
                "        <div class=\"progress mygoal\">" +
                "          <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\" >   " +
                "                <span  class=\"popOver\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"MY GOAL (85%)\"> </span>     " +
                "          </div>" +
                "        </div>" +
                "      </div>" +
                "</div>   " +*/

                "</body>" +

                "</html>";
    }





}
