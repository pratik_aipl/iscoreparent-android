package com.iscore.parent.activity.reports.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iscore.parent.model.ReportMCQ;
import com.iscore.parent.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 2/25/2017.
 */

public class SubjectAccuracyLevelAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<ReportMCQ> mcqReportsArray;
    public LayoutInflater inflater;

    public SubjectAccuracyLevelAdapter(Fragment ft, ArrayList<ReportMCQ> mcqReportsArray) {
        mContext = ft.getActivity();
        this.mcqReportsArray = mcqReportsArray;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mcqReportsArray.size();

    }

    @Override
    public ReportMCQ getItem(int position) {
        // TODO Auto-generated method stub
        return mcqReportsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    private static class ViewHolder {

        public TextView tv_subj;
        public TextView tv_accuracy_level, tv_Total_attempt, tv_accuracy_one, tv_accuracy_two, tv_accuracy_three,
                tv_attempt_one, tv_attempt_two, tv_attempt_three;

        public ViewHolder(View v) {
            tv_subj = (TextView) v.findViewById(R.id.tv_sub_name);
            tv_accuracy_level = (TextView) v.findViewById(R.id.tv_accuracy_level);
            tv_Total_attempt = (TextView) v.findViewById(R.id.tv_Total_attempt);
            tv_accuracy_one = (TextView) v.findViewById(R.id.tv_accuracy_one);
            tv_accuracy_two = (TextView) v.findViewById(R.id.tv_accuracy_two);
            tv_accuracy_three = (TextView) v.findViewById(R.id.tv_accuracy_three);
            tv_attempt_one = (TextView) v.findViewById(R.id.tv_attempt_one);
            tv_attempt_two = (TextView) v.findViewById(R.id.tv_attempt_two);
            tv_attempt_three = (TextView) v.findViewById(R.id.tv_attempt_three);

        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_subject_accuracylevel_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_subj.setText(mcqReportsArray.get(position).getSubject()+" (Attempt/Accuracy)");

        holder.tv_accuracy_one.setText(mcqReportsArray.get(position).getAccurecy_level1() + "%");
        holder.tv_accuracy_two.setText(mcqReportsArray.get(position).getAccurecy_level2() + "%");
        holder.tv_accuracy_three.setText(mcqReportsArray.get(position).getAccurecy_level3() + "%");

        holder.tv_accuracy_level.setText("(" + mcqReportsArray.get(position).getAccurecy_level() + ")%");
        holder.tv_Total_attempt.setText("(" + mcqReportsArray.get(position).getSubject_total_test() + ")");
        holder.tv_attempt_one.setText(mcqReportsArray.get(position).getTotallevel1());
        holder.tv_attempt_two.setText(mcqReportsArray.get(position).getTotallevel2());
        holder.tv_attempt_three.setText(mcqReportsArray.get(position).getTotallevel3());

        return convertView;
    }

}
