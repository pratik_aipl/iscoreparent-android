package com.iscore.parent.activity.reports.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.iscore.parent.model.CctReport;
import com.iscore.parent.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 2/25/2017.
 */

public class SubjectCCTAdapter extends BaseAdapter {
    private Context mContext;
    public ArrayList<CctReport> cctArray;
    public LayoutInflater inflater;

    public SubjectCCTAdapter(Fragment ft, ArrayList<CctReport> cctArray) {
        mContext = ft.getActivity();
        this.cctArray = cctArray;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return cctArray.size();
    }

    @Override
    public CctReport getItem(int position) {
        // TODO Auto-generated method stub
        return cctArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    private static class ViewHolder {

        public TextView tv_subj;
        public TextView tv_accuracy, tv_total;


        public ViewHolder(View v) {
            tv_subj = (TextView) v.findViewById(R.id.tv_sub_name);

            tv_accuracy = (TextView) v.findViewById(R.id.tv_accuracy);
            tv_total = (TextView) v.findViewById(R.id.tv_total);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_subject_cct_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_subj.setText(cctArray.get(position).getSubjectName()+" (Attempt/Accuracy)");
        holder.tv_total.setText("(" + cctArray.get(position).getTotal() + ")");
        holder.tv_accuracy.setText(cctArray.get(position).getAcuuracy() + "%");

        return convertView;
    }

}
