package com.iscore.parent.activity.reports.Adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.iscore.parent.model.ReportTest;
import com.iscore.parent.R;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 11/2/2017.
 */

public class SubjectTestlAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<ReportTest> testReportsArray;
    public LayoutInflater inflater;

    public SubjectTestlAdapter(Fragment ft, ArrayList<ReportTest> testReportsArray) {
        mContext = ft.getActivity();
        this.testReportsArray = testReportsArray;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return testReportsArray.size();
    }

    @Override
    public ReportTest getItem(int position) {
        // TODO Auto-generated method stub
        return testReportsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    private static class ViewHolder {

        public TextView tv_subj;
        public TextView tv_total,tv_ready,tv_prelim,tv_set;
        public ProgressBar prgBar;

        public ViewHolder(View v) {
            tv_subj = (TextView) v.findViewById(R.id.tv_sub_name);

            tv_total = (TextView) v.findViewById(R.id.tv_total);
            tv_ready = (TextView) v.findViewById(R.id.tv_ready);
            tv_prelim = (TextView) v.findViewById(R.id.tv_prelim);
            tv_set = (TextView) v.findViewById(R.id.tv_set);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_subject_test_row, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_subj.setText(testReportsArray.get(position).getSubject_name()+" (Total Test)");
        holder.tv_total.setText(getItem(position).getTotal_test());
        holder.tv_prelim.setText(getItem(position).getPrelim_test());
        holder.tv_ready.setText(getItem(position).getReady_test());
        holder.tv_set.setText(getItem(position).getSet_test());

        return convertView;
    }

}
