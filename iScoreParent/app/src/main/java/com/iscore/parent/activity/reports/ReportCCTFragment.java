package com.iscore.parent.activity.reports;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;


import com.iscore.parent.activity.reports.Adapter.SubjectCCTAdapter;
import com.iscore.parent.App;
import com.iscore.parent.model.CctReport;
import com.iscore.parent.R;
import com.iscore.parent.utils.CursorParserUniversal;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Karan - Empiere on 11/3/2017.
 */

public class ReportCCTFragment extends Fragment {
    public App app;
    public GridView gridView;
    public CursorParserUniversal cParse;
    public SubjectCCTAdapter adapter;
    public View v;
    public int attempted;
    public TextView tv_totalTest, tv_cct_accuracy;
    private LinkedHashMap<String, String> array, tempArray;
    public CctReport cctObj;
    public ArrayList<CctReport> cctArray = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_cct, container, false);
        app = App.getInstance();
        tv_totalTest = (TextView) v.findViewById(R.id.tv_total_test);
        tv_cct_accuracy = (TextView) v.findViewById(R.id.tv_cct_accuracy);
        tv_cct_accuracy.setText(app.cctReport.cct_overall_accuracy + "% Accuracy");
        tv_totalTest.setText(app.cctReport.cct_attemps + "");

        adapter = new SubjectCCTAdapter(ReportCCTFragment.this, app.cctReport.cctReportsArray);
        gridView = (GridView) v.findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        return v;
    }
}
