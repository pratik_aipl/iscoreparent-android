package com.iscore.parent.activity.reports;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;


import com.iscore.parent.activity.reports.Adapter.SubjectComparisionAdapter;
import com.iscore.parent.App;
import com.iscore.parent.model.ComparisionReport;
import com.iscore.parent.R;
import com.iscore.parent.utils.CursorParserUniversal;

import java.util.ArrayList;

/**
 * Created by admin on 2/24/2017.
 */
public class ReportComparisionFragment extends Fragment {
    public GridView gridView;

    public CursorParserUniversal cParse;
    public SubjectComparisionAdapter adapter;

    public View v;
    public ArrayList<ComparisionReport> ComparisionArray = new ArrayList<>();
    public ComparisionReport ComparisionObj;
    public App app;

    public TextView tv_total_mcq, tv_accuracy_mcq, tv_total_cct, tv_accuracy_cct, tv_genratepaper_total;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_comarision, container, false);

        app = App.getInstance();
        tv_total_mcq = (TextView) v.findViewById(R.id.tv_total_mcq);
        tv_accuracy_mcq = (TextView) v.findViewById(R.id.tv_accuracy_mcq);
        tv_total_cct = (TextView) v.findViewById(R.id.tv_total_cct);
        tv_accuracy_cct = (TextView) v.findViewById(R.id.tv_accuracy_cct);
        tv_genratepaper_total = (TextView) v.findViewById(R.id.tv_genratepaper_total);


        tv_total_mcq.setText(app.mcq.getMcq_attemps()+"");
        tv_accuracy_mcq.setText(app.mcq.getAccuracy()+"%");
        tv_total_cct.setText(app.cctReport.cct_attemps+"");
        tv_accuracy_cct.setText(app.cctReport.cct_overall_accuracy+"%");
        tv_genratepaper_total.setText(app.test.No_of_paper_generate+"");
        adapter = new SubjectComparisionAdapter(this, app.compar.compArray);
        gridView = (GridView) v.findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);



        return v;
    }


}
