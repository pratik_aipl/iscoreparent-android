package com.iscore.parent.activity.reports;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.iscore.parent.activity.reports.Adapter.SubjectAccuracyLevelAdapter;
import com.iscore.parent.App;
import com.iscore.parent.model.McqReport;
import com.iscore.parent.R;
import com.iscore.parent.utils.CursorParserUniversal;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by admin on 2/24/2017.
 */
public class ReportMCQFragment extends Fragment {
    public GridView gridView;
    public CursorParserUniversal cParse;
     public SubjectAccuracyLevelAdapter adapter;
    public View v;
    public int attempted;
    public TextView tv_totalTest, tv_defLevel1, tv_defLevel2, tv_defLevel3, tv_mcq_accuracy;
    public String testHeaderIDS = "";
    private LinkedHashMap<String, String> array, tempArray;
    public App app;
    public McqReport mcqReport;
    public ArrayList<McqReport> mcqReportArray = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_mcq, container, false);

        app = App.getInstance();

        tv_totalTest = (TextView) v.findViewById(R.id.tv_total_test);

        tv_defLevel1 = (TextView)v.findViewById(R.id.tv_defLevel1);
        tv_defLevel2 = (TextView)v.findViewById(R.id.tv_defLevel2);
        tv_defLevel3 = (TextView)v.findViewById(R.id.tv_defLevel3);
        tv_mcq_accuracy= (TextView)v.findViewById(R.id.tv_mcq_accuracy);
        tv_totalTest.setText(app.mcq.getMcq_attemps());
        tv_defLevel1.setText(app.mcq.getLevel_1());
        tv_defLevel2.setText(app.mcq.getLevel_2());
        tv_defLevel3.setText(app.mcq.getLevel_3());
        tv_mcq_accuracy.setText(app.mcq.getAccuracy() + "% Accuracy");

        Log.i("TAG", "MCQ FRagment:->" + app.mcq.mcqArray.size());
        adapter = new SubjectAccuracyLevelAdapter(ReportMCQFragment.this, app.mcq.mcqArray);
        gridView = (GridView) v.findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);
        return v;
    }




}
