package com.iscore.parent.activity.reports;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iscore.parent.App;
import com.iscore.parent.R;
import com.iscore.parent.utils.CursorParserUniversal;
import com.iscore.parent.utils.Utils;

/**
 * Created by admin on 2/24/2017.
 */
public class ReportOverAllFragment extends Fragment {

    public TextView tv_mcq_count, tv_boardpaper_count, tv_cct_count, tv_paractiespaper_count,
            tv_mcq_accuracy, tv_cct_accuracy;
    public View v;
    public CursorParserUniversal cParse;
    public App app;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_overall, container, false);
        cParse = new CursorParserUniversal();
        app=App.getInstance();

        Utils.hideProgressDialog();
        tv_mcq_accuracy = (TextView) v.findViewById(R.id.tv_mcq_accuracy);
        tv_cct_accuracy = (TextView) v.findViewById(R.id.tv_cct_accuracy);
        tv_paractiespaper_count = (TextView) v.findViewById(R.id.tv_paractiespaper_count);
        tv_cct_count = (TextView) v.findViewById(R.id.tv_cct_count);
        tv_mcq_count = (TextView) v.findViewById(R.id.tv_mcq_count);
        tv_boardpaper_count = (TextView) v.findViewById(R.id.tv_boardpaper_count);

        tv_mcq_count.setText(app.overall.getMcq_attemps());
        tv_boardpaper_count.setText(app.overall.getBoard_papaer_download());
        tv_mcq_accuracy.setText(app.mcq.getAccuracy()+ "% Accuracy");
        tv_cct_accuracy.setText(app.overall.getCct_overall_accuracy()+ "% Accuracy");
        tv_paractiespaper_count.setText(String.valueOf(app.overall.getPractice_paper_download()+ ""));
        tv_cct_count.setText(app.overall.getCct_test_total());
        return v;
    }

}
