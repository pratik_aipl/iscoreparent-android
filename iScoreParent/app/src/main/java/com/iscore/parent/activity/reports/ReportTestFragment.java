package com.iscore.parent.activity.reports;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.iscore.parent.activity.reports.Adapter.SubjectTestlAdapter;
import com.iscore.parent.App;
import com.iscore.parent.model.TestReport;
import com.iscore.parent.R;
import com.iscore.parent.utils.CursorParserUniversal;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by admin on 2/24/2017.
 */
public class ReportTestFragment extends Fragment {
    public TextView tv_total, tv_PrelimsPaperDownload, tv_SetPaperDownload, tv_ReadyPaperDownload;
    public int readyPaper;
    public int setPaper;
    public int prelimPaper;
    public int total_ganrate;
    public GridView gridView;
    public SubjectTestlAdapter adapter;
    public CursorParserUniversal cParse;
    public String testHeaderIDS = "";
    private LinkedHashMap<String, String> array, tempArray;
    public String[] tempSujectID = new String[]{};
    public App app;
    public TestReport testReport;
    public ArrayList<TestReport> testReportArray = new ArrayList<>();


    public static ReportTestFragment newInstance(String param1, String param2) {
        ReportTestFragment fragment = new ReportTestFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }

    }

    public View v;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        v = inflater.inflate(R.layout.fragment_test, container, false);

        cParse = new CursorParserUniversal();
        app = App.getInstance();

        tv_total = (TextView) v.findViewById(R.id.tv_total);
        tv_PrelimsPaperDownload = (TextView) v.findViewById(R.id.tv_PrelimsPaperDownload);
        tv_SetPaperDownload = (TextView) v.findViewById(R.id.tv_SetPaperDownload);
        tv_ReadyPaperDownload = (TextView) v.findViewById(R.id.tv_ReadyPaperDownload);

        tv_total.setText(app.test.getNo_of_paper_generate());
        tv_PrelimsPaperDownload.setText(app.test.getPrelim_Paper_test());
        tv_SetPaperDownload.setText(app.test.getSet_paper_test());
        tv_ReadyPaperDownload.setText(app.test.getReady_paper_test());
        adapter = new SubjectTestlAdapter(this, app.test.testArray);
        gridView = (GridView) v.findViewById(R.id.grid_view);
        gridView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        return v;
    }
}
