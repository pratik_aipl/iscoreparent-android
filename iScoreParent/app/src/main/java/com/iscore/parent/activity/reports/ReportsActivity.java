package com.iscore.parent.activity.reports;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.App;
import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.model.CctReport;
import com.iscore.parent.model.Comparision;
import com.iscore.parent.model.Overall;
import com.iscore.parent.model.ReportMCQ;
import com.iscore.parent.model.ReportTest;
import com.iscore.parent.R;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.CursorParserUniversal;
import com.iscore.parent.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class ReportsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, AsynchTaskListner {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public int attempted;
    public static TextView tv_accuracy_level, tv_target;
    public String testHeaderIDS = "";
    public CursorParserUniversal cParse;
    public App app;
    public ReportsActivity instance;
    public ImageView img_back;
    public TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_reports);
        Utils.logUser();

        instance = this;
        app = App.getInstance();
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("Reports");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_accuracy_level = (TextView) findViewById(R.id.tv_accuracy_level);
        tv_target = (TextView) findViewById(R.id.tv_target);
        tv_target.setText(app.mySharedPref.getlblTarget());
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null)
            setupViewPager(viewPager);
        if (app != null && app.mcq != null && !TextUtils.isEmpty(app.mcq.getAccuracy()))
            tv_accuracy_level.setText(app.mcq.getAccuracy());
        tv_target.setText(app.mySharedPref.getlblTarget());

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        setupTabFont();


    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setupTabFont() {

        TextView OverAll = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        OverAll.setText("Overall");
        tabLayout.getTabAt(0).setCustomView(OverAll);

        TextView MCQ = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        MCQ.setText("MCQ");
        tabLayout.getTabAt(1).setCustomView(MCQ);

        TextView CCT = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        CCT.setText("CCT");
        tabLayout.getTabAt(2).setCustomView(CCT);
        TextView Test = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        Test.setText("Practise");
        tabLayout.getTabAt(3).setCustomView(Test);
        TextView Comparision = (TextView) LayoutInflater.from(instance).inflate(R.layout.custom_report_tab, null);
        Comparision.setText("Comparison");
        tabLayout.getTabAt(4).setCustomView(Comparision);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(instance.getSupportFragmentManager());
        adapter.addFragment(new ReportOverAllFragment(), "Overall");
        adapter.addFragment(new ReportMCQFragment(), "MCQ");
        adapter.addFragment(new ReportCCTFragment(), "CCT");
        adapter.addFragment(new ReportTestFragment(), "Practise");
        adapter.addFragment(new ReportComparisionFragment(), "Comparison");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                switch (request) {

                    case getStudentParentData:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("status") == true) {

                            JSONObject jData = jObj.getJSONObject("data");
                            JSONObject jOverall = jData.getJSONObject("overall");
                            app.overall = new Overall();
                            app.overall.setMcq_attemps(jOverall.getString("mcq_attemps"));
                            app.overall.setReady_papaer_download(jOverall.getString("ready_paper_download"));
                            app.overall.setSet_papaer_download(jOverall.getString("set_paper_download"));
                            app.overall.setPrelim_papaer_download(jOverall.getString("prelim_paper_download"));
                            app.overall.setBoard_papaer_download(jOverall.getString("board_paper_download"));
                            app.overall.setPractice_paper_download(jOverall.getInt("practice_paper_download"));
                            app.overall.setCct_test_total(jOverall.getString("cct_test_total"));
                            app.overall.setCct_overall_accuracy(String.valueOf(jOverall.getInt("cct_overall_accuracy")));


                            //------------ MCQ ---------------

                            JSONObject jMcq = jData.getJSONObject("mcq");
                            app.mcq = new ReportMCQ();
                            app.mcq.setMcq_attemps(jMcq.getString("mcq_attemps"));
                            app.mcq.setLevel_1(jMcq.getString("level_1"));
                            app.mcq.setLevel_2(jMcq.getString("level_2"));
                            app.mcq.setLevel_3(jMcq.getString("level_3"));
                            app.mcq.setAccuracy(String.valueOf(jMcq.getInt("accuracy_level")));


                            JSONArray jSubject_accuracy = jMcq.getJSONArray("subject_accuracy");
                            if (jSubject_accuracy != null && jSubject_accuracy.length() > 0) {

                                for (int i = 0; i < jSubject_accuracy.length(); i++) {

                                    JSONObject jSubjectObj = jSubject_accuracy.getJSONObject(i);
                                    ReportMCQ mcq = new ReportMCQ();
                                    mcq.setSubject(jSubjectObj.getString("subject"));
                                    mcq.setSubject_total_test(jSubjectObj.getString("subject_total_test"));
                                    mcq.setTotallevel1(jSubjectObj.getString("Totallevel1"));
                                    mcq.setTotallevel2(jSubjectObj.getString("Totallevel2"));
                                    mcq.setTotallevel3(jSubjectObj.getString("Totallevel3"));
                                    mcq.setAccurecy_level(jSubjectObj.getInt("accurecy_level1"));
                                    mcq.setAccurecy_level2(jSubjectObj.getInt("accurecy_level2"));
                                    mcq.setAccurecy_level3(jSubjectObj.getInt("accurecy_level3"));
                                    mcq.setAccurecy_level(jSubjectObj.getInt("accurecy_level"));

                                    app.mcq.mcqArray.add(mcq);
                                    Log.i("TAG", "MCQ ARRAY:->" + app.mcq.mcqArray.get(i).getSubject());
                                }

                            }
                            //------------ CCT ---------------

                            JSONObject jCct = jData.getJSONObject("cct");
                            app.cctReport = new CctReport();
                            app.cctReport.setCct_attemps(jCct.getString("cct_attemps"));
                            app.cctReport.setCct_overall_accuracy(String.valueOf(jCct.getInt("cct_overall_accuracy")));

                            JSONArray jCct_accuracy = jCct.getJSONArray("subject_accuracy");
                            if (jCct_accuracy != null && jCct_accuracy.length() > 0) {
                                for (int i = 0; i < jCct_accuracy.length(); i++) {
                                    JSONObject obj = jCct_accuracy.getJSONObject(i);
                                    CctReport cctReportObj = new CctReport();
                                    cctReportObj.setSubjectName(obj.getString("SubjectName"));
                                    cctReportObj.setTotal(obj.getString("Total"));
                                    cctReportObj.setAcuuracy(obj.getString("Acuuracy"));
                                    app.cctReport.cctReportsArray.add(cctReportObj);
                                }

                            }
                            //SubjectAccuracyLevelAdapter subAccu = new SubjectAccuracyLevelAdapter(this,mcqArray);
                            //-------------- TEST -----------------

                            JSONObject jTest = jData.getJSONObject("test");
                            app.test = new ReportTest();
                            app.test.setNo_of_paper_generate(jTest.getString("no_of_paper_generate"));
                            app.test.setWith_self_assesment(jTest.getString("with_self_assesment"));
                            app.test.setWithout_self_assesment(jTest.getString("without_self_assesment"));
                            app.test.setPrelim_Paper_test(jTest.getString("prelim_test"));
                            app.test.setSet_paper_test(jTest.getString("set_paper_test"));
                            app.test.setReady_paper_test(jTest.getString("ready_paper_test"));

                            JSONArray jSubject_Test = jTest.getJSONArray("subject_array");
                            if (jSubject_Test != null && jSubject_Test.length() > 0) {

                                for (int i = 0; i < jSubject_Test.length(); i++) {

                                    JSONObject jSubjectObj = jSubject_Test.getJSONObject(i);
                                    ReportTest test = new ReportTest();
                                    test.setSubject_name(jSubjectObj.getString("subject_name"));
                                    test.setPrelim_test(jSubjectObj.getString("prelim_test"));
                                    test.setReady_test(jSubjectObj.getString("ready_test"));
                                    test.setSet_test(jSubjectObj.getString("set_test"));
                                    test.setTotal_test(jSubjectObj.getString("total_test"));

                                    app.test.testArray.add(test);
                                }
                            }

                            //--------------- Comparition -------------------


                            JSONObject jComparision = jData.getJSONObject("comparision");
                            app.compar = new Comparision();
                            JSONArray jSubject_comparision = jComparision.getJSONArray("subject_comparision");
                            if (jSubject_comparision != null && jSubject_comparision.length() > 0) {
                                for (int i = 0; i < jSubject_comparision.length(); i++) {

                                    JSONObject jSubjectOb = jSubject_comparision.getJSONObject(i);
                                    Comparision cmp = new Comparision();
                                    cmp.setSubject(jSubjectOb.getString("subject"));
                                    cmp.setMcq_total_test(jSubjectOb.getString("mcq_total_test"));
                                    cmp.setPaper_total_test(jSubjectOb.getString("paper_total_test"));
                                    cmp.setScalar(jSubjectOb.getString("scalar"));
                                    cmp.setMcq_accuracy(jSubjectOb.getInt("mcq_accuracy"));
                                    cmp.setCct_total_test(jSubjectOb.getInt("cct_total_test"));
                                    cmp.setCct_accuracy(jSubjectOb.getInt("cct_accuracy"));

                                    app.compar.compArray.add(cmp);

                                }

                            }
                            setupViewPager(viewPager);

                            // switchFragment(new ReportNewFragment());


                        } else {
                            Utils.showToast("Please try again later", ReportsActivity.this);

                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
        }
    }


}