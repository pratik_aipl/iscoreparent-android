package com.iscore.parent.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.iscore.parent.R;
import com.iscore.parent.utils.Utils;

public class weeklyHtmlActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_weekly_html);
        Utils.logUser();
        WebView webView = (WebView) findViewById(R.id.webview);
        webView.loadUrl("file:///android_asset/email2.html");
    }
}
