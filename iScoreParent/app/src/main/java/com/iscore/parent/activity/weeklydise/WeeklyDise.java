package com.iscore.parent.activity.weeklydise;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 3/24/2018.
 */

public class WeeklyDise implements Serializable {


    public String SubjectID = "";

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(String accuracy) {
        Accuracy = accuracy;
    }

    public String getTotalPaper() {
        return TotalPaper;
    }

    public void setTotalPaper(String totalPaper) {
        TotalPaper = totalPaper;
    }

    public String SubjectName = "";
    public String Accuracy = "";
    public String TotalPaper = "";

}
