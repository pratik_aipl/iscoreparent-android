package com.iscore.parent.activity.weeklydise;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iscore.parent.model.WeeklyReport;
import com.iscore.parent.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 3/23/2018.
 */

public class WeeklyDiseAdapter extends RecyclerView.Adapter<WeeklyDiseAdapter.MyViewHolder> {

    private ArrayList<WeeklyReport> startDate;
    public Context context;
    public int SubjectID;
    public String SubjectName;

    public WeeklyDiseAdapter(ArrayList<WeeklyReport> startDate, Context context) {
        this.startDate = startDate;
         this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_subject_name, tv_month, tv_expdate, tv_take_test, tv_date;

        public MyViewHolder(View view) {
            super(view);
            tv_subject_name = (TextView) view.findViewById(R.id.tv_subject_name);
            tv_month = (TextView) view.findViewById(R.id.tv_month);
            tv_expdate = (TextView) view.findViewById(R.id.tv_expdate);
            tv_date = (TextView) view.findViewById(R.id.tv_date);
            tv_take_test = (TextView) view.findViewById(R.id.tv_take_test);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_weekly_raw, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SimpleDateFormat fmt = new SimpleDateFormat("dd MMM");


        holder.tv_subject_name.setText(startDate.get(position).getName());
        holder.tv_expdate.setText(startDate.get(position).getStart_date() + "-" +(startDate.get(position).getEnd_date()));
        final SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MM-dd");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(position==0){
                    context.startActivity(new Intent(context, WeeklyReportPreviewActivity.class)
                            //  .putExtra("suObj", mcqdata)
                            .putExtra("end_date",startDate.get(position).getEnd_date())
                            .putExtra("start_date",startDate.get(position).getStart_date())
                            .putExtra("old_end_date","0000-00-00")
                            .putExtra("old_start_date","0000-00-00")
                            );
                }else{
                context.startActivity(new Intent(context, WeeklyReportPreviewActivity.class)
                        //  .putExtra("suObj", mcqdata)
                        .putExtra("end_date",startDate.get(position).getEnd_date())
                        .putExtra("start_date",startDate.get(position).getStart_date())
                        .putExtra("old_end_date",startDate.get(position-1).getEnd_date())
                        .putExtra("old_start_date",startDate.get(position-1).getStart_date()));}
            }
        });

    }


    @Override
    public int getItemCount() {
        return startDate.size();
    }

}



