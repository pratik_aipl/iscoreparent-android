package com.iscore.parent.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iscore.parent.R;
import com.iscore.parent.model.Subject;

import java.util.List;

public class CustomSubjectSpinnerAdapter extends ArrayAdapter<Subject> {

    LayoutInflater flater;
    Context context;

    public CustomSubjectSpinnerAdapter(Activity context, int resouceId, int textviewId, List<Subject> list) {

        super(context, resouceId, textviewId, list);
        this.context = context;
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Subject rowItem = getItem(position);
        View rowview = flater.inflate(R.layout.raw_spinner_layout, null, true);
        TextView txtTitle = (TextView) rowview.findViewById(R.id.title);
        txtTitle.setText(rowItem.getSubjectName());
        return rowview;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = flater.inflate(R.layout.spinner_dropdown_item, parent, false);
        }
        Subject rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setSingleLine(true);
        txtTitle.setText(rowItem.getSubjectName());
        return convertView;
    }
}