package com.iscore.parent.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iscore.parent.model.NavDrawerItem;
import com.iscore.parent.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by RoBot on 6/5/2017.
 */

public class DashboardDrawerAdapter extends RecyclerView.Adapter<DashboardDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public DashboardDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.titleIcon.setImageResource(current.getIcons());

      /*  if (position == 3||position==6) {
            int width = holder.title.getWidth();
            holder.title.setLayoutParams(new RelativeLayout.LayoutParams(width, 0));
            width = holder.titleIcon.getWidth();
            holder.titleIcon.setLayoutParams(new RelativeLayout.LayoutParams(width, 0));
        }*/
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView titleIcon;
        View itemView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            titleIcon = (ImageView) itemView.findViewById(R.id.titleIcon);
            this.itemView = itemView;

        }
    }
}
