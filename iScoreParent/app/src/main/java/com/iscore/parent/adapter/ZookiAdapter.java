package com.iscore.parent.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.iscore.parent.activity.ZookiOfflineActivity;
import com.iscore.parent.activity.ZookiWebActivity;
import com.iscore.parent.model.Zooki;
import com.iscore.parent.R;
import com.iscore.parent.utils.Constant;
import com.iscore.parent.utils.Utils;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 10/6/2017.
 */

public class ZookiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Context mContext;
    LayoutInflater inflater;
    Zooki zookiObj;
    private ArrayList<Zooki> spaArray;
    public int type = 0;

    private static final int VIEW_TYPE_DEFULT = 0;
    private static final int VIEW_TYPE_VIEW_MORE = 1;

    public ZookiAdapter(Activity context, ArrayList<Zooki> spaArray) {

        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.spaArray = spaArray;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_DEFULT) {
            return new ViewHolderMenu(inflater.inflate(R.layout.custom_zooki_row, parent, false));
        } else {
            return new ViewHolderViewMore(inflater.inflate(R.layout.custom_zooki_viewmore_row, parent, false));
        }
    }


    public Zooki getItem(int position) {
        return spaArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return spaArray.size();
    }


    public class ViewHolderMenu extends RecyclerView.ViewHolder {
        public TextView tv_videoTitle;
        public ImageView img_video;
        public ProgressBar pBar;

        int pos;

        public ViewHolderMenu(View v) {
            super(v);
            tv_videoTitle = (TextView) v.findViewById(R.id.tv_videoTitle);
            img_video = (ImageView) v.findViewById(R.id.img_video);

        }
    }

    public class ViewHolderViewMore extends RecyclerView.ViewHolder {
        public Button btn_viewmore;


        public ViewHolderViewMore(View v) {
            super(v);
            btn_viewmore = (Button) v.findViewById(R.id.btn_viewmore);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderMenu offerHolder = (ViewHolderMenu) holder;
                bindSpaHolder(offerHolder, position);
                break;
            case 1:
                ViewHolderViewMore addrHolder = (ViewHolderViewMore) holder;
                bindViewMoreHolder(addrHolder, position);
                break;

        }

    }

    @Override
    public int getItemViewType(int position) {

        if (position == (spaArray.size() - 1)) {
            return VIEW_TYPE_VIEW_MORE;
        } else {
            return VIEW_TYPE_DEFULT;
        }


    }


    public void bindViewMoreHolder(final ViewHolderViewMore holder, final int pos) {
        holder.btn_viewmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(mContext)) {
                    mContext.startActivity(new Intent(mContext, ZookiWebActivity.class));
                }else{
                    Utils.showToast(mContext.getResources().getString(R.string.conect_internet), mContext);


                }
            }
        });

    }

    public void bindSpaHolder(final ViewHolderMenu holder, final int pos) {
        zookiObj = getItem(pos);
        holder.tv_videoTitle.setText(zookiObj.getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, ZookiOfflineActivity.class)
                        .putExtra("zooki", spaArray)
                        .putExtra("pos", pos));

            }
        });

        File image = new File(Constant.LOCAL_IMAGE_PATH + "/Zooki/" + zookiObj.getImage());
        // Log.i("Image Path :::: ", image.getPath());
        if (image.exists()) {
            try {
                holder.img_video.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
            } catch (NullPointerException e) {
            }
        }
      /*  AQuery aq = aqNewsIMG.recycle(holder.itemView);
        //Log.i("TAG", "IMAG_PATH::->" + Constant.IMAGE_PATH + specialities.getImage());
        aq.id(holder.img_speciality).progress(holder.pBar).image(MyConstants.SPECIALITY_URL + specialities.getImage(), true, true, 0, R.drawable.login_back, null, 0, 0.0f);
      */


    }

}


