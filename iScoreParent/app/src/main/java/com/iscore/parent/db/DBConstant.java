package com.iscore.parent.db;

/**
 * Created by Sagar Sojitra on 1/19/2017.
 */

public class DBConstant {

    public static final String DB_NAME = "iScoreDb.db";
    public static final String DB_OneSignal = "OneSignal.db";
    public static String notification = "notification";
    public static String subject = "subject";
    public static String zooki = "zooki";
}
