package com.iscore.parent.db;

import java.util.HashMap;

/**
 * Created by Sagar Sojitra on 1/30/2017.
 */

public class DBTables {
    public HashMap<String, String> tables = new HashMap<>();
    public DBConstant dbConstant;
    public DBTables() {
        dbConstant = new DBConstant();
    }

    public void createAll(MyDBManager db) {
//        subject(db);
        notification(db);
        zooki(db);
    }

    public void notification(MyDBManager db) {
        tables.clear();
        tables.put("NID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("OneSignalID", "integer");
        tables.put("notification_type_id", "varchar (225)");
        tables.put("android_notification_id", "varchar (225)");
        tables.put("Title", "varchar (225)");
        tables.put("Message", "varchar (225)");
        tables.put("CreatedOn", "varchar (225)");
        tables.put("ModuleType", "varchar (225)");
        tables.put("isOpen", "varchar (225)");
        tables.put("isTypeOpen", "varchar (225)");
        tables.put("CctSubmit", "varchar (225)");
        tables.put("paper_type", "varchar (225)");
        tables.put("SubjectName", "varchar (225)");
        db.createTable(tables, dbConstant.notification);
    }

    public void subject(MyDBManager db) {
        tables.clear();
        tables.put("NID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("SubjectID", "integer");
        tables.put("SubjectName", "varchar (225)");
        db.createTable(tables, dbConstant.subject);
    }

    public void zooki(MyDBManager db) {
        tables.clear();
        tables.put("ZookiID", "integer PRIMARY KEY AUTOINCREMENT");
        tables.put("Title", "varchar (255)");
        tables.put("Desc", "longtext");
        tables.put("image", "varchar (255)");
        tables.put("CreatedOn", "varchar (255)");
        db.createTable(tables, dbConstant.zooki);
    }


}
