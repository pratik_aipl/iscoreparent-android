package com.iscore.parent.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.iscore.parent.utils.Utils;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class MyDBManager extends SQLiteOpenHelper {

    private static final String TAG = "DATABASE";
    private static MyDBManager sInstance;
    public SQLiteDatabase myDb;
    public Context myContext;
    private static final int DATABASE_VERSION = 3;


    public MyDBManager(Context context) {
        super(context, DBConstant.DB_NAME, null, DATABASE_VERSION);
        this.myContext = context;
    }


    public static synchronized MyDBManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new MyDBManager(context);
        }
        return sInstance;
    }


    public boolean isDbOpen() {
        return myDb.isOpen();
    }

    public MyDBManager open(Context context) throws SQLException {
        boolean dbExist =
                checkDataBase();

        if (!dbExist) {
            Log.i(TAG,
                    "Getting writable DB");
            myDb = this.getWritableDatabase();
        }

        return this;
    }

    private boolean checkDataBase() {


        try {
            if (myDb != null && isDbOpen()) {
                return true;
            }
            myDb = SQLiteDatabase.openOrCreateDatabase(getDatabasePath(DBConstant.DB_NAME), null);
            return true;
        } catch (SQLiteException e) {
            e.printStackTrace();
            return false;
        }


    }

    public void close() {
        if (sInstance != null) {
            sInstance.close();
        }
    }

    public String getDatabasePath(String name) {

        String dbFile = myContext.getPackageCodePath() + File.separator + name;
        //   String dbfile = Constant.LOCAL_IMAGE_PATH + File.separator + name;


        File result = new File(dbFile);

        if (!result.getParentFile().exists()) {
            result.getParentFile().mkdirs();
        }

        if (Log.isLoggable(TAG, Log.WARN)) {
            Log.w(TAG,
                    "getDatabasePath(" + name + ") = " + result.getAbsolutePath());
        }

        return result.getAbsolutePath();
    }

    public void createTable(HashMap<String, String> columns, String tableName) {

        String query = "create table if not EXISTS " + tableName + " (";

        List<String> dataType = new ArrayList<String>(columns.values());
        List<String> names = new ArrayList<String>(columns.keySet());
        for (int i = 0; i < names.size(); i++) {
            //     System.out.println(names.get(i) + "====>" + dataType.get(i));

            query += names.get(i) + " " + dataType.get(i);
            if (i < names.size() - 1) {
                query += ",";
            }
        }
        query += ")";

        dbPreparedQuery(query);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.myDb = sqLiteDatabase;

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean dbQuery(String query) {
        try {
            // Log.i(" Query :  ==>", query);
            query = query.replaceAll("\\p{M}", "");
            myDb.execSQL(query);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    public String replaceName(String name) {

        if (name.equalsIgnoreCase("TempMCQTestHDRID")) {
            return "StudentMCQTestHDRID";
        } else {
            return name;
        }

    }

    public long insertRow(Object o, String tableName) {
        Class c;
        try {
            if (isTableExists(tableName)) {
                //   Log.i("TAG Table exists", "INSERT ROW::->" + tableName);
                ContentValues values = new ContentValues();
                c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                Field[] fields = c.getFields();
                for (Field filed : fields) {
                    Object type = filed.getType();


                    if (filed.getName().equalsIgnoreCase("CreatedOn")) {
                        values.put(replaceName(filed.getName()), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                    } else {
                        if (filed.getType().isAssignableFrom(String.class)) {
                            //   Log.i("TAG  " + tableName + " inserted :: String ", filed.getName() + " :-> " + (String) filed.get(o));

                            if ((String) filed.get(o) == null) {
                                values.put(replaceName(filed.getName()), "");
                            } else {
                                values.put(replaceName(filed.getName()), (String) filed.get(o));
                            }
                        }
                        if (type == int.class) {
                            if ((int) filed.get(o) != -1) {
                                // Log.i("TAG  " + tableName + " inserted integer  :: ", filed.getName() + " :-> " + (Integer) filed.get(o));
                                values.put(replaceName(filed.getName()), (Integer) filed.get(o));
                            }
                        }
                    }

                }
                return myDb.insert(tableName, null, values);
            }
            if (tableName.equals("class_evaluater_cct_count")) {

                DBTables dbTables = new DBTables();
                if (isTableExists(tableName)) {

                    ContentValues values = new ContentValues();
                    c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                    Field[] fields = c.getFields();
                    for (Field filed : fields) {
                        Object type = filed.getType();


                        if (filed.getName().equalsIgnoreCase("CreatedOn")) {
                            values.put(replaceName(filed.getName()), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        } else {
                            if (filed.getType().isAssignableFrom(String.class)) {
                                // Log.i("TAG  " + tableName + " inserted :: String ", filed.getName() + " :-> " + (String) filed.get(o));

                                if ((String) filed.get(o) == null) {
                                    values.put(replaceName(filed.getName()), "");
                                } else {
                                    values.put(replaceName(filed.getName()), (String) filed.get(o));
                                }
                            }
                            if (type == int.class) {
                                if ((int) filed.get(o) != -1) {
                                    // Log.i("TAG  " + tableName + " inserted integer  :: ", filed.getName() + " :-> " + (Integer) filed.get(o));
                                    values.put(replaceName(filed.getName()), (Integer) filed.get(o));
                                }
                            }
                        }

                    }
                    return myDb.insert(tableName, null, values);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return -1;
    }

    public long insertWebRow(Object o, String tableName) {
        Class c;
        try {
            if (isTableExists(tableName)) {
                ContentValues values = new ContentValues();
                c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                Field[] fields = c.getFields();
                for (Field filed : fields) {
                    Object type = filed.getType();


                    if (filed.getType().isAssignableFrom(String.class)) {
                        //Log.i("TAG  " + tableName + " inserted :: String ", filed.getName() + " :-> " + (String) filed.get(o));

                        if ((String) filed.get(o) == null) {
                            values.put(replaceName(filed.getName()), "");
                        } else {
                            values.put(replaceName(filed.getName()), (String) filed.get(o));
                        }
                    }
                    if (type == int.class) {
                        if ((int) filed.get(o) != -1) {
                            // Log.i("TAG  " + tableName + " inserted integer  :: ", filed.getName() + " :-> " + (Integer) filed.get(o));
                            values.put(replaceName(filed.getName()), (Integer) filed.get(o));
                        }
                    }


                }


                return myDb.insert(tableName, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        return -1;
    }

    public long insertRowWithHelper(Object o, String tableName) {
        Class c;
        try {
            ContentValues values = new ContentValues();
            c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
            Field[] fields = c.getFields();

            String statement = " insert into " + tableName + " (";


            for (Field f : fields) {
                if (f.getType().isAssignableFrom(String.class)) {

                    statement += f.getName() + ",";
                } else if (f.getType() == int.class) {

                    statement += f.getName() + ",";
                }
            }
            statement = statement.substring(0, statement.length() - 1);
            statement += ") VALUES (";
            for (int i = 0; i < fields.length; i++) {
                statement += "?,";
            }
            statement = statement.substring(0, statement.length() - 1);
            Log.i("TABLE  QUERY :", statement);

            for (Field filed : fields) {
                Object type = filed.getType();


                if (filed.getName().equalsIgnoreCase("CreatedOn")) {
                    values.put(replaceName(filed.getName()), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                } else {
                    if (filed.getType().isAssignableFrom(String.class)) {
                        //Log.i("TAG  " + tableName + " inserted :: String ", filed.getName() + " :-> " + (String) filed.get(o));

                        if ((String) filed.get(o) == null) {
                            values.put(replaceName(filed.getName()), "");
                        } else {
                            values.put(replaceName(filed.getName()), (String) filed.get(o));
                        }
                    }
                    if (type == int.class) {
                        if ((int) filed.get(o) != -1) {
                            //Log.i("TAG  " + tableName + " inserted integer  :: ", filed.getName() + " :-> " + (Integer) filed.get(o));
                            values.put(replaceName(filed.getName()), (Integer) filed.get(o));
                        }
                    }
                }

            }
            return myDb.insert(tableName, null, values);

        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }


    }

    public void insertTempRow(Object o, String tableName) {
        Class c;
        try {
            if (isTableExists(tableName)) {
                ContentValues values = new ContentValues();
                c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                Field[] fields = c.getFields();
                for (Field filed : fields) {
                    Object type = filed.getType();

                    //  Log.i("TAG  " + tableName + " inserted :: String ", filed.getName() + " :-> " + (String) filed.get(o));

                    if (filed.getType().isAssignableFrom(String.class)) {
                        values.put(filed.getName(), (String) filed.get(o));
                    }
                    if (filed.getType().isAssignableFrom(Integer.class)) {
                        if ((Integer) filed.get(o) != -1)
                            values.put(filed.getName(), (Integer) filed.get(o));
                    }

                }
                myDb.insert(tableName, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void updateRow(Object o, String tableName, String tableQuery) {
        Class c;
        try {
            if (isTableExists(tableName)) {
                ContentValues values = new ContentValues();
                c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());
                Field[] fields = c.getFields();
                for (Field filed : fields) {
                    Object type = filed.getType();
                    if (filed.getType().isAssignableFrom(String.class)) {
                        values.put(filed.getName(), (String) filed.get(o));
                    }
                    if (filed.getType().isAssignableFrom(Integer.class)) {
                        values.put(filed.getName(), (Integer) filed.get(o));
                    }
                }
                myDb.update(tableName, values, tableQuery, null);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }


    public void emptyTable(String tableName) {
        try {
            myDb.delete(tableName, null, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeTable(String tableName) {
        try {
            String query = "DROP TABLE IF EXISTS " + tableName;
            myDb.execSQL(query);
            //   myDb.delete(tableName, null, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean dbPreparedQuery(String query) {
        try {
            Log.i(" Query :  ==>", query);
            myDb.execSQL(query);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }


    public Cursor getAllRows(String query) {
        try {
            Log.i(" Query :  ==>", query);
            Cursor res = myDb.rawQuery(query, null);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public int getLastRecordID(String tablename, String columnName) {
        String countQuery = "SELECT  " + columnName + " FROM " + tablename + "  ORDER BY " + columnName + " DESC Limit 1";
        Cursor cursor = myDb.rawQuery(countQuery, null);
        if (cursor != null && cursor.moveToNext()) {
            int data = cursor.getInt(0);
            cursor.close();
            return data;
        } else {
            cursor.close();
            return -1;
        }
    }

    public int getLastRecordIDForRequest(String tablename, String columnName) {
        String countQuery = "SELECT  " + columnName + " FROM " + tablename + "  ORDER BY " + columnName + " DESC Limit 1";
        Cursor cursor = myDb.rawQuery(countQuery, null);
        if (cursor != null && cursor.moveToNext()) {
            int data = cursor.getInt(0);
            Log.i("Printing data ", tablename + "  " + columnName + " :->" + data);
            cursor.close();
            return data;
        } else {
            cursor.close();
            return 0;
        }
    }


    public int getTotalUsingJoinQuery(String countQuery) {
        Cursor cursor = myDb.rawQuery(countQuery, null);
        if (cursor != null && cursor.moveToNext()) {
            int data = cursor.getInt(0);
            cursor.close();
            return data;
        } else {
            cursor.close();
            return -1;
        }
    }

    public int getAllRecordsUsingQuery(String query) {

        //Log.i(" QUERY :=>  ", query);

        try {
            Cursor cursor = myDb.rawQuery(query, null);
            int cnt = cursor.getCount();
            cursor.close();

            return cnt;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

    }

    public int getAllReports(String query) {
        try {
            Cursor cursor = myDb.rawQuery(query, null);
            int cnt = cursor.getCount();
            cursor.close();

            return cnt;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    public int getAllRecords(String TABLE_NAME) {
        if (isTableExists(TABLE_NAME)) {
            String countQuery = "SELECT  * FROM " + TABLE_NAME;

            Cursor cursor = myDb.rawQuery(countQuery, null);
            int cnt = cursor.getCount();
            cursor.close();
            return cnt;
        } else {
            return 0;
        }

    }

    public boolean isTableExists(String tableName) {

        if (tableName == null || myDb == null || !myDb.isOpen()) {
            return false;
        }
        Cursor cursor = myDb.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[]{"table", tableName});
        // Log.i("Cursor isTableExists", DatabaseUtils.dumpCursorToString(cursor));
        if (!cursor.moveToFirst()) {
            cursor.close();
            return false;
        }
        int count = cursor.getInt(0);
        cursor.close();
        return count > 0;
    }

    public int getJoin(String FILED_NAME1) {
        String countQuery = "SELECT  * FROM " + FILED_NAME1;

        Cursor cursor = myDb.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    private class DatabaseContext extends ContextWrapper {

        private static final String DEBUG_CONTEXT = "DatabaseContext";

        private DatabaseContext(Context base) {
            super(base);
        }


        @Override
        public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory) {
            SQLiteDatabase result = SQLiteDatabase.openOrCreateDatabase(getDatabasePath(DBConstant.DB_NAME), null);
            // SQLiteDatabase result = super.openOrCreateDatabase(name, mode, factory);
            if (Log.isLoggable(TAG, Log.WARN)) {
                Log.w(TAG,
                        "openOrCreateDatabase(" + name + ",,) = " + result.getPath());
            }
            return result;
        }

        @Override
        public File getDatabasePath(String name) {

            /*String dbfile = Environment.getExternalStorageDirectory()
                    + "/" + DBConstant.IMAGE_FOLDER + File.separator + "databases" + File.separator + name;*/

            String directoryPath = Utils.getDataDir(myContext);

            if (!directoryPath.isEmpty()) {

                String dbfile = directoryPath + File.separator + name;

                if (!dbfile.endsWith(".db")) {
                    dbfile += ".db";
                }

                File result = new File(dbfile);

                if (!result.getParentFile().exists()) {
                    result.getParentFile().mkdirs();
                }

                if (Log.isLoggable(DEBUG_CONTEXT, Log.WARN)) {
                    Log.w(DEBUG_CONTEXT,
                            "getDatabasePath(" + name + ") = " + result.getAbsolutePath());
                }
                return result;
            } else {
                Utils.showToast("Not available data direcotry", myContext);
                return null;
            }


        }


    }

}

