package com.iscore.parent.interfaceparent;

public interface OTPListener {

    public void otpReceived(String messageText);
}