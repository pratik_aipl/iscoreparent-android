package com.iscore.parent.interfaceparent;

import com.iscore.parent.activity.library.PersonalFiles;

public interface PersonalFolderCallback {

        void onItemClick(PersonalFiles file);

    }
