package com.iscore.parent.model;

import java.io.Serializable;
import java.util.ArrayList;

public class CctReport implements Serializable {
    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    /*SubjectID: "1",
    SubjectName: "English",

    COUNT(CEMCQID): "1"*/
    public String SubjectID = "";
    public String SubjectName = "";
    public String Total = "";
    public String Acuuracy="";
    public String cct_attemps="";
    public String cct_overall_accuracy="";

    public String getCct_overall_accuracy() {
        return cct_overall_accuracy;
    }

    public void setCct_overall_accuracy(String cct_overall_accuracy) {
        this.cct_overall_accuracy = cct_overall_accuracy;
    }

    public ArrayList<CctReport> getCctReportsArray() {
        return cctReportsArray;
    }

    public void setCctReportsArray(ArrayList<CctReport> cctReportsArray) {
        this.cctReportsArray = cctReportsArray;
    }

    public ArrayList<CctReport> cctReportsArray = new ArrayList<>();

    public String getCct_attemps() {
        return cct_attemps;
    }

    public void setCct_attemps(String cct_attemps) {
        this.cct_attemps = cct_attemps;
    }

    public String getAcuuracy() {
        return Acuuracy;
    }

    public void setAcuuracy(String acuuracy) {
        Acuuracy = acuuracy;
    }
}
