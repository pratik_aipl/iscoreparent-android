package com.iscore.parent.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 3/23/2017.
 */
public class Comparision implements Serializable{
  public String  Subject="",Mcq_total_test="",Paper_total_test="",scalar="";
  public int mcq_accuracy,cct_total_test,cct_accuracy;

    public String getScalar() {
        return scalar;
    }

    public void setScalar(String scalar) {
        this.scalar = scalar;
    }

    public int getMcq_accuracy() {
        return mcq_accuracy;
    }

    public void setMcq_accuracy(int mcq_accuracy) {
        this.mcq_accuracy = mcq_accuracy;
    }

    public int getCct_total_test() {
        return cct_total_test;
    }

    public void setCct_total_test(int cct_total_test) {
        this.cct_total_test = cct_total_test;
    }

    public int getCct_accuracy() {
        return cct_accuracy;
    }

    public void setCct_accuracy(int cct_accuracy) {
        this.cct_accuracy = cct_accuracy;
    }

    public ArrayList<Comparision> getCompArray() {
        return compArray;
    }

    public void setCompArray(ArrayList<Comparision> compArray) {
        this.compArray = compArray;
    }

    public ArrayList<Comparision> compArray = new ArrayList<>();

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getMcq_total_test() {
        return Mcq_total_test;
    }

    public void setMcq_total_test(String mcq_total_test) {
        Mcq_total_test = mcq_total_test;
    }

    public String getPaper_total_test() {
        return Paper_total_test;
    }

    public void setPaper_total_test(String paper_total_test) {
        Paper_total_test = paper_total_test;
    }
}
