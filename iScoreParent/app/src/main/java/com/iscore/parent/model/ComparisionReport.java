package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 4/5/2017.
 */

public class ComparisionReport implements Serializable {
    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        this.SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        this.SubjectName = subjectName;
    }

    public String getTotalMcq() {
        return TotalMcq;
    }

    public void setTotalMcq(String totalMcq) {
        this.TotalMcq = totalMcq;
    }

    public String getToatalGenratPaper() {
        return ToatalGenratPaper;
    }

    public String getMcqHdrID() {
        return McqHdrID;
    }

    public void setMcqHdrID(String mcqHdrID) {
        McqHdrID = mcqHdrID;
    }

    public void setToatalGenratPaper(String toatalGenratPaper) {
        this.ToatalGenratPaper = toatalGenratPaper;
    }

    public String SubjectID = "";
    public String SubjectName = "";
    public String TotalMcq = "";
    public String ToatalGenratPaper = "";
    public String McqHdrID = "";
    public String TotalCct = "";
    public String Accuracy_cct = "";

    public String getAccuracy_cct() {
        return Accuracy_cct;
    }

    public void setAccuracy_cct(String accuracy_cct) {
        Accuracy_cct = accuracy_cct;
    }

    public String getTotalCct() {
        return TotalCct;
    }

    public void setTotalCct(String totalCct) {
        TotalCct = totalCct;
    }

    public String getAccuracy_MCQ() {
        return Accuracy_MCQ;
    }

    public void setAccuracy_MCQ(String accuracy_MCQ) {
        Accuracy_MCQ = accuracy_MCQ;
    }

    public String Accuracy_MCQ = "";


}
