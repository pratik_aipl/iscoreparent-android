package com.iscore.parent.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 7/26/2017.
 */

public class EvaluterMcqTestMain implements Serializable, Cloneable {

    public String Question = "", ClassMCQTestDTLID = "",Level="",IsRight = "", MasterorCustomized = "", QuestionID = "", AnswerID = "", IsAttempt = "";
    public String ClassMCQTestHDRID = "";
    public String start_time = "";
    public boolean isHeader = false;

    public String getLevel() {
        return Level;
    }

    public void setLevel(String level) {
        Level = level;
    }

    public Object clone() throws CloneNotSupportedException {

        return super.clone();
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public boolean isRightt = false;
    public int selectedAns = -1;

    public String getIsRight() {
        return IsRight;
    }

    public void setIsRight(String isRight) {
        IsRight = isRight;
    }

   public int isAttemptedd = -1;
    public ArrayList<EvaluterQuestionOption> optionArray = new ArrayList<>();

    public String getClassMCQTestHDRID() {
        return ClassMCQTestHDRID;
    }

    public void setClassMCQTestHDRID(String classMCQTestHDRID) {
        ClassMCQTestHDRID = classMCQTestHDRID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getClassMCQTestDTLID() {
        return ClassMCQTestDTLID;
    }

    public void setClassMCQTestDTLID(String classMCQTestDTLID) {
        ClassMCQTestDTLID = classMCQTestDTLID;
    }

    public String getMasterorCustomized() {
        return MasterorCustomized;
    }

    public void setMasterorCustomized(String masterorCustomized) {
        MasterorCustomized = masterorCustomized;
    }

    public String getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(String questionID) {
        QuestionID = questionID;
    }

    public String getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(String answerID) {
        AnswerID = answerID;
    }

    public String getIsAttempt() {
        return IsAttempt;
    }

    public void setIsAttempt(String isAttempt) {
        IsAttempt = isAttempt;
    }

    public ArrayList<EvaluterQuestionOption> getOptionArray() {
        return optionArray;
    }

    public void setOptionArray(ArrayList<EvaluterQuestionOption> optionArray) {
        this.optionArray = optionArray;
    }
}
