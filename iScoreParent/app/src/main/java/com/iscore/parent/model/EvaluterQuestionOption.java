package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 7/26/2017.
 */

public class EvaluterQuestionOption implements Serializable {

    public boolean isRight =false;
    public int isRightAns =-1;
   public String MCQOPtionID = "", Options = "", isCorrect = "";

    public String getMCQOPtionID() {
        return MCQOPtionID;
    }

    public void setMCQOPtionID(String MCQOPtionID) {
        this.MCQOPtionID = MCQOPtionID;
    }

    public String getOptions() {
        return Options;
    }

    public void setOptions(String options) {
        Options = options;
    }

    public String getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        this.isCorrect = isCorrect;
    }
}
