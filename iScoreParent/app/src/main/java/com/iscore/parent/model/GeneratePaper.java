package com.iscore.parent.model;

/**
 * Created by empiere-vaibhav on 1/3/2018.
 */

public class GeneratePaper {
    public String totalQue;
    public boolean isQuestionType = false;
    public boolean isSolution = true;

    public GeneratePaper() {

    }

    public GeneratePaper(String totalQue, boolean isQuestionType) {
        this.totalQue = totalQue;
        this.isQuestionType = isQuestionType;
    }


    public String getTotalQue() {
        return totalQue;
    }

    public void setTotalQue(String totalQue) {
        this.totalQue = totalQue;
    }
}
