package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/6/2018.
 */

public class KeyData implements Serializable {
    public String STKID = "";
    public String StudentID = "";
    public String BoardID = "";
    public String MediumID = "";
    public String StandardID = "";
    public String ClassID = "";
    public String BranchID = "";
    public String BatchID = "";
    public String StudentKey = "";
    public String ActiveKey = "";
    public String isDemo = "";
    public String CreatedOn = "";
    public String StandardName = "";
    public String ClassName = "";
    public String BoardName = "";
    public String PreviousYearScore="";
    public String ClassLogo="";
    public String ClassRoundLogo="";
    public String SetTarget="";

    public String getSetTarget() {
        return SetTarget;
    }

    public void setSetTarget(String setTarget) {
        SetTarget = setTarget;
    }

    public String getClassRoundLogo() {
        return ClassRoundLogo;
    }

    public void setClassRoundLogo(String classRoundLogo) {
        ClassRoundLogo = classRoundLogo;
    }

    public String getClassLogo() {
        return ClassLogo;
    }

    public void setClassLogo(String classLogo) {
        ClassLogo = classLogo;
    }

    public String getPreviousYearScore() {
        return PreviousYearScore;
    }

    public void setPreviousYearScore(String previousYearScore) {
        PreviousYearScore = previousYearScore;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String MediumName = "";

    public String getSTKID() {
        return STKID;
    }

    public void setSTKID(String STKID) {
        this.STKID = STKID;
    }

    public String getStudentID() {
        return StudentID;
    }

    public void setStudentID(String studentID) {
        StudentID = studentID;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getBranchID() {
        return BranchID;
    }

    public void setBranchID(String branchID) {
        BranchID = branchID;
    }

    public String getBatchID() {
        return BatchID;
    }

    public void setBatchID(String batchID) {
        BatchID = batchID;
    }

    public String getStudentKey() {
        return StudentKey;
    }

    public void setStudentKey(String studentKey) {
        StudentKey = studentKey;
    }

    public String getActiveKey() {
        return ActiveKey;
    }

    public void setActiveKey(String activeKey) {
        ActiveKey = activeKey;
    }

    public String getIsDemo() {
        return isDemo;
    }

    public void setIsDemo(String isDemo) {
        this.isDemo = isDemo;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }
}
