package com.iscore.parent.model;

import java.io.Serializable;

public class LevelTestReportMCQ implements Serializable {
        public int Total_MCQ_Que = 0;
        public int Total_right = 0;

        public int getTotal_MCQ_Que() {
            return Total_MCQ_Que;
        }

        public void setTotal_MCQ_Que(int total_MCQ_Que) {
            Total_MCQ_Que = total_MCQ_Que;
        }

        public int getTotal_right() {
            return Total_right;
        }

        public void setTotal_right(int total_right) {
            Total_right = total_right;
        }
    }