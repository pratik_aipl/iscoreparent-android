package com.iscore.parent.model;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 3/23/2017.
 */

public class MCQ {
    public String Mcq_attemps = "";
    public String Level_1 = "";
    public String Level_2 = "";
    public String Level_3 = "";
    public String Subject = "";
    public String Accuracy = "";
    public String subject_total_test = "";
    public String Totallevel1 = "";
    public String Totallevel2 = "";
    public String Totallevel3 = "";
    public int accurecy_level1 ;

    public String getSubject_total_test() {
        return subject_total_test;
    }

    public void setSubject_total_test(String subject_total_test) {
        this.subject_total_test = subject_total_test;
    }

    public String getTotallevel1() {
        return Totallevel1;
    }

    public void setTotallevel1(String totallevel1) {
        Totallevel1 = totallevel1;
    }

    public String getTotallevel2() {
        return Totallevel2;
    }

    public void setTotallevel2(String totallevel2) {
        Totallevel2 = totallevel2;
    }

    public String getTotallevel3() {
        return Totallevel3;
    }

    public void setTotallevel3(String totallevel3) {
        Totallevel3 = totallevel3;
    }

    public int getAccurecy_level1() {
        return accurecy_level1;
    }

    public void setAccurecy_level1(int accurecy_level1) {
        this.accurecy_level1 = accurecy_level1;
    }

    public int getAccurecy_level2() {
        return accurecy_level2;
    }

    public void setAccurecy_level2(int accurecy_level2) {
        this.accurecy_level2 = accurecy_level2;
    }

    public int getAccurecy_level3() {
        return accurecy_level3;
    }

    public void setAccurecy_level3(int accurecy_level3) {
        this.accurecy_level3 = accurecy_level3;
    }

    public int getAccurecy_level() {
        return accurecy_level;
    }

    public void setAccurecy_level(int accurecy_level) {
        this.accurecy_level = accurecy_level;
    }

    public ArrayList<MCQ> getMcqArray() {
        return mcqArray;
    }

    public void setMcqArray(ArrayList<MCQ> mcqArray) {
        this.mcqArray = mcqArray;
    }

    public int accurecy_level2;
    public int accurecy_level3;
    public int accurecy_level;

    public ArrayList<MCQ> mcqArray = new ArrayList<>();

    public String getMcq_attemps() {
        return Mcq_attemps;
    }

    public void setMcq_attemps(String mcq_attemps) {
        Mcq_attemps = mcq_attemps;
    }

    public String getLevel_1() {
        return Level_1;
    }

    public void setLevel_1(String level_1) {
        Level_1 = level_1;
    }

    public String getLevel_2() {
        return Level_2;
    }

    public void setLevel_2(String level_2) {
        Level_2 = level_2;
    }

    public String getLevel_3() {
        return Level_3;
    }

    public void setLevel_3(String level_3) {
        Level_3 = level_3;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(String accuracy) {
        Accuracy = accuracy;
    }
}
