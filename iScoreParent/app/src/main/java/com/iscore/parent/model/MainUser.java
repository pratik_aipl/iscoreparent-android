package com.iscore.parent.model;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 2/28/2017.
 */

public class MainUser {


    public static String SubjectID = "";
    public static String SubjectName = "";
    public static String BoardID = "";
    public static String FirstName = "";
    public static String Student_id = "";
    public static String LastName = "";
    public static String RegKey = "";
    public static String Guid = "";
    public static String ClassID = "";
    public static String EmailID = "";
    public static String RegMobNo = "";
    public static String subjects = "";
    public static String StanderdName = "";
    public static String ClassName = "";
    public static String PLID = "";

    public static String getPLID() {
        return PLID;
    }

    public static void setPLID(String PLID) {
        MainUser.PLID = PLID;
    }

    public static String getKeyType() {
        return KeyType;
    }

    public static void setKeyType(String keyType) {
        KeyType = keyType;
    }

    public static String KeyStatus = "";
    public static String ActiveKey = "";
    public static String KeyType = "";
    public static String BoardName = "";

    public static String getStudentCode() {
        return StudentCode;
    }

    public static void setStudentCode(String studentCode) {
        StudentCode = studentCode;
    }

    public static String StudentCode = "";

    public static String getProfile_image() {
        return profile_image;
    }

    public static void setProfile_image(String profile_image) {
        MainUser.profile_image = profile_image;
    }

    public static String profile_image = "";

    public static String getPreviousYearScore() {
        return PreviousYearScore;
    }

    public static void setPreviousYearScore(String previousYearScore) {
        PreviousYearScore = previousYearScore;
    }

    public static String MediumName = "";
    public static String StandardName = "";
    public static String PreviousYearScore = "";
    public static String ProfileLastSync = "";

    public static String getProfileLastSync() {
        return ProfileLastSync;
    }

    public static void setProfileLastSync(String profileLastSync) {
        ProfileLastSync = profileLastSync;
    }


    public static String getMediumName() {
        return MediumName;
    }

    public static void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public static String getBoardName() {
        return BoardName;
    }

    public static void setBoardName(String boardName) {
        BoardName = boardName;
    }


    public static String getActiveKey() {
        return ActiveKey;
    }

    public static void setActiveKey(String activeKey) {
        ActiveKey = activeKey;
    }

    public static String Version = "0";

    public static String getVersion() {
        return Version;
    }

    public static void setVersion(String version) {
        Version = version;
    }

    public static String getBranchID() {
        return BranchID;
    }

    public static void setBranchID(String branchID) {
        BranchID = branchID;
    }

    public static String getBatchID() {
        return BatchID;
    }

    public static void setBatchID(String BatchID) {
        MainUser.BatchID = BatchID;
    }

    public static String getSchoolName() {
        return SchoolName;
    }

    public static void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public static String BranchID = "", BatchID = "", SchoolName = "";

    public static String getKeyStatus() {
        return KeyStatus;
    }

    public static void setKeyStatus(String keyStatus) {
        KeyStatus = keyStatus;
    }

    public static String getClassName() {
        return ClassName;
    }

    public static void setClassName(String className) {
        ClassName = className;
    }

    public static String getStanderdName() {
        return StanderdName;
    }

    public static void setStanderdName(String standerdName) {
        StanderdName = standerdName;
    }

    public static String getStudent_id() {
        return Student_id;
    }

    public static void setStudent_id(String student_id) {
        Student_id = student_id;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public static String getFirstName() {
        return FirstName;
    }

    public static void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public static String getLastName() {
        return LastName;
    }

    public static void setLastName(String lastName) {
        LastName = lastName;
    }

    public static String getRegKey() {
        return RegKey;
    }

    public static void setRegKey(String regKey) {
        RegKey = regKey;
    }

    public static String getGuid() {
        return Guid;
    }

    public static void setGuid(String guid) {
        MainUser.Guid = guid;
    }

    public static String getClassID() {
        return ClassID;
    }

    public static void setClassID(String classID) {
        ClassID = classID;
    }

    public static String getRegMobNo() {
        return RegMobNo;
    }

    public static void setRegMobNo(String regMobNo) {
        RegMobNo = regMobNo;
    }

    public static String getEmailID() {
        return EmailID;
    }

    public static void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public static String getSubjects() {
        return subjects;
    }

    public static void setSubjects(String subjects) {
        MainUser.subjects = subjects;
    }

    public static void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public static String getSubjectName() {
        return SubjectName;
    }

    public static void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public static String getBoardID() {
        return BoardID;
    }

    public static void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public static String getMediumID() {
        return MediumID;
    }

    public static void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public static String getStandardID() {
        return StandardID;
    }

    public static void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public static String getCreatedBy() {
        return CreatedBy;
    }

    public static void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public static String getCreatedOn() {
        return CreatedOn;
    }

    public static void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public static String getModifiedBy() {
        return ModifiedBy;
    }

    public static void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public static String getModifiedOn() {
        return ModifiedOn;
    }

    public static void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public static String MediumID = "";
    public static String StandardID = "";
    public static String CreatedBy = "";
    public static String CreatedOn = "";
    public static String ModifiedBy = "";

    public static String getExpiryDate() {
        return ExpiryDate;
    }

    public static void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public static String ExpiryDate = "";
    public static String ModifiedOn = "";

    public static ArrayList<KeyData> keyDataArray = new ArrayList<>();

}
