package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by admin on 2/28/2017.
 */
public class McqOption implements Serializable {
    public String MCQOPtionID;
    public String MCQQuestionID;
    public String Options;
    public String ImageURL;
    public String OptionNo;
    public String isCorrect ;
    public String CreatedBy;
    public String CreatedOn;
    public String ModifiedBy;
    public String ModifiedOn;
    public int isRightAns =-1;

    public McqOption() {

    }

    public McqOption(String MCQOPtionID, String MCQQuestionID, String Options, String ImageURL,
                     String OptionNo, String isCorrect, String CreatedBy,
                     String CreatedOn, String ModifiedBy, String ModifiedOn) {

        this.MCQOPtionID = MCQOPtionID;
        this.MCQQuestionID = MCQQuestionID;
        this.Options = Options;
        this.ImageURL = ImageURL;
        this.OptionNo = OptionNo;
        this.isCorrect = isCorrect;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;

    }

    public String getMCQOPtionID() {
        return MCQOPtionID;
    }

    public void setMCQOPtionID(String MCQOPtionID) {
        this.MCQOPtionID = MCQOPtionID;
    }

    public String getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(String MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }

    public String getOptions() {
        return Options;
    }

    public void setOptions(String options) {
        Options = options;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getOptionNo() {
        return OptionNo;
    }

    public void setOptionNo(String optionNo) {
        OptionNo = optionNo;
    }

    public String getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        this.isCorrect = isCorrect;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
