package com.iscore.parent.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by admin on 2/28/2017.
 */
public class McqQuestion implements Serializable, Cloneable {

    public String MCQQuestionID;
    public String Question;
    public String ImageURL;
    public String BoardID;
    public String MediumID;
    public String ClassID;
    public String StandardID;
    public String SubjectID;
    public String ChapterID;
    public String QuestionTypeID;
    public String QuestionLevelID;
    public String Duration;
    public String CreatedBy;
    public String CreatedOn;
    public String ModifiedBy;
    public String ModifiedOn;
    public String Solution = "";

    public String getSolution() {
        return Solution;
    }

    public void setSolution(String solution) {
        Solution = solution;
    }

    public boolean isRight = false;
    public int selectedAns = -1;
    public int isAttempted = -1;
    public boolean isHeader = false;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public ArrayList<McqOption> optionsArray = new ArrayList<>();

    public ArrayList<McqOption> getOptionsArray() {
        return optionsArray;
    }

    public void setOptionsArray(ArrayList<McqOption> optionsArray) {
        this.optionsArray = optionsArray;
    }

    public McqQuestion() {

    }


    public McqQuestion(String MCQQuestionID, String Question, String ImageURL, String BoardID, String MediumID, String ClassID,
                       String StandardID, String SubjectID, String ChapterID, String QuestionTypeID,
                       String QuestionLevelID, String Duration, String CreatedBy,
                       String CreatedOn, String ModifiedBy, String ModifiedOn) {

        this.MCQQuestionID = MCQQuestionID;
        this.Question = Question;
        this.ImageURL = ImageURL;
        this.BoardID = BoardID;
        this.MediumID = MediumID;
        this.ClassID = ClassID;
        this.StandardID = StandardID;
        this.SubjectID = SubjectID;
        this.ChapterID = ChapterID;
        this.QuestionTypeID = QuestionTypeID;
        this.QuestionLevelID = QuestionLevelID;
        this.Duration = Duration;
        this.CreatedBy = CreatedBy;
        this.CreatedOn = CreatedOn;
        this.ModifiedBy = ModifiedBy;
        this.ModifiedOn = ModifiedOn;
    }

    public String getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(String MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getBoardID() {
        return BoardID;
    }

    public void setBoardID(String boardID) {
        BoardID = boardID;
    }

    public String getMediumID() {
        return MediumID;
    }

    public void setMediumID(String mediumID) {
        MediumID = mediumID;
    }

    public String getClassID() {
        return ClassID;
    }

    public void setClassID(String classID) {
        ClassID = classID;
    }

    public String getStandardID() {
        return StandardID;
    }

    public void setStandardID(String standardID) {
        StandardID = standardID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getChapterID() {
        return ChapterID;
    }

    public void setChapterID(String chapterID) {
        ChapterID = chapterID;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getQuestionLevelID() {
        return QuestionLevelID;
    }

    public void setQuestionLevelID(String questionLevelID) {
        QuestionLevelID = questionLevelID;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }


}
