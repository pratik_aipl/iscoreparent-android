package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 11/4/2017.
 */

public class McqReport implements Serializable {
    public String SubjectID = "";
    public String SubjectName = "";
    public String TotalTest = "";
    public String TotalAccuraacy = "";

    public String getTotalTest() {
        return TotalTest;
    }

    public void setTotalTest(String totalTest) {
        TotalTest = totalTest;
    }

    public String getTotalAccuraacy() {
        return TotalAccuraacy;
    }

    public void setTotalAccuraacy(String totalAccuraacy) {
        TotalAccuraacy = totalAccuraacy;
    }

    public String TotalDFLone = "";
    public String TotalDFLtwo = "";
    public String TotalDFLthree = "";

    public String AccuLone = "";
    public String AccuLtwo = "";
    public String Accuthree = "";
    public String StudentMCQTestHDRID = "";

    public String getStudentMCQTestHDRID() {
        return StudentMCQTestHDRID;
    }

    public void setStudentMCQTestHDRID(String studentMCQTestHDRID) {
        StudentMCQTestHDRID = studentMCQTestHDRID;
    }

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getTotalDFLone() {
        return TotalDFLone;
    }

    public void setTotalDFLone(String totalDFLone) {
        TotalDFLone = totalDFLone;
    }

    public String getTotalDFLtwo() {
        return TotalDFLtwo;
    }

    public void setTotalDFLtwo(String totalDFLtwo) {
        TotalDFLtwo = totalDFLtwo;
    }

    public String getTotalDFLthree() {
        return TotalDFLthree;
    }

    public void setTotalDFLthree(String totalDFLthree) {
        TotalDFLthree = totalDFLthree;
    }

    public String getAccuLone() {
        return AccuLone;
    }

    public void setAccuLone(String accuLone) {
        AccuLone = accuLone;
    }

    public String getAccuLtwo() {
        return AccuLtwo;
    }

    public void setAccuLtwo(String accuLtwo) {
        AccuLtwo = accuLtwo;
    }

    public String getAccuthree() {
        return Accuthree;
    }

    public void setAccuthree(String accuthree) {
        Accuthree = accuthree;
    }

}
