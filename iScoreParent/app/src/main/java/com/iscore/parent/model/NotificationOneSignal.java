package com.iscore.parent.model;

import java.io.Serializable;

public class NotificationOneSignal implements Serializable {
 public String _id = "", notification_id = "", android_notification_id = "", group_id = "", collapse_id = "", is_summary = "",
 opened = "", dismissed = "", title = "", message = "", full_data = "", created_time = "";

 public String get_id() {
 return _id;
 }

 public void set_id(String _id) {
 this._id = _id;
 }

 public String getNotification_id() {
 return notification_id;
 }

 public void setNotification_id(String notification_id) {
 this.notification_id = notification_id;
 }

 public String getAndroid_notification_id() {
 return android_notification_id;
 }

 public void setAndroid_notification_id(String android_notification_id) {
 this.android_notification_id = android_notification_id;
 }

 public String getGroup_id() {
 return group_id;
 }

 public void setGroup_id(String group_id) {
 this.group_id = group_id;
 }

 public String getCollapse_id() {
 return collapse_id;
 }

 public void setCollapse_id(String collapse_id) {
 this.collapse_id = collapse_id;
 }

 public String getIs_summary() {
 return is_summary;
 }

 public void setIs_summary(String is_summary) {
 this.is_summary = is_summary;
 }

 public String getOpened() {
 return opened;
 }

 public void setOpened(String opened) {
 this.opened = opened;
 }

 public String getDismissed() {
 return dismissed;
 }

 public void setDismissed(String dismissed) {
 this.dismissed = dismissed;
 }

 public String getTitle() {
 return title;
 }

 public void setTitle(String title) {
 this.title = title;
 }

 public String getMessage() {
 return message;
 }

 public void setMessage(String message) {
 this.message = message;
 }

 public String getFull_data() {
 return full_data;
 }

 public void setFull_data(String full_data) {
 this.full_data = full_data;
 }

 public String getCreated_time() {
 return created_time;
 }

 public void setCreated_time(String created_time) {
 this.created_time = created_time;
 }
 }