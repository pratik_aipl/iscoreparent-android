package com.iscore.parent.model;

import java.io.Serializable;

public class NotificationType implements Serializable {
 public String CreatedOn = "";
 public int OneSignalID = -1;
 public String Message = "";
 public String Title = "";
 public String notification_type_id = "";
 public String isOpen = "";
 public String CctSubmit = "0";
 public String SubjectName = "";
 public String paper_type = "";
 public String android_notification_id="";

 public String getAndroid_notification_id() {
  return android_notification_id;
 }

 public void setAndroid_notification_id(String android_notification_id) {
  this.android_notification_id = android_notification_id;
 }

 public String getPaperType() {
 return paper_type;
 }

 public void setPaperType(String paperType) {
 paper_type = paperType;
 }

 public String getCctSubmit() {
 return CctSubmit;
 }

 public void setCctSubmit(String cctSubmit) {
 CctSubmit = cctSubmit;
 }

 public String getSubjectName() {
 return SubjectName;
 }

 public void setSubjectName(String subjectName) {
 SubjectName = subjectName;
 }

 public String getCreatedOn() {
 return CreatedOn;
 }

 public void setCreatedOn(String createdOn) {
 CreatedOn = createdOn;
 }

 public int getOneSignalID() {
 return OneSignalID;
 }

 public void setOneSignalID(int n_ID) {
 this.OneSignalID = n_ID;
 }

 public String getMessage() {
 return Message;
 }

 public void setMessage(String message) {
 Message = message;
 }

 public String getTitle() {
 return Title;
 }

 public void setTitle(String title) {
 Title = title;
 }

 public String getNotification_type_id() {
 return notification_type_id;
 }

 public void setNotification_type_id(String notification_type_id) {
 this.notification_type_id = notification_type_id;
 }

 public String getIsOpen() {
 return isOpen;
 }

 public void setIsOpen(String isOpen) {
 this.isOpen = isOpen;
 }

 public String getModuleType() {
 return ModuleType;
 }

 public void setModuleType(String type) {
 this.ModuleType = type;
 }

 public String getIsTypeOpen() {
 return isTypeOpen;
 }

 public void setIsTypeOpen(String isTypeOpen) {
 this.isTypeOpen = isTypeOpen;
 }

 public String ModuleType = "";
 public String isTypeOpen = "";
}