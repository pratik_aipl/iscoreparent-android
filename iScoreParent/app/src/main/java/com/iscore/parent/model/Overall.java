package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 3/23/2017.
 */

public class Overall implements Serializable {
    public String mcq_attemps = "", cct_test_total = "", cct_overall_accuracy = "", ready_papaer_download = "", set_papaer_download = "", prelim_papaer_download = "", board_papaer_download = "";
    public int practice_paper_download=0;

    public String getMcq_attemps() {
        return mcq_attemps;
    }

    public String getCct_test_total() {
        return cct_test_total;
    }

    public void setCct_test_total(String cct_test_total) {
        this.cct_test_total = cct_test_total;
    }

    public String getCct_overall_accuracy() {
        return cct_overall_accuracy;
    }

    public void setCct_overall_accuracy(String cct_overall_accuracy) {
        this.cct_overall_accuracy = cct_overall_accuracy;
    }

    public int getPractice_paper_download() {
        return practice_paper_download;
    }

    public void setPractice_paper_download(int practice_paper_download) {
        this.practice_paper_download = practice_paper_download;
    }

    public void setMcq_attemps(String mcq_attemps) {
        this.mcq_attemps = mcq_attemps;
    }

    public String getReady_papaer_download() {
        return ready_papaer_download;
    }

    public void setReady_papaer_download(String ready_papaer_download) {
        this.ready_papaer_download = ready_papaer_download;
    }

    public String getSet_papaer_download() {
        return set_papaer_download;
    }

    public void setSet_papaer_download(String set_papaer_download) {
        this.set_papaer_download = set_papaer_download;
    }

    public String getPrelim_papaer_download() {
        return prelim_papaer_download;
    }

    public void setPrelim_papaer_download(String prelim_papaer_download) {
        this.prelim_papaer_download = prelim_papaer_download;
    }

    public String getBoard_papaer_download() {
        return board_papaer_download;
    }

    public void setBoard_papaer_download(String board_papaer_download) {
        this.board_papaer_download = board_papaer_download;
    }
}

