package com.iscore.parent.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PrelimQuestionListModel implements Serializable {
    public String pos;

    public PrelimQuestionListModel(String pos, ArrayList<PrelimTestRecord> prelimList) {
        this.pos = pos;
        this.prelimList = prelimList;
    }


    public PrelimQuestionListModel() {

    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }


    public ArrayList<PrelimTestRecord> prelimList = new ArrayList<>();
    public ArrayList<SetPaperQuestionAndTypes> prelimSetList = new ArrayList<>();

}