package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 1/23/2018.
 */

public class PrelimTestRecord implements Serializable {
    public String ExamTypePatternDetailID = "";
    public String QuestionNo = "";
    public String SubQuestionNo = "";
    public String QuestionTypeText = "";
    public String isQuestion = "";
    public String PageNo = "";
    public String Question = "";
    public String Answer = "";
    public String QuestionMarks = "";
    public String QuestionTypeID = "";

    public String getIsQuestionVisible() {
        return isQuestionVisible;
    }

    public void setIsQuestionVisible(String isQuestionVisible) {
        this.isQuestionVisible = isQuestionVisible;
    }

    public String isQuestionVisible = "";

    public String getQuestionMarks() {
        return QuestionMarks;
    }

    public void setQuestionMarks(String questionMarks) {
        QuestionMarks = questionMarks;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public boolean isSolution = false;

    public String getExamTypePatternDetailID() {
        return ExamTypePatternDetailID;
    }

    public void setExamTypePatternDetailID(String examTypePatternDetailID) {
        ExamTypePatternDetailID = examTypePatternDetailID;
    }

    public String getQuestionNo() {
        return QuestionNo;
    }

    public void setQuestionNo(String questionNo) {
        QuestionNo = questionNo;
    }

    public String getSubQuestionNo() {
        return SubQuestionNo;
    }

    public void setSubQuestionNo(String subQuestionNo) {
        SubQuestionNo = subQuestionNo;
    }

    public String getQuestionTypeText() {
        return QuestionTypeText;
    }

    public void setQuestionTypeText(String questionTypeText) {
        QuestionTypeText = questionTypeText;
    }

    public String getIsQuestion() {
        return isQuestion;
    }

    public void setIsQuestion(String isQuestion) {
        this.isQuestion = isQuestion;
    }

    public String getPageNo() {
        return PageNo;
    }

    public void setPageNo(String pageNo) {
        PageNo = pageNo;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }
}
