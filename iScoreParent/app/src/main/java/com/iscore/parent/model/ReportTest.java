package com.iscore.parent.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 5/4/2018.
 */

public class ReportTest implements Serializable {

    public String getNo_of_paper_generate() {
        return No_of_paper_generate;
    }

    public void setNo_of_paper_generate(String no_of_paper_generate) {
        No_of_paper_generate = no_of_paper_generate;
    }

    public ArrayList<ReportTest> testArray = new ArrayList<>();

    public String getWith_self_assesment() {
        return With_self_assesment;
    }

    public void setWith_self_assesment(String with_self_assesment) {
        With_self_assesment = with_self_assesment;
    }

    public String getWithout_self_assesment() {
        return Without_self_assesment;
    }

    public void setWithout_self_assesment(String without_self_assesment) {
        Without_self_assesment = without_self_assesment;
    }

    public String getPrelim_test() {
        return Prelim_test;
    }

    public void setPrelim_test(String prelim_test) {
        Prelim_test = prelim_test;
    }

    public String getSet_paper_test() {
        return Set_paper_test;
    }

    public ArrayList<ReportTest> getTestArray() {
        return testArray;
    }

    public void setTestArray(ArrayList<ReportTest> testArray) {
        this.testArray = testArray;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getPrelim_Paper_test() {
        return prelim_Paper_test;
    }

    public void setPrelim_Paper_test(String prelim_Paper_test) {
        this.prelim_Paper_test = prelim_Paper_test;
    }

    public String getReady_test() {
        return ready_test;
    }

    public void setReady_test(String ready_test) {
        this.ready_test = ready_test;
    }

    public String getSet_test() {
        return set_test;
    }

    public void setSet_test(String set_test) {
        this.set_test = set_test;
    }

    public String getTotal_test() {
        return total_test;
    }

    public void setTotal_test(String total_test) {
        this.total_test = total_test;
    }

    public void setSet_paper_test(String set_paper_test) {
        Set_paper_test = set_paper_test;
    }

    public String getReady_paper_test() {
        return Ready_paper_test;
    }

    public void setReady_paper_test(String ready_paper_test) {
        Ready_paper_test = ready_paper_test;
    }

    public String No_of_paper_generate = "", With_self_assesment = "", Without_self_assesment = "", Prelim_test = "", Set_paper_test = "", Ready_paper_test = "", subject_name = "", prelim_Paper_test = "", ready_test = "", set_test = "", total_test = "";
}

