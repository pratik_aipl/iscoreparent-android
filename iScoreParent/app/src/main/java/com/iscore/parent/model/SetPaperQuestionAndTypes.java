package com.iscore.parent.model;

import java.io.Serializable;

public class SetPaperQuestionAndTypes implements Serializable {

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String answer) {
        Answer = answer;
    }

    public String getMQuestionID() {
        return MQuestionID;
    }

    public void setMQuestionID(String MQuestionID) {
        this.MQuestionID = MQuestionID;
    }

    public String getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(String questionType) {
        QuestionType = questionType;
    }

    public String getQuestionTypeID() {
        return QuestionTypeID;
    }

    public void setQuestionTypeID(String questionTypeID) {
        QuestionTypeID = questionTypeID;
    }

    public String getTotalAsk() {
        return TotalAsk;
    }

    public void setTotalAsk(String totalAsk) {
        TotalAsk = totalAsk;
    }

    public String getToAnswer() {
        return ToAnswer;
    }

    public void setToAnswer(String toAnswer) {
        ToAnswer = toAnswer;
    }

    public String getTotalMark() {
        return TotalMark;
    }

    public void setTotalMark(String totalMark) {
        TotalMark = totalMark;
    }

    public boolean isHeader = false;
    public boolean isSolution=false;
    public String Question = "", Answer = "", MQuestionID, QuestionType, QuestionTypeID, TotalAsk, ToAnswer, TotalMark;
}
