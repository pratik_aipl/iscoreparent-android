package com.iscore.parent.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Subject implements Serializable {

    public String SubjectID = "";
    public String SubjectName = "";

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }
}
