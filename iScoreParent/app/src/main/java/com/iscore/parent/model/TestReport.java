package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 11/4/2017.
 */

public class TestReport implements Serializable {
    public String SubjectID = "";
    public String SubjectName = "";
    public String TotalTest = "";

    public String getTotalTest() {
        return TotalTest;
    }

    public void setTotalTest(String totalTest) {
        TotalTest = totalTest;
    }


    public String Ready = "";
    public String SetPaper = "";

    public String getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(String subjectID) {
        SubjectID = subjectID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getReady() {
        return Ready;
    }

    public void setReady(String ready) {
        Ready = ready;
    }

    public String getSetPaper() {
        return SetPaper;
    }

    public void setSetPaper(String setPaper) {
        SetPaper = setPaper;
    }

    public String getPrelim() {
        return Prelim;
    }

    public void setPrelim(String prelim) {
        Prelim = prelim;
    }

    public String Prelim = "";
}