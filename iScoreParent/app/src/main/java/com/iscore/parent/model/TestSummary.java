package com.iscore.parent.model;

/**
 * Created by empiere-vaibhav on 1/4/2018.
 */

public class TestSummary {

    public String queName;

    public String getQueName() {
        return queName;
    }

    public void setQueName(String queName) {
        this.queName = queName;
    }

    public TestSummary() {

    }

    public TestSummary(String queName) {
        this.queName = queName;

    }
}
