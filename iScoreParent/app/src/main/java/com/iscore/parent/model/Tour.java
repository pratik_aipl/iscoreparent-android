package com.iscore.parent.model;

import com.iscore.parent.R;

public enum Tour {

    REPORTS(R.string.tour_report, R.layout.activity_tour_report),
    INSTITUTE(R.string.tour_institute, R.layout.activity_tour_institute),
    MCQ(R.string.tour_mcq, R.layout.activity_tour_mcq);

    private int mTitleResId;
    private int mLayoutResId;

    Tour(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}