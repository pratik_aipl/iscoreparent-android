package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 5/4/2018.
 */

public class WeeklyReport implements Serializable {
    public String Name = "";
    public String start_date = "";

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String end_date = "";
}
