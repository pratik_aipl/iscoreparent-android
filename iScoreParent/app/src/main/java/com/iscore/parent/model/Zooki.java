package com.iscore.parent.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 12/22/2017.
 */

public class Zooki implements Serializable {
    public int ZookiID = -1;

    public int getZookiID() {
        return ZookiID;
    }

    public void setZookiID(int zookiID) {
        ZookiID = zookiID;
    }

    public String Title = "";
    public String Desc = "";

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String image = "";
    public String CreatedOn = "";

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }


    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }
}
