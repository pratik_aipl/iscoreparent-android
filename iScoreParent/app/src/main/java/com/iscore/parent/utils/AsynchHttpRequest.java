package com.iscore.parent.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.iscore.parent.interfaceparent.AsynchTaskListner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 10/11/2016.
 */
public class AsynchHttpRequest {
    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsynchTaskListner aListner;
    public Constant.POST_TYPE post_type;

    public AsynchHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ft.getActivity());
            } else {
                this.map.remove("show");
            }

            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            if (!map.containsKey("show")) {
                Utils.showToast("No Internet, Please try again later", ft.getActivity());
            }
        }
    }

    public Context ct;

    public AsynchHttpRequest(Context ct, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ct;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ct;
        this.post_type = post_type;
        if (Utils.isNetworkAvailable(ct)) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ct);
            } else {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            if (!map.containsKey("show")) {
                Utils.showToast("No Internet, Please try again later", ct);
            }
        }
    }

    class MyRequest extends AsyncTask<Map<String, String>, Void, String> {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));
            String responseBody = "";
            try {
                switch (post_type) {
                    case GET:
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        map[0].remove("url");
                        List<String> values = new ArrayList<String>(map[0].values());
                        List<String> keys = new ArrayList<String>(map[0].keySet());
                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();
                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i).replace(" ", "%20");
                            ;
                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }
                        System.out.println("URL" + "====>" + query);
                        HttpGet httpGet = new HttpGet(query);
                        HttpResponse httpResponse = httpClient.execute(httpGet);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                        System.out.println("Responce" + "====>" + responseBody);
                        return responseBody;

                    case POST:
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        HttpPost postRequest = new HttpPost(map[0].get("url"));
                        DefaultHttpClient httpclient = new DefaultHttpClient();
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key).replace(" ", "%20")));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        HttpResponse response = httpclient.execute(postRequest);
                        httpEntity = response.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                        return responseBody;
                    case POST_WITH_IMAGE:
                        httpClient = new DefaultHttpClient();
                        postRequest = new HttpPost(map[0].get("url"));
                        reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });
                        BasicHttpContext localContext = new BasicHttpContext();
                        for (String key : map[0].keySet()) {
                            System.out.println();
                            System.out.println(key + "====>" + map[0].get(key));
                            if (key.equals("image")) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "image/png";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart("image", fbody);
                                    }
                                }
                            } else {
                                try {
                                    Log.i("Sanjeevani", " :  Key " + key);
                                    Log.i("Sanjeevani", " :  value " + map[0].get(key));
                                    reqEntity.addPart(key, new StringBody(map[0].get(key).replace(" ", "%20")));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                }
                            }
                        }
                        postRequest.setEntity(reqEntity);
                        HttpResponse responses = httpClient.execute(postRequest, localContext);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                "UTF-8"));
                        String sResponse;
                        while ((sResponse = reader.readLine()) != null) {
                            responseBody = responseBody + sResponse;
                        }
                        System.out.println("Responce ::" + responseBody);
                        return responseBody;

                    case POST_WITH_JSON:
                        String result = "";
                        httpClient = new DefaultHttpClient();
                        HttpPost httpPost = new HttpPost(map[0].get("url"));
                        nameValuePairs = new ArrayList<NameValuePair>();
                        nameValuePairs.add(new BasicNameValuePair("StudID", map[0].get("StudID")));
                        nameValuePairs.add(new BasicNameValuePair("action", map[0].get("action")));
                        nameValuePairs.add(new BasicNameValuePair("data", map[0].get("json")));
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                        httpResponse = httpClient.execute(httpPost);
                        httpEntity = httpResponse.getEntity();
                        for (String key : map[0].keySet()) {
                            Log.i("ISCORE", " :  Key " + key);
                            Log.i("ISCORE", " :  value " + map[0].get(key));

                        }
                        result = EntityUtils.toString(httpEntity, "UTF-8");
                        System.out.println("Responce ::" + result);

                        return result;

                    case Eval_POST_WITH_JSON:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            aListner.onTaskCompleted(result, request);
            super.onPostExecute(result);
        }
    }

    private static String getUrlContents(String theUrl) {
        StringBuilder content = new StringBuilder();

        // many of these calls can throw exceptions, so i've just
        // wrapped them all in one try/catch statement.
        try {
            // create a url object
            URL url = new URL(theUrl);

            // create a urlconnection object
            URLConnection urlConnection = url.openConnection();

            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;

            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
