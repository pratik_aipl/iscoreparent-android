package com.iscore.parent.utils;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.iscore.parent.App;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 10/11/2016.
 */
public class CallRequest {

    public static String OLD_URL = Constant.BASIC_URL + Constant.URL_VERSION + "web_services/";
    public static String URL = Constant.BASIC_URL + Constant.URL_VERSION + "parent_web_services/";
    public static String VIEW_PAPER_EVALUTER_URL = Constant.BASIC_VIEW_PAPER_EVA_URL + "class-admin/web-services-paper/";
    public static String IURL = Constant.BASIC_URL + Constant.URL_VERSION + "evaluator_web_services/";


    public App app;
    public Context ct;
    public Fragment ft;

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }


    //------------------  Login,  Register, OTP   Start------------------//

    public void getLogin(String register_mobile, String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "parent_login?");
        map.put("register_mobile", register_mobile);
        map.put("player_id", PleyaerID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getLogin, Constant.POST_TYPE.POST, map);
    }

    public void confirmOtp(String otp, String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "confirm_otp?");
        map.put("otp", otp);
        map.put("player_id", PleyaerID);
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("PLID", App.mainUser.getPLID());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.confirmOtp, Constant.POST_TYPE.POST, map);
    }

    public void resend_otp(String mobile) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "resend_otp?");
        map.put("mobile_number", mobile);
        map.put("PLID", App.mainUser.getPLID());

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.resend_otp, Constant.POST_TYPE.POST, map);
    }

    public void get_recent_mcq_paper() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_cct_paper?");
        map.put("StudID", App.mainUser.getStudent_id());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_recent_mcq_paper, Constant.POST_TYPE.POST, map);
    }

    public void getStudentParentData() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "student_report_data?");
        map.put("StudID", App.mainUser.getStudent_id());

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.getStudentParentData, Constant.POST_TYPE.POST, map);
    }

    public void get_weekly_digest_report() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "get_weekly_digest_report?");
        map.put("StudID", App.mainUser.getStudent_id());

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_weekly_digest_report, Constant.POST_TYPE.POST, map);
    }

    public void querySendMail(String from, String QueryMessage) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", OLD_URL + "query_message");
        map.put("from", from);
        map.put("QueryMessage", QueryMessage);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.qurySendMail, Constant.POST_TYPE.POST, map);

    }


    //--------------------- Data Sync And Profile Upadate  Start -----------------------//


    public void sendMcqDataLogin(String McqDtl) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "mcq");
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("json", McqDtl);
        map.put("show", "");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.sendMcqData, Constant.POST_TYPE.POST_WITH_JSON, map);
    }


    public void upadateProfile(String firstname, String lastname, String target, String previousTarget, String profile_image, String PleyaerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", URL + "datasync?");
        map.put("action", "UpdateProfile");
        map.put("Firstname", firstname);
        map.put("Lastname", lastname);
        map.put("SetTarget", target);
        map.put("PreviousYearScore", previousTarget);
        map.put("image", profile_image);

        map.put("StudID", App.mainUser.getStudent_id());
        map.put("player_id", PleyaerID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.upadateProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void get_zooki() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", OLD_URL + "get-zooki?");
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("show", "");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_zooki, Constant.POST_TYPE.GET, map);
    }
    public void get_subject() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", OLD_URL + "get_subjects?");
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("show", "");
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_subject, Constant.POST_TYPE.GET, map);
    }

    //--------------------- Data Sync And Profile Upadate  End  -----------------------//


    ////--------------------------- Evaluter Connect   Start -------------------------//


    ////-------------- CCT STart  ---------//


    public void get_mcq_test_result_activity(String TakenTest) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get-mcq-test-result?");
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("PaperID", TakenTest);


        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_mcq_test_result, Constant.POST_TYPE.GET, map);

    }

    public void mcq_view_summary(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "mcq_view_summary?");
        map.put("id", id);
        map.put("StudID", App.mainUser.getStudent_id());

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.mcq_view_summary, Constant.POST_TYPE.GET, map);

    }

    ////-------------- CCT END   ---------//


    ////-------------- Practies paper STart  ---------//


    public void view_paper_activity(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", VIEW_PAPER_EVALUTER_URL + "view-paper?");
        map.put("id", id);


        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.view_paper, Constant.POST_TYPE.GET, map);

    }

    public void view_model_answer(String id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", VIEW_PAPER_EVALUTER_URL + "view-model-answer?");
        map.put("id", id);

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.view_model_answer, Constant.POST_TYPE.GET, map);

    }

    ////-------------- Practies paper End  ---------//


    public void get_recent_question_paper_activity() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get-recent-question-paper?");
        map.put("StudID", App.mainUser.getStudent_id());


        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_recent_question_paper, Constant.POST_TYPE.GET, map);

    }


    public void get_single_question_paper(String PaperID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", IURL + "get_single_question_paper/?");
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("PaperID", PaperID);

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_single_question_paper, Constant.POST_TYPE.GET, map);

    }


    ////--------------------------- Evaluter Connect   END -------------------------//


    //------------  Board, Moderate, And  Sure-Shot  Start-----------------//


    public void get_notice_board() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", OLD_URL + "get_notice_board?");
        map.put("StudID", App.mainUser.getStudent_id());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_notice_board, Constant.POST_TYPE.GET, map);
    }


    //------------  Board, Moderate, And  Sure-Shot  End-----------------//


    ////--------------------------- Library Start-------------------------//

    public void get_library_parent_list() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", OLD_URL + "get_library_parent_list?");
        map.put("StudID", App.mainUser.getStudent_id());
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_library_parent_list, Constant.POST_TYPE.GET, map);
    }

    public void get_library_chapter_list(String FolderID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", OLD_URL + "get_library_chapter_list?");
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("FolderID", FolderID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_library_chapter_list, Constant.POST_TYPE.GET, map);
    }

    public void get_system_library_files(String FolderID, String FileType) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", OLD_URL + "get_system_library_files?");
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("FolderID", FolderID);
        map.put("FileType", FileType);

        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_system_library_files, Constant.POST_TYPE.GET, map);
    }

    public void get_personal_library_sub_folder_files_list(String FolderID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", OLD_URL + "get_personal_library_sub_folder_files_list?");
        map.put("StudID", App.mainUser.getStudent_id());
        map.put("FolderID", FolderID);
        new MySecureAsynchHttpRequest(ct, Constant.REQUESTS.get_personal_library_sub_folder_files_list, Constant.POST_TYPE.GET, map);
    }

    ////--------------------------- Library End-------------------------//


}
