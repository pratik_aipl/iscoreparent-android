package com.iscore.parent.utils;

import android.os.Environment;

/**
 * Created by admin on 10/12/2016.
 */
public class Constant {
    public static int EXTERNAL_STORAGE_PERMISSION = 9999;
    public static int READ_PHONE_STATE = 568;
    public static String PREFRENCE = "shared_pref";

    public static final String CONNECT_TO_WIFI = "WIFI";
    public static final String CONNECT_TO_MOBILE = "MOBILE";
    public static final String NOT_CONNECT = "NOT_CONNECT";
    public final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    public static String LOCAL_IMAGE_PATH = Environment.getExternalStorageDirectory() + "/.iScore";
    public static String LOCAL_Library_PATH = Environment.getExternalStorageDirectory() + "/Download";
    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;


    //------------------- PAYTM TEST Cradantial--------------------------------------
    /*
    public static final String M_ID = "PARSHV66654838019682"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail"; //Paytm industry type got it in paytm credential
    public static final String WEBSITE = "APPSTAGING";
    public static final String CALLBACK_URL = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
    public static String TXN_STATUS = "https://securegw-stage.paytm.in/merchant-status/getTxnStatus?";
*/

    //------------------- PAYTM LIVE Cradantial--------------------------------------

    public static final String M_ID = "Parshv15070673965035"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail109"; //Paytm industry type got it in paytm credential
    public static final String WEBSITE = "APPPROD";
    public static final String CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback.jsp";
    public static String TXN_STATUS_URL = "https://securegw.paytm.in/merchant-status/getTxnStatus?";

    //------------------- LIVE SERVER--------------------------------------

    public static String BASIC_URL = "https://staff.parshvaa.com/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL = "https://evaluater.parshvaa.com/";
    public static String URL_VERSION = "version_58/";

    //------------------- PEMP SERVER--------------------------------------
  /*  public static String BASIC_URL = "https://test.pemiscore.parshvaa.com/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL = "https://test.pemevaluater.parshvaa.com/";
    public static String URL_VERSION = "version_39/";*/
    //------------------- TEST SERVER--------------------------------------
    /*
     public static String BASIC_URL = "https://test.escore.parshvaa.com/web_services/";
    public static String BASIC_VIEW_PAPER_EVA_URL = "https://test.evaluator.parshvaa.com/";
    public static String URL_VERSION = "version_39/";
    */

    public static String EVEL_LIB_SYSTEM_IMAGE_PATH = BASIC_VIEW_PAPER_EVA_URL+"library_system/";

    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE, POST_WITH_JSON, Eval_POST_WITH_JSON;
    }


    public enum REQUESTS {
        getCountry, getOldConsultation, getImages,
        getCard, getCategoryWaitTime, getDBfile, getWaitTime, reschedule, forgot_password, saveSchedule, getSlots, addMinutes, saveCard, getLogin, getCategory, getBrands, register, getCallHistory, getScheduleCallHistory, editProfile, getUserProfile, getMinutes, rateFeedback,
        setCall, getCallToken, startCall, endCall, paymentTransaction, getTechnician, registerDevice, getState, getQulification, getDoctorEdit, getDoctor, getStatePin, getPatientEditProfile, getSpecialist, updateToken, updateDoctorToken, updatePatientToken, getDepartment, verifyOTP, getTimeSlot, changeLiveStatus, getChemist, getFreeDay, CreateConsultation, connectCall, setPrescription, viewEarning, getPetient, forgot_pw, sendOTP, resendOTP, changePass, callStatus, webLogin, getSpinner,
        getBoard, getMedium, getStandeard, getSubject, getBoardPaperFIle, getBoardsPaperTable, getPackage,


        notification, getSubjectTable, getChapterTable, getMcqQuetions, getPaper_Type, getExamtype_subject, getExamtypes, getExamTypePattern, getExamTypePatternDetail, getMasterQuestionTable, getQuesTypeTable, getMcqOptions, sendMcqData, sendGanreatPaper, sendBoardPaperDownload, upadateProfile, qurySendMail, SendMcqData,
        getTableRecords, get_paper, get_subject_mcq_pape, check_app_login, UpdateTokan, getSilentLogin, evaluator_connect, view_paper, get_evaluator_subjects, mcq_view_paper, view_model_answer, submit_mcq_test, get_recent_mcq_paper, get_mcq_test_result, get_recent_question_paper, evaluator_register, mcq_view_summary, getCctReport, send_otp, get_updated_masterquetion, checkKey, direct_register, get_subject_board, get_boardpaper, get_single_question_paper, get_zooki,get_subject, get_sure_shot, get_moderatepaper, get_moderatepaper_subject, get_notice_board, get_library_parent_list, get_library_chapter_list, get_system_library_files, get_personal_library_sub_folder_files_list, check_login_token, sendIncorrectQuestion, sendNotAppearedQuestion, change_class_key, get_student_class_dtl, resend_otp, update_last_sync_time, subscribe, verify_bord_medium_standard, getTxnStatus, confirmOtp, getStudentParentData, weekly_digest_report_dtl, get_weekly_digest_report;
    }

    public static int SUBJECT_TABLE = 2;
    public static int CHAPTER_TABLE = 3;

    /**
     * This is for PUSH Notification
     **/

    public static final String SENDER_ID = "";
    public static final String DISPLAY_ACTION = "com.markteq.wms";

    public static final int TWITTER_LOGIN_REQUEST_CODE = 1;
}
