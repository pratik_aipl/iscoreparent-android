package com.iscore.parent.utils;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;


import com.iscore.parent.db.MyDBManager;

import java.util.ArrayList;

/**
 * Created by Karan - Empiere on 1/8/2018.
 */

public class CustomDatabaseQuery extends AsyncTask<String, ArrayList<Object>, ArrayList<Object>> {

    public MyDBManager mDb;
    public CursorParserUniversal cParse;

    public Context ct;
    public Object obj;

    public CustomDatabaseQuery(Context ct, Object obj) {
        this.ct = ct;
        this.obj = obj;

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDb = MyDBManager.getInstance(ct);
        mDb.open(ct);
        this.cParse = new CursorParserUniversal();


    }

    @Override
    protected ArrayList<Object> doInBackground(String... id) {
       // Log.i("Total Rows", " : " + id[0]);
        Cursor c = mDb.getAllRows(id[0]);


       // System.out.println("Total Rows : " + id[0]);

        ArrayList<Object> objArray = new ArrayList<>();

        if (c != null && c.moveToFirst()) {
            do {

               // System.out.println("Total Rows on onPreExecute class");

                Class cl;
                try {
                    cl = Class.forName(obj.getClass().getCanonicalName(), false, obj.getClass().getClassLoader());
                    obj = cl.newInstance();
                    obj = (Object) cParse.parseCursor(c, obj);

                    objArray.add(obj);

                } catch (Exception e) {
                    e.printStackTrace();

                }

            } while (c.moveToNext());

           // Log.v("Cursor Object", DatabaseUtils.dumpCursorToString(c));
            c.close();
        } else {

          //  Log.i("Total Rows", " : Not any SubjectAccuracy Found from");
            //  Utils.showToast("Not any SubjectAccuracy Found from : ", ct);
        }

        return objArray;

    }

    @Override
    protected void onPostExecute(ArrayList<Object> objects) {

        super.onPostExecute(objects);
    }


    public ArrayList<Object> executeOnExecutor(String[] strings) {
        ArrayList<Object> objArray = new ArrayList<>();

        return objArray;
    }
}