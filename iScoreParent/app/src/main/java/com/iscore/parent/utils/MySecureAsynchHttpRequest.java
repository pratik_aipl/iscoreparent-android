package com.iscore.parent.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.iscore.parent.interfaceparent.AsynchTaskListner;
import com.iscore.parent.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Sagar Sojitra on 3/8/2016.
 */
public class MySecureAsynchHttpRequest {

    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsynchTaskListner aListner;
    public Constant.POST_TYPE post_type;

    public MySecureAsynchHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ft.getActivity());
            } else {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast(ft.getResources().getString(R.string.conect_internet), ft.getActivity());
        }
    }

    public Context ct;

    public MySecureAsynchHttpRequest(Context ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ct)) {
            if (!map.containsKey("show")) {

                Utils.showProgressDialog(ct);
            } else {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showToast(ct.getResources().getString(R.string.conect_internet), ct);
        }
    }


    class MyRequest extends AsyncTask<Map<String, String>, Void, String>

    {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));

            String responseBody = "";
            try {
                switch (post_type) {
                    case GET:
                        DefaultHttpClient httpClient = getNewHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        map[0].remove("url");


                        List<String> values = new ArrayList<String>(map[0].values());


                        List<String> keys = new ArrayList<String>(map[0].keySet());

                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();

                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i).replace(" ", "%20");

                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }
                       // URLEncoder.encode(query, "UTF-8");
                        System.out.println("URL" + "====>" +query);
                        HttpGet httpGet = new HttpGet(query);

                        HttpResponse httpResponse = httpClient.execute(httpGet);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);

                        System.out.println("StatusCode" + "====>" + httpResponse.getStatusLine().getStatusCode());
                        System.out.println("Responce" + "====>" + responseBody);
                        return responseBody;

                    case POST:
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        HttpPost postRequest = new HttpPost(map[0].get("url"));
                        DefaultHttpClient httpclient = getNewHttpClient();
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        HttpResponse response = httpclient.execute(postRequest);
                        httpEntity = response.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                        System.out.println("Responce" + "====>" + responseBody);


                        return responseBody;
                    case POST_WITH_IMAGE:
                        httpclient = getNewHttpClient();
                        postRequest = new HttpPost(map[0].get("url"));

                        map[0].remove("url");
                        reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });
                        BasicHttpContext localContext = new BasicHttpContext();
                        for (String key : map[0].keySet()) {
                            System.out.println();
                            System.out.println(key + "====>" + map[0].get(key));
                            if (key.equals("image")) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "image/png";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart("image", fbody);

                                    }
                                }
                            } else {
                                try {
                                    reqEntity.addPart(key, new StringBody(map[0].get(key).replace(" ", "%20")));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                }
                            }

                        }
                        postRequest.setEntity(reqEntity);
                        HttpResponse responses = httpclient.execute(postRequest, localContext);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                "UTF-8"));
                        String sResponse;
                        while ((sResponse = reader.readLine()) != null) {
                            responseBody = responseBody + sResponse;
                        }
                        System.out.println("Responce ::" + responseBody);
                        return responseBody;


                   /* case POST_WITH_IMAGE:
                        httpClient = getNewHttpClient();
                        postRequest = new HttpPost(map[0].get("url"));

                        BasicHttpContext localContext = new BasicHttpContext();
                        for (String key : map[0].keySet()) {
                            System.out.println();
                            System.out.println(key + "====>" + map[0].get(key));
                            if (key.equals("image")) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "image/png";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart("image", fbody);
                                    }
                                }
                            } else {
                                try {
                                      reqEntity.addPart(key, new StringBody(map[0].get(key).replace(" ", "%20")));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                }
                               // reqEntity.addPart(key, new StringBody(map[0].get(key)));
                            }
                        }
                        postRequest.setEntity(reqEntity);
                        HttpResponse responses = httpClient.execute(postRequest, localContext);

                        BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                "UTF-8"));

                        String sResponse;

                        while ((sResponse = reader.readLine()) != null) {
                            responseBody = responseBody + sResponse;
                        }

                        return responseBody;
                */
                    case Eval_POST_WITH_JSON:
                        String results = "";
                        httpClient = getNewHttpClient();
                        HttpPost httpPost = new HttpPost(map[0].get("url"));

                        nameValuePairs = new ArrayList<NameValuePair>();

                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                        //System.out.println("Final URI::" + httpPost.getEntity().toString());
                        response = httpClient.execute(httpPost);
                        httpEntity = response.getEntity();
                        results = EntityUtils.toString(httpEntity, "UTF-8");
                        System.out.println("Responce" + "====>" + results);
                        return results;

                    case POST_WITH_JSON:
                        String result = "";
                        httpClient = getNewHttpClient();
                        httpPost = new HttpPost(map[0].get("url"));
                        nameValuePairs = new ArrayList<NameValuePair>();
                        nameValuePairs.add(new BasicNameValuePair("StudID", map[0].get("StudID")));
                        nameValuePairs.add(new BasicNameValuePair("action", map[0].get("action")));
                        nameValuePairs.add(new BasicNameValuePair("data", map[0].get("json")));
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                        httpResponse = httpClient.execute(httpPost);
                        httpEntity = httpResponse.getEntity();

                        Log.i("ISCORE", " :  StudID " + map[0].get("StudID"));
                        Log.i("ISCORE", " :  action " + map[0].get("action"));
                        // Log.i("ISCORE", " :  data " + map[0].get("json"));


                        result = EntityUtils.toString(httpEntity, "UTF-8");
                        System.out.println("Responce ::" + result);


                        return result;
                }


            } catch (Exception e) {
                Utils.hideProgressDialog();


                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String result) {


            aListner.onTaskCompleted(result, request);
            super.onPostExecute(result);
        }
    }

    public DefaultHttpClient HostVerifier() {
        HostnameVerifier hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

        DefaultHttpClient client = new DefaultHttpClient();

        SchemeRegistry registry = new SchemeRegistry();
        SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
        socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
        registry.register(new Scheme("https", socketFactory, 443));
        SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
        DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

// Set verifier
        HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
        return httpClient;
    }

    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
}
