package com.iscore.parent.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.iscore.parent.App;

import java.util.Map;


/**
 * Created by Karan - Empiere on 3/30/2017.
 */

public class MySharedPref {
    public App app;
    SharedPreferences shared;
    SharedPreferences.Editor et;
    public final String LAST_MCQ_SYNC = "last_mcq_sync";
    public final String LAST_GeneratePaper_SYNC = "last_generateaper_sync";
    public final String first_name = "first_name";
    public final String last_name = "last_name";
    public final String Target = "Target";
    public final String PreviousTarget = "PreviousTarget";

    public final String student_id = "student_id";
    public final String reg_key = "reg_key";
    public final String prof_image = "prof_image";
    public final String LAST_BoardPaper_SYNC = "last_BoardPaper_sync";
    public final String IMEINo = "Guid";
    public final String ClassID = "ClassID";
    public final String BoardID = "BoardID";
    public final String MediumID = "MediumID";
    public final String StandardID = "StandardID";
    public final String LastSyncTime = "LastSyncTime";
    public final String StandardName = "StandardName";
    public final String EmailID = "EmailID";
    public final String RegMobNo = "RegMobNo";
    public final String subjects = "subjects";
    public final String isFirst = "isFirst";

    public final String isLoggedIn = "isLoggedIn";
    public final String ClassName = "ClassName";
    public final String KeyStatus = "KeyStatus";
    public final String BranchID = "BranchID";
    public final String BatchID = "BatchID";
    public final String ExpiryDate = "expiry";
    public final String Institutename = "Institutename";
    public final String Guuid = "Guuid";
    public final String downloadStatus = "downloadStatus";
    public final String VersioCode = "versioncode";

    public final String ISEvaluter = "evaluter";
    public final String Version = "version";
    public final String EvaKey = "evakey";
    public final String DeviceID = "deviceid";
    public final String SchoolName = "schoolName";
    public final String BoardName = "BoardName";
    public final String MediumName = "MediumName";
    public final String ClassLogo = "ClassLogo";
    public final String ClassRoundLogo = "ClassRoundLogo";
    public final String StudentCode = "StudentCode";
    public final String CreatedOn = "CreatedOn";
    public int BadgeCount = 0;


    public String getStudentCode() {
        return shared.contains(StudentCode) ? shared.getString(StudentCode, "") : "";
    }

    public void setCreatedOn(String createdOn) {
        et.putString(CreatedOn, createdOn);
        et.commit();
    }

    public String getCreatedOn() {
        return shared.contains(CreatedOn) ? shared.getString(CreatedOn, "") : "";
    }

    public void setStudentCode(String studentCode) {
        et.putString(StudentCode, studentCode);
        et.commit();
    }

    public String getBoardName() {
        return shared.contains(BoardName) ? shared.getString(BoardName, "") : "";
    }

    public void setBoardName(String boardName) {
        et.putString(BoardName, boardName);
        et.commit();
    }

    public String getMediumName() {
        return shared.contains(MediumName) ? shared.getString(MediumName, "") : "";
    }

    public void setMediumName(String mediumName) {
        et.putString(MediumName, mediumName);
        et.commit();
    }

    public void clearApp() {
        et.clear();
        et.apply();
        et.commit();
    }

    public String getClassLogo() {
        return shared.contains(ClassLogo) ? shared.getString(ClassLogo, "") : "";
    }

    public void setClassLogo(String classLogo) {
        et.putString(ClassLogo, classLogo);
        et.commit();
    }

    public String getClassRoundLogo() {
        return shared.contains(ClassRoundLogo) ? shared.getString(ClassRoundLogo, "") : "";
    }

    public void setClassRoundLogo(String classRoundLogo) {
        et.putString(ClassRoundLogo, classRoundLogo);
        et.commit();
    }

    public int getBadgeCount() {
        return shared.contains(String.valueOf(BadgeCount)) ? shared.getInt(String.valueOf(BadgeCount), 0) : 0;
    }

    public void setBadgeCount(int badgeCount) {
        et.putInt(String.valueOf(BadgeCount), badgeCount);
        et.commit();
    }

    public String getDeviceID() {
        return shared.contains(DeviceID) ? shared.getString(DeviceID, "") : "";
    }

    public void setDeviceID(String deviceid) {
        et.putString(DeviceID, deviceid);
        et.commit();
    }

    public String getEvaKey() {
        return shared.contains(EvaKey) ? shared.getString(EvaKey, "") : "";
    }

    public void setEvaKey(String evaluter) {
        et.putString(EvaKey, evaluter);
        et.commit();
    }

    public String getISEvaluter() {
        return shared.contains(ISEvaluter) ? shared.getString(ISEvaluter, "") : "";
    }

    public void setISEvaluter(String evaluter) {
        et.putString(ISEvaluter, evaluter);
        et.commit();
    }

    public String getVersion() {
        return shared.contains(Version) ? shared.getString(Version, "") : "";
    }

    public void setVersion(String version) {
        et.putString(Version, version);
        et.commit();
    }

    public String getVersioCode() {
        return shared.contains(VersioCode) ? shared.getString(VersioCode, "") : "";
    }

    public void setVersioCode(String versioncode) {
        et.putString(VersioCode, versioncode);
        et.commit();
    }

    public String getGuuid() {
        return shared.contains(Guuid) ? shared.getString(Guuid, "") : "";
    }

    public void setGuuid(String guuid) {
        et.putString(Guuid, guuid);
        et.commit();
    }

    public String getKeyStatus() {
        return shared.contains(KeyStatus) ? shared.getString(KeyStatus, "") : "";
    }

    public void setKeyStatus(String keyStatus) {
        et.putString(KeyStatus, keyStatus);
        et.commit();
    }

    public String getDownloadStatus() {
        return shared.contains(downloadStatus) ? shared.getString(downloadStatus, "") : "";
    }

    public void setDownloadStatus(String ownloadStatus) {
        et.putString(downloadStatus, ownloadStatus);
        et.commit();
    }

    public String getBranchID() {
        return shared.contains(BranchID) ? shared.getString(BranchID, "") : "";
    }

    public void setBranchID(String branchID) {
        et.putString(BranchID, branchID);
        et.commit();
    }

    public String getBatchID() {
        return shared.contains(BatchID) ? shared.getString(BatchID, "") : "";
    }


    public void setLastSyncTime(String syncTime) {
        et.putString(LastSyncTime, syncTime);
        et.commit();
    }

    public String getLastSyncTime() {
        return shared.contains(LastSyncTime) ? shared.getString(LastSyncTime, "") : "";
    }


    public void setExpiryDate(String expiry) {
        et.putString(ExpiryDate, expiry);
        et.commit();
    }

    public String getExpiryDate() {
        return shared.contains(ExpiryDate) ? shared.getString(ExpiryDate, "") : "";
    }

    public void setBatchID(String batchID) {
        et.putString(BatchID, batchID);
        et.commit();
    }

    public String getSchoolName() {
        return shared.contains(Institutename) ? shared.getString(Institutename, "") : "";
    }

    public void setInstitutename(String institutename) {
        et.putString(Institutename, institutename);
        et.commit();
    }


    public boolean getIsFirstTime() {
        return shared.contains(isFirst) ? shared.getBoolean(isFirst, false) : false;
    }

    public void setIsFirstTime(boolean isfirst) {
        et.putBoolean(isFirst, isfirst);
        et.commit();
    }


    public String getlblTarget() {
        return shared.contains(Target) ? shared.getString(Target, "") : "";
    }

    public void setlblTarget(String target) {
        et.putString(Target, target);
        et.commit();
    }

    public String getlblPrviousTarget() {
        return shared.contains(PreviousTarget) ? shared.getString(PreviousTarget, "") : "";
    }

    public void setlblPrviousTarget(String previousTarget) {
        et.putString(PreviousTarget, previousTarget);
        et.commit();
    }

    public boolean getIsLoggedIn() {
        return shared.contains(isLoggedIn) ? shared.getBoolean(isLoggedIn, false) : false;
    }

    public void setIsLoggedIn(boolean tf) {
        et.putBoolean(isLoggedIn, tf);
        et.commit();
    }

    public String getRegMobNo() {
        return shared.contains(RegMobNo) ? shared.getString(RegMobNo, "") : "";
    }

    public void setRegMobNo(String No) {
        et.putString(RegMobNo, No);
        et.commit();
    }

    public String getSubjects() {
        return shared.contains(subjects) ? shared.getString(subjects, "") : "";
    }

    public void setSubjects(String sub) {
        et.putString(subjects, sub);
        et.commit();
    }

    public String getEmailID() {
        return shared.contains(EmailID) ? shared.getString(EmailID, "") : "";
    }

    public void setEmailID(String email) {
        et.putString(EmailID, email);
        et.commit();
    }

    public String getStandardID() {
        return shared.contains(StandardID) ? shared.getString(StandardID, "") : "";
    }

    public void setStandardID(String std) {
        et.putString(StandardID, std);
        et.commit();
    }

    public String getStandardName() {
        return shared.contains(StandardName) ? shared.getString(StandardName, "") : "";
    }

    public void setStandardName(String stdName) {
        et.putString(StandardName, stdName);
        et.commit();
    }

    public String getClassName() {
        return shared.contains(ClassName) ? shared.getString(ClassName, "") : "";
    }

    public void setClassName(String stdName) {
        et.putString(ClassName, stdName);
        et.commit();
    }

    public String getMediumID() {
        return shared.contains(MediumID) ? shared.getString(MediumID, "") : "";
    }

    public void setMediumID(String mID) {
        et.putString(MediumID, mID);
        et.commit();
    }

    public String getBoardID() {
        return shared.contains(BoardID) ? shared.getString(BoardID, "") : "";
    }

    public void setBoardID(String bID) {
        et.putString(BoardID, bID);
        et.commit();
    }

    public String getClassID() {
        return shared.contains(ClassID) ? shared.getString(ClassID, "") : "";
    }

    public void setClassID(String cID) {
        et.putString(ClassID, cID);
        et.commit();
    }

    public String getIMEINo() {
        return shared.contains(IMEINo) ? shared.getString(IMEINo, "") : "";

    }

    public void setIMEINo(String No) {
        et.putString(IMEINo, No);
        et.commit();
    }


    public MySharedPref(Context ct) {

        shared = ct.getSharedPreferences(App.myPref, 0);
        et = shared.edit();
    }

    public void clearAll() {
        et.clear();
        et.commit();
    }

    public void setProfileImage(String img) {
        et.putString(prof_image, img);
        et.commit();
    }

    public void setFirst_name(String name) {
        et.putString(first_name, name);
        et.commit();
    }

    public void setLast_name(String name) {
        et.putString(last_name, name);
        et.commit();
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public SharedPreferences getShared() {
        return shared;
    }

    public void setShared(SharedPreferences shared) {
        this.shared = shared;
    }

    public SharedPreferences.Editor getEt() {
        return et;
    }

    public void setEt(SharedPreferences.Editor et) {
        this.et = et;
    }

    public String getLAST_MCQ_SYNC() {
        return LAST_MCQ_SYNC;
    }

    public String getLAST_GeneratePaper_SYNC() {
        return LAST_GeneratePaper_SYNC;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getStudent_id() {
        return student_id;
    }

    public String getLAST_BoardPaper_SYNC() {
        return LAST_BoardPaper_SYNC;
    }

    public String getIsFirst() {
        return isFirst;
    }

    public String getInstitutename() {
        return Institutename;
    }

    public void setStudent_id(String id) {
        et.putString(student_id, id);
        et.commit();

    }

    public void setReg_key(String key) {
        et.putString(reg_key, key);
        et.commit();
    }

    public void setTarget(String t) {
        et.putString(Target, t);
        et.commit();
    }

    public String getTarget() {
        return shared.contains(Target) ? shared.getString(Target, "") : "";
    }


    public String getProf_image() {
        return shared.contains(prof_image) ? shared.getString(prof_image, "") : "";
    }

    public String getFirstName() {
        return shared.contains(first_name) ? shared.getString(first_name, "") : "";
    }

    public String getLastName() {
        return shared.contains(last_name) ? shared.getString(last_name, "") : "";
    }

    public String getStudentID() {
        return shared.contains(student_id) ? shared.getString(student_id, "") : "";
    }

    public String getReg_key() {
        return shared.contains(reg_key) ? shared.getString(reg_key, "") : "";

    }

    public void setLastMCQlastSync(String sync) {
        et.putString(LAST_MCQ_SYNC, sync);
        et.commit();
    }


    public String getLastMCQSyncTime() {
        return shared.contains(LAST_MCQ_SYNC) ? shared.getString(LAST_MCQ_SYNC, "") : "";
    }


    public void setLastGeneratePaperlastSync(String sync) {
        et.putString(LAST_GeneratePaper_SYNC, sync);
        et.commit();
    }

    public String getLastGeneratePaperSyncTime() {
        return shared.contains(LAST_GeneratePaper_SYNC) ? shared.getString(LAST_GeneratePaper_SYNC, "") : "";

    }

    public void setLastBoardPaperDownloadSyncTime(String sync) {
        et.putString(LAST_BoardPaper_SYNC, sync);
        et.commit();
    }

    public String getLastBoardPaperDownloadSyncTime() {
        return shared.contains(LAST_BoardPaper_SYNC) ? shared.getString(LAST_BoardPaper_SYNC, "") : "";
    }


    public void printAll(){

        Map<String,?> keys = shared.getAll();

        for(Map.Entry<String,?> entry : keys.entrySet()){
            Log.d("Sagar Shared  values",entry.getKey() + " :->  " +
                    entry.getValue().toString());
        }
    }
}
