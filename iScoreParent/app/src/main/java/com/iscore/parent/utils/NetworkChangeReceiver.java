package com.iscore.parent.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.iscore.parent.activity.DashBoardActivity;
import com.iscore.parent.App;

/**
 * Created by Karan - Empiere on 4/7/2017.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static final int NETWORK_STATUS_NOT_CONNECTED = 0, NETWORK_STAUS_WIFI = 1, NETWORK_STATUS_MOBILE = 2;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        int status = getConnectivityStatus(context);
        if (!"android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if (status == NETWORK_STATUS_NOT_CONNECTED) {

            }else{
                DashBoardActivity.getConnectivityStatusString(context);
            }
        } else {
            DashBoardActivity.getConnectivityStatusString(context);


        }
    }

    public static int getConnectivityStatus(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if (activeNetwork.getType() == TYPE_WIFI) {
                    Log.i("TAG ", "wifi DATA");
                    return TYPE_WIFI;
                }

                if (activeNetwork.getType() == TYPE_MOBILE) {
                    Log.i("TAG ", "MObiler DATA");
                    return TYPE_MOBILE;
                }
            } else {
                if (App.mySharedPref.getDownloadStatus().equals("n")) {
                }
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

        return TYPE_NOT_CONNECTED;
    }
}