package com.iscore.parent.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.iscore.parent.R;
import com.onesignal.OneSignal;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * Created by admin on 10/12/2016.
 */
public class Utils {
    /* for GCM */
    static final String DISPLAY_MESSAGE_ACTION = "com.maxinternational.max.DISPLAY_MESSAGE";
    static final String EXTRA_MESSAGE = "message";
    public static SimpleDateFormat Input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat outputAMPM = new SimpleDateFormat("dd MMM yyyy | hh:mm aa    ");
    public static SimpleDateFormat InputDate = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat output = new SimpleDateFormat("dd-MMM-yyyy");
    public static SimpleDateFormat outputDateFormate = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
    public static SimpleDateFormat outputDateFormatePMAM = new SimpleDateFormat("MMM dd, yyyy hh:mm aa");
    public static SimpleDateFormat simpleFormate = new SimpleDateFormat("yyyyMMdd");
    public static Dialog dialog;
    static float density = 1;
    private static ProgressDialog mProgressDialog;
    static String Pleyaer = "";

    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return Settings.System.getInt(c.getContentResolver(), Settings.System.AUTO_TIME, 0) == 1;
        }
    }

    public static String OneSignalPlearID() {

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    Pleyaer = userId;
                Log.d("debug", "User:" + userId);

                Log.d("debug", "registrationId:" + registrationId);

            }
        });
        return Pleyaer;
    }

    public static String replaceImagesPath(String data) {
        //http://staff.parshvaa.com/Uploads/10/MARATHI/Geometry/Geom_x_mm_1.png#-105#1
        File image = new File(Constant.LOCAL_IMAGE_PATH);
        if (data.contains("src=\"http://staff.parshvaa.com/Uploads/")) {
            data = data.replaceAll("src=\"http://staff.parshvaa.com/Uploads/", "src=\"file://" + image.getAbsolutePath().toString() + "/Uploads/");
            //    data=data.replaceAll("%20"," ");
        } else if (data.contains("src=\"/Uploads/")) {
            data = data.replaceAll("src=\"/Uploads/", "src=\"file://" + image.getAbsolutePath().toString() + "/Uploads/");
        } else {
            //data = data.replaceAll("<img alt=\"\" src=\"", "<img src=\"file://" + image.getAbsolutePath().toString());
        }

        return data;


    }

    public static String changeDateToMMDDYYYY(String input) {
        String outputDateStr = "";
        try {
            Date date = InputDate.parse(input);
            outputDateStr = output.format(date);
            //Log.i("output", outputDateStr);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }

    public static String changeDateAndTimeFormet(String input) {
        String outputDateStr = "";
        try {
            Date date = Input.parse(input);
            outputDateStr = outputDateFormatePMAM.format(date);
            //Log.i("output", outputDateStr);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }

    public static String changeDateAndTimeAMPMFormet(String input) {
        String outputDateStr = "";
        try {
            Date date = Input.parse(input);
            outputDateStr = outputAMPM.format(date);
            //Log.i("output", outputDateStr);
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return outputDateStr;
    }

    public static String DateFormetChangeUniversal(DateFormat inputFormat, DateFormat outputFormat, String input) {
        String outputDateStr;

        try {

            Date date = inputFormat.parse(input);

            outputDateStr = outputFormat.format(date);
            Log.i("output", outputDateStr);
            return outputDateStr;


        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static String changeDateToDDMMYYYY(String input) {
        //   21-05-1916
        //   1916-05-21//
      /*  Date date = new Date(input);
        SimpleDateFormat formatter5 = new SimpleDateFormat("yyyy-mm-dd");
        return formatter5.format(date);*/
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyy");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            // String input="2013-06-24";
            Date date = inputFormat.parse(input);

            outputDateStr = outputFormat.format(date);
            Log.i("output", outputDateStr);
            return outputDateStr;


        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }



    public static boolean checkOutDated() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date strDate = sdf.parse("31/03/2021");
            if (System.currentTimeMillis() > strDate.getTime()) {
                return false;
            } else {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }
    }


    public static boolean checkStoragePermission(Context ct) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(ct, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.i("iScore", "Permission granted");
                return true;
            } else {

                Log.i("iScore", "Permission revoked");
                ActivityCompat.requestPermissions((Activity) ct, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        Constant.EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.i("iScore", "Permission granted");
            return true;
        }
    }

    public static boolean checkPhoneStatePermission(Context ct) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(ct, Manifest.permission.READ_PHONE_STATE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.i("iScore", "Permission granted");
                return true;
            } else {

                Log.i("iScore", "Permission revoked");
                ActivityCompat.requestPermissions((Activity) ct, new String[]{Manifest.permission.READ_PHONE_STATE},
                        Constant.EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.i("iScore", "Permission granted");
            return true;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkWritePermission(Context ct) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(ct, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.i("iScore", "Permission granted");
                return true;
            } else {

                Log.i("iScore", "Permission revoked");
                ActivityCompat.requestPermissions((Activity) ct, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        Constant.READ_WRITE_EXTERNAL_STORAGE_CAMERA);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.i("iScore", "Permission granted");
            return true;
        }
    }

    public static String getValue(EditText et) {
        return et.getText().toString();
    }

    public static boolean isEmpty(EditText et) {
        return et.getText().toString().equalsIgnoreCase("");
    }

    public static void showProgressDialog(Context context, String msg) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.cancel();
            }
            if (dialog == null) {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);


                dialog.setContentView(R.layout.custom_progress_dialog);
                TextView txtView = (TextView) dialog.findViewById(R.id.txt_di);
                txtView.setText(msg);


            }


            if (!dialog.isShowing()) {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        dialog.show();
                    }
                };
                r.run();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showProgressDialog(Context context) {
        if (!((Activity) context).isFinishing()) {
            //show dialog

            showProgressDialog(context, "Please Wait..");
        }
    }

    public static void removeWorkingDialog(Context context) {
        if (!((Activity) context).isFinishing()) {

            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        }
    }

    public static void hideProgressDialog() {
        try {

            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            }

/*
            if (mProgressDialog != null && mProgressDialog.isShowing() ) {
                mProgressDialog.cancel();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }*/
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getUpperCase(String name) {
        StringBuilder sb = new StringBuilder(name); // one StringBuilder object
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString(); // one String object
    }


    public static void addValueToEditor(SharedPreferences.Editor et, Object o) {
        Class c;
        try {
            c = Class.forName(o.getClass().getCanonicalName(), false, o.getClass().getClassLoader());

            Field[] fields = c.getFields();
            for (Field f : fields) {
                f.setAccessible(true);
                if (f.getType() == String.class) {
                    et.putString(f.getName(), f.get(o).toString());

                } else if (f.getType() == Integer.class) {
                    et.putInt(f.getName(), Integer.parseInt(f.get(o).toString()));

                } else if (f.getType() == Boolean.class) {
                    et.putBoolean(f.getName(), Boolean.valueOf(f.get(o).toString()));
                }
            }

            et.commit();


        } catch (Exception e) {
            e.printStackTrace();


        }

    }

    public static String consvertNumberToString(String number) {
        String numberz = number;
        String returnz = "";
        try {
            final long Number = Long.parseLong(numberz);
            returnz = Words.convert(Number);
        } catch (NumberFormatException e) {
            //Toast.makeToast("illegal number or empty number" , toast.long)
        }
        return returnz;
    }

    public static void showCancelableSpinProgressDialog(Context context, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    public static void removeSimpleSpinProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.cancel();
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public static float getDisplayMetricsDensity(Context context) {
        density = context.getResources().getDisplayMetrics().density;

        return density;
    }

    public static int getPixel(Context context, int p) {
        if (density != 1) {
            return (int) (p * density + 0.5);
        }
        return p;
    }

    public static Animation FadeAnimation(float nFromFade, float nToFade) {
        Animation fadeAnimation = new AlphaAnimation(nToFade, nToFade);

        return fadeAnimation;
    }

    public static String getDeviceId(Context context) {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String uuid = tManager.getDeviceId();
        return uuid;
    }

    public static Animation inFromRightAnimation() {
        Animation inFromRight = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromRight;
    }

    public static Animation inFromLeftAnimation() {
        Animation inFromLeft = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromLeft;
    }

    public static Animation inFromBottomAnimation() {
        Animation inFromBottom = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, +1.0f, Animation.RELATIVE_TO_PARENT,
                0.0f);

        return inFromBottom;
    }

    public static Animation outToLeftAnimation() {
        Animation outToLeft = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                -1.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f);
        return outToLeft;
    }

    public static Animation outToRightAnimation() {
        Animation outToRight = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                +1.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f);

        return outToRight;
    }

    public static Animation outToBottomAnimation() {
        Animation outToBottom = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                +1.0f);

        return outToBottom;
    }

    public static String create_imgage_view(String imageURL, int width, int height) {
        StringBuffer html = new StringBuffer(
                "<html lang='en'><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>");
        html.append("<body style='width:" + width + "; height:" + height + "; padding:0; margin:0;'>");

        html.append("<div style='width:" + width + "; height:" + height + "; overflow:hidden;'>");
        html.append("<img src='" + imageURL + "' style='width:" + width + ";' border=0 />");
        html.append("</div>");

        html.append("</body>");
        html.append("</html>");

        return html.toString();
    }

    public static boolean checkWIFIConnected(Context activity) {
        ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return mWifi.isConnected();
    }

    public static boolean isNetworkAvailable(Context activity) {


        ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
            return true;
        } else {
            showToast("Please connect to Internet", activity);
            return false;
        }

    }

    public static boolean isOnline(Context Ct) {
        ConnectivityManager cm =
                (ConnectivityManager) Ct.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public static String convertSecondsToHMm(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%d:%02d", h, m);
    }

    public static String convertSecondsToHMmSs(long milliSeconds) {
        long seconds = milliSeconds / 1000;
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%d:%02d:%02d", h, m, s);
    }

    public static boolean eMailValidation(String emailstring) {
        if (null == emailstring || emailstring.length() == 0) {
            return false;
        }
        Pattern emailPattern = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher emailMatcher = emailPattern.matcher(emailstring);
        return emailMatcher.matches();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void showPleaseTryAgain(Context ctx) {
        Toast toast = Toast.makeText(ctx, "Please Try again Later", Toast.LENGTH_SHORT);
        // toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public static void showToast(String msg, Context ctx) {
        Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
       /* toast.setGravity(Gravity.CENTER, 0, 0);*/
        toast.show();

    }

    public static void showToastinCenter(String msg, Context ctx) {
        Toast toast = Toast.makeText(ctx,
                msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public static void showSnakBar(String msg, final View view, Context ctx) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ContextCompat.getColor(ctx, R.color.textColor));

        /*.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbar1 = Snackbar.make(ctx, "Message is restored!", Snackbar.LENGTH_SHORT);
                        snackbar1.show();
                    }
                });*/

        snackbar.show();
    }

    public static void showAlert(String msg, Context ctx) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(ctx);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    // public static String convert24HoursTo12HoursFormat(String twelveHourTime)
    // throws ParseException {
    // return outputformatter.format(inputformatter.parse(twelveHourTime));
    // }

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    /**
     * You can integrate interstitials in any placement in your app, but for
     * testing purposes we present integration tied to a button click
     */
    public static Typeface setNormalFontFace(Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSans.ttf");
        return font;
    }

    public static Typeface setBoldFontFace(Context context) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/DroidSans-Bold.ttf");
        return font;
    }

    public static View setTabCaption() {

        return null;
    }

    public static String manageDecimal(String temp) {
        String displayRiskDecimal = null, before = null, after = null;
        if (temp.contains(".")) {
            int in = temp.indexOf(".") + 1;
            before = temp.substring(0, in);
            after = temp.substring(in, temp.length());
            if (after.length() == 1) {
                displayRiskDecimal = before + after + "0";
            } else {
                displayRiskDecimal = before + after;
            }
            return displayRiskDecimal;
        } else {
            displayRiskDecimal = temp + ".00";
            return displayRiskDecimal;
        }
    }

    public static String manageDecimalPointForGetLines(String temp) {
        String displayRiskDecimal = null, before = null, after = null;
        int in = temp.indexOf(".") + 1;
        before = temp.substring(0, in);
        after = temp.substring(in, temp.length());
        if (after.equals("00")) {
            displayRiskDecimal = before.substring(0, before.length() - 1);
        } else if (after.length() == 2) {
            if (after.startsWith("0", 1)) {
                displayRiskDecimal = after.substring(0, after.length() - 1);
                displayRiskDecimal = before + displayRiskDecimal;
            }
        }
        return displayRiskDecimal;
    }

    public static String manageSign(String temp) {
        String displayRiskDecimal = null;
        if (temp.contains("-") || temp.contains("+")) {
            displayRiskDecimal = temp;
        } else {
            displayRiskDecimal = "+" + temp;
        }
        return displayRiskDecimal;
    }

    public static SpannableStringBuilder displayNameValue(String battingTeamName, String displayBaseValue) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        SpannableString WordtoSpan = new SpannableString(displayBaseValue);
        WordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#1298DE")), 0, displayBaseValue.length(), 0);
        builder.append(battingTeamName);
        builder.append(WordtoSpan);
        return builder;
    }

    private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the
		 * BufferedReader.readLine() method. We iterate until the BufferedReader
		 * return null which means there's no more data to read. Each line will
		 * appended to a StringBuilder and returned as String.
		 */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public static String removeSpecialChar(String value) {
        try {
            if (value.contains("("))
                value = value.replaceAll("", "");
            if (value.contains(")"))
                value = value.replaceAll("", "");
            if (value.contains("-"))
                value = value.replaceAll("-", "");
            if (value.contains(" "))
                value = value.replaceAll(" ", "");
            if (value.contains("."))
                value = value.replaceAll(".", "");
            if (value.contains(":"))
                value = value.replaceAll(":", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String getUrlVideoRTSP(String urlYoutube, String video_id) {
        try {
            String gdy = "http://gdata.youtube.com/feeds/api/videos/";
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String id = video_id;
            URL url = new URL(gdy + id);

            System.out.println("Gdata Url :::" + url);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            Document doc = documentBuilder.parse(connection.getInputStream());
            Element el = doc.getDocumentElement();
            NodeList list = el.getElementsByTagName("media:content");// /media:content
            String cursor = urlYoutube;
            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                if (node != null) {
                    NamedNodeMap nodeMap = node.getAttributes();
                    HashMap<String, String> maps = new HashMap<String, String>();
                    for (int j = 0; j < nodeMap.getLength(); j++) {
                        Attr att = (Attr) nodeMap.item(j);
                        maps.put(att.getName(), att.getValue());
                    }
                    if (maps.containsKey("yt:format")) {
                        String f = maps.get("yt:format");
                        if (maps.containsKey("url")) {
                            cursor = maps.get("url");
                        }
                        if (f.equals("1"))
                            return cursor;
                    }
                }
            }
            return cursor;
        } catch (Exception ex) {
            Log.e("Get Url Video ", ex.toString());
        }
        return urlYoutube;

    }

    public static void hideKeyboard(Activity context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static String getcurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("KK:mm a");
        return dateFormat.format(new Date());
    }

    public static String findDeviceID(Context cnt) {
        return Settings.Secure.getString(cnt.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void showNetworkAlert(Context ct) {
        try {
            final AlertDialog alertDialog = new AlertDialog.Builder(ct).create();

            alertDialog.setTitle("Info");
            alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });

            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String removeLastComma(String s) {
        if (s.length() > 0) {
            s = s.substring(0, s.length() - 1);
            return s;
        } else {
            return s;
        }
    }


    public static String getAccuraccy(int totalAns, int rightAns) {
        try {
            return String.valueOf(rightAns * 100 / totalAns);
        } catch (ArithmeticException e) {
            e.printStackTrace();
            return "0";
        }
    }


    public static float getProgress(int totalAns, int rightAns) {
        try {
            return (rightAns * 100 / totalAns);
        } catch (ArithmeticException e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static String convertArraytoCommaSeprated(ArrayList<String> array) {
        return TextUtils.join(",", array);
    }


    public static String getDataDir(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.dataDir;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    static int count = 0;


    public static int getNoOfFiles(String path) {
        count = 0;

        getFile(path);
        return count;
    }

    private static void getFile(String dirPath) {
        File f = new File(dirPath);
        File[] files = f.listFiles();

        if (files != null)
            for (int i = 0; i < files.length; i++) {
                count++;
                File file = files[i];

                if (file.isDirectory()) {
                    getFile(file.getAbsolutePath());
                }
            }
    }

    public static int currentAppVersionCode(Activity act) {

        PackageInfo pInfo = null;
        try {
            pInfo = act.getPackageManager().getPackageInfo(act.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionCode;
    }

    public static void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods


        Crashlytics.setUserIdentifier("ISccore-Parent");
        Crashlytics.setUserEmail("parshvaaedumentor@gmail.com");
        Crashlytics.setUserName("ISccore");
    }


    public static void webSettings(WebView webView) {
        WebSettings settings = webView.getSettings();

        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setDomStorageEnabled(true);
        settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);

    }
}
